from apps.contactos.models import *
from apps.campeonato.models import *
from django.contrib.auth.models import *
from apps.evento.models import *

grupos = ['UsuarioAdmin', 'UsuariosWeb', 'Arbitros']
for grupo in grupos:
    nuevo = Group.objects.create(name=grupo)

subdisciplinas = ['Futbol_5','Futbol_7','Futbol_9','Futbol_11','Basketball_3x3','Basketball_5x5','Voleyball_6x6']
for subdisciplina in subdisciplinas:
    nuevo = Subdisciplina.objects.create(
        nombre=subdisciplina,
    )

disciplinas = ['Futbol', 'Basketball', 'Voleyball']
for disciplina in disciplinas:
    nuevo = Disciplina.objects.create(
        nombre=disciplina,
    )

for disciplina in Disciplina.objects.all():
    for subdisciplina in Subdisciplina.objects.all():
        if disciplina.nombre in subdisciplina.nombre:
            disciplina.subdisciplinas.add(subdisciplina)
            disciplina.save()

TIPO_DOCUMENTOS = [
    ('0','Otro'),
    ('1','Cedula Ciudadania'),
    ('2','Cedula Extranjeria'),
    ('3','NIT'),
    ('4','Pasaporte'),
    ('5','Registro Unico de Identificacion'),
    ('6','Tarjeta de Identidad'),

]

for tipo in TIPO_DOCUMENTOS:
    tipo_documento = TipoIdentificacion(
        id = tipo[0],
        nombre = tipo[1],
    )
    tipo_documento.save()
    print("Creado documento", tipo_documento.nombre )

#Creacion de Municipios y departamentos de Colombia

import pandas as pd
from sodapy import Socrata

client = Socrata("www.datos.gov.co", None)
results = client.get("xdk5-pm3f", limit=2000)

departamentos = []
municipios = []

for result in results:
    departamento_str = result['departamento']
    municipio_str=result['municipio']

    if not departamento_str in departamentos:
        departamentos.append(departamento_str)
    
    municipios.append((departamento_str,municipio_str))

for depto in departamentos:
    departamento = Departamento(
        nombre = depto,
    )
    departamento.save()
    print('Departamento creado', departamento.nombre, departamento.pk)

for munic in municipios:
    municipio = Municipio(
        nombre = munic[1],
        departamento = Departamento.objects.get(nombre = munic[0])
    )
    municipio.save()
    print('Municipio Creado ',munic[1])

