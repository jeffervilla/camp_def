from apps.tickets.models import *

#tipos_riesgo = [('10', 'RIESGO I'), ('20', 'RIESGO IIA') , ('30', 'RIESGO IIB'), ('40', 'RIESGO III'), ('99', 'NO APLICA')]
#for tipo in tipos_riesgo:
#    TipoRiesgo.objects.create(id=tipo[0], nombre=tipo[1])

estados_tickets = [('10', 'POR_ASIGNAR', '#65c7ff'), ('20', 'ASIGNADO', '#984dff') , ('30', 'ACEPTADO', '#41e5c0'), ('40', 'EN_EJECUCION', '#ab8903'), ('50', 'EN_EVALUACION', '#29B765'), ('60', 'REASIGNADO', '#D67520'), ('70', 'FINALIZADO', '#3498DB'), ('80', 'NO_CONCLUIDO', '#E74C3C')]
for estado in estados_caso:
    EstadoTicket.objects.create(id=estado[0], nombre=estado[1], color=estado[2])

#tipos_propiedad = [('10', 'NINGUNO'), ('20', 'PROPIO') , ('30', 'COMODATO'), ('40', 'LEASING'), ('50', 'ARRIENDO'), ('60', 'EXTERNO')]
#for tipo in tipos_propiedad:
#    TipoPropiedad.objects.create(id=tipo[0], nombre=tipo[1])

tipos_urgencia = [('10', 'ALTA'), ('20', 'MEDIA') , ('30', 'BAJA')]
for tipo in tipos_urgencia:
    TipoUrgencia.objects.create(id=tipo[0], nombre=tipo[1])

#tipos_mantenimiento = [('10', 'CORRECTIVO'), ('20', 'PREVENTIVO') , ('30', 'PREDICTIVO')]
#for tipo in tipos_mantenimiento:
#    TipoMantenimiento.objects.create(id=tipo[0], nombre=tipo[1])

#tipos_equipo = [('10', 'EQUIPO DE OFICINA'), ('20', 'EQUIPO BIOMEDICO') , ('30', 'EQUIPO DE APOYO')]
#for tipo in tipos_equipo:
#    TipoEquipo.objects.create(id=tipo[0], nombre=tipo[1])

#tipos_empresa = [('10', 'CLIENTE'), ('20', 'PROVEEDOR') , ('30', 'OTRO')]
#for tipo in tipos_empresa:
#    TipoEmpresa.objects.create(id=tipo[0], nombre=tipo[1])
