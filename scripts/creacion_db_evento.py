import random

from apps.contactos.models import *
from apps.campeonato.models import *
from apps.evento.models import *

#region CREAR UN EVENTO 

evento = Evento(
    nombre = 'torneo grupoda2',
    disciplina = Disciplina.objects.get(pk = 1), 
    subdisciplina = Subdisciplina.objects.get(pk = 1), 
    creado_por = User.objects.get(username = 'william@grupoda2.com'), 
)

evento.save()
print("Evento Creado", evento.pk)

for i in range(3):
    categoria = Categoria (
        nombre = 'Categoria '+str(i),
        genero = 'Mixto',
        evento = evento
    )
    categoria.save()
    print("Categoria Creada", categoria.nombre)

    escenario = Escenario (
        nombre = 'escenario'+str(i),
        creado_por = User.objects.get(username = 'william@grupoda2.com'),
        evento = evento
    )
    escenario.save()
    print("Escenario Creado", escenario.nombre)


#endregion

#region CREACION DE EQUIPOS DE UNA CATEGORIA

categoria = Categoria.objects.filter(evento = evento)[0]

numero_equipos = 9 
contador_jugadores = 7
contador_cuerpo_tecnico = 2
contador_documento = 11111222
for j in range(numero_equipos):
    equipo = Equipo(
        nombre = 'equipo'+str(j),
        categoria = categoria,
        departamento = Departamento.objects.get(pk = random.randrange(1,34)),
        municipio = Municipio.objects.get(pk = random.randrange(1,1123) ),
        creado_por = User.objects.get(username = 'william@grupoda2.com'), 
        evento = evento
    )
    equipo.save()
    print("Equipo Creado {} {} {}".format(equipo.nombre, equipo.departamento, equipo.municipio))
    

    for k in range(contador_jugadores):
        jugador = Contacto(
            nombres = 'jugadores'+str(k),
            apellidos = 'jugadores'+str(k),
            documento = contador_documento,
            sexo='Masculino',
            telefono='22333222',
            creado_por = User.objects.get(username = 'william@grupoda2.com'), 
            tipo_identificacion = TipoIdentificacion.objects.get(pk=2),
            talla = 'S',
            edad = 15,
            equipo=equipo
        )
        jugador.save()
        print("Jugador Creado {} {} {}".format(jugador.nombres, jugador.apellidos, jugador.documento))
        contador_documento += 1

    for m in range(contador_cuerpo_tecnico):
        tecnico = Contacto(
            nombres = 'tecnicos'+str(k),
            apellidos = 'tecnicos'+str(k),
            documento = contador_documento,
            sexo='Masculino',
            telefono='22333222',
            email = 'example@example.com',
            creado_por = User.objects.get(username = 'william@grupoda2.com'),
            tipo_identificacion = TipoIdentificacion.objects.get(pk=2),
            talla = 'S',
            edad = 15,
            es_jugador = False,
            equipo = equipo
        )
        tecnico.save()
        print("Tecnico Creado".format(tecnico.nombres, tecnico.apellidos, tecnico.documento))
        contador_documento += 1

#endregion

