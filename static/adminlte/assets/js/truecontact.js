const contentMain = document.getElementById('contentMain');

function cargarPagina(url) {
  fetch(url)
    .then(function(response) {
      return response.text();
    })
    .then(function(html) {
      contentMain.innerHTML = html;
    })
    .catch(function(error) {
      console.log('Hubo un problema con la petición Fetch:' + error.message);
    });
}
