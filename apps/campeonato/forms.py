from django import forms
from .models import Partido, Abono

class PartidoForm(forms.ModelForm):
    class Meta:
        model = Partido
        fields = '__all__'

class AbonoForm(forms.ModelForm):
    class Meta:
        model = Abono
        fields = ['equipo', 'valor', 'archivo']