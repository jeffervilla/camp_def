from config.utils import render_to_pdf
from datetime import datetime, timezone
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.db.models.query_utils import select_related_descend
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import get_template
from django.urls import reverse
from django.urls.base import reverse_lazy
from django.utils import tree
from django.views.generic import ListView, View, DetailView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView, FormView, DeleteView
import json
import locale
locale.setlocale( locale.LC_ALL, '' )  #PARA ENVIAR LOS SALDOS BIEN BONITOS
import random
import string

from .forms import *
from .models import *
from apps.contactos.models import *
from apps.campeonato.utiles import *

class IndexView(TemplateView):
    template_name = 'init_page.html'

#region  PARTIDOS VIEWS
class PartidoConsultaDatos(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            partido = Partido.objects.get(pk=request.GET.get('pk_partido'))
            print("Tengo el partido ", partido)
            try:
                ganador = partido.ganador.nombre
            except:
                ganador = "Empate"
            partido_json = {
                'pk': partido.pk,
                'categoria' : {'pk': partido.categoria.pk, 'nombre': partido.categoria.nombre},
                'fase' : partido.fase,
                'grupo' : partido.grupo.nombre if partido.grupo else '',
                'local' : {'pk': partido.local.pk, 'nombre':partido.local.nombre},
                'visitante' : {'pk': partido.visitante.pk, 'nombre':partido.visitante.nombre},
                'fecha' : partido.fecha,
                'ganador': ganador,
                'arbitro1': partido.arbitro1,
                'arbitro2': partido.arbitro2,
                'arbitro3': partido.arbitro3,
                'cronogramista': partido.cronogramista,
                'coordinador': partido.coordinador,
            }
            print(partido_json)
        
        return JsonResponse({'partido': partido_json})
        


class PartidoEdit(UpdateView):

    def post(self, request, *args, **kwargs):
        info_partido = dict(request.POST)
        print("********************RECOJO PARTIDO ", info_partido)
        partido = Partido.objects.get(pk=request.POST.get('p_pk_partido'))
        goles_local = int(request.POST.get('goles_local'))
        goles_visitante = int(request.POST.get('goles_visitante'))
        local = partido.local
        print("el local es :", local)
        visitante = partido.visitante
        print("el visitante es :", visitante)
        try:
            penaltis_local = int(request.POST.get('penales_local'))
            penaltis_visitante = int(request.POST.get('penales_visitante'))
            if penaltis_local > penaltis_visitante:
                ganador = local
            elif penaltis_local < penaltis_visitante:
                ganador = visitante
            else:
                ganador = None
        except:
            print("No traigo resultado de Penaltis")
            if goles_local > goles_visitante:
                ganador = local
            elif goles_local < goles_visitante:
                ganador = visitante
            else:
                ganador = None
        print("el ganador es :", ganador)

        ids_jugadores_local=request.POST.getlist('id_local')
        print("lista ids jugadores", ids_jugadores_local)
        ids_jugadores_visitante=request.POST.getlist('id_visitante')
        print("lista ids jugadores", ids_jugadores_visitante)
        jugadores_partido = []

        for id_jugador in ids_jugadores_local:
            jugador = Contacto.objects.get(documento=id_jugador, equipo=local)
            jugadores_partido.append(jugador)
        
        for id_jugador in ids_jugadores_visitante:
            jugador = Contacto.objects.get(documento=id_jugador, equipo=visitante , es_jugador= True)
            jugadores_partido.append(jugador)
        
            
        partido.fecha = request.POST.get('fecha_hora_partido')
        partido.arbitro1 = request.POST.get('arbitro1').title()
        partido.arbitro2 = request.POST.get('arbitro2').title()
        partido.arbitro3 = request.POST.get('arbitro3').title()
        partido.cronogramista = request.POST.get('cronogramista').title()
        partido.coordinador = request.POST.get('coordinador').title()
        partido.ganador = ganador
        partido.estado = True
        partido.save()

        numeros_jugadores_local=request.POST.getlist('numero_jugador_local')
        goles_jugadores_local=request.POST.getlist('goles_jugador_local')
        amonestaciones_jugadores_local=request.POST.getlist('amonestaciones_jugador_local')
        expulsiones_jugadores_local=request.POST.getlist('expulsiones_jugador_local')

        for i in range(len(ids_jugadores_local)):
            jugador_partido = JugadoresPartido(
                jugador = Contacto.objects.get(documento=ids_jugadores_local[i], equipo=local, es_jugador=True),
                numero = numeros_jugadores_local[i],
                gol_partido = goles_jugadores_local[i],
                amarilla_partido = amonestaciones_jugadores_local[i],
                roja_partido = expulsiones_jugadores_local[i],
                local = True,
                evento = partido.evento
            )
            jugador_partido.save()
            partido.jugadores.add(jugador_partido)
            partido.save()

        numeros_jugadores_visitante=request.POST.getlist('numero_jugador_visitante')
        goles_jugadores_visitante=request.POST.getlist('goles_jugador_visitante')
        amonestaciones_jugadores_visitante=request.POST.getlist('amonestaciones_jugador_visitante')
        expulsiones_jugadores_visitante=request.POST.getlist('expulsiones_jugador_visitante')

        for i in range(len(ids_jugadores_visitante)):
            jugador_partido = JugadoresPartido(
                jugador = Contacto.objects.get(documento=ids_jugadores_visitante[i], equipo=visitante, es_jugador=True),
                numero = numeros_jugadores_visitante[i],
                gol_partido = goles_jugadores_visitante[i],
                amarilla_partido = amonestaciones_jugadores_visitante[i],
                roja_partido = expulsiones_jugadores_visitante[i],
                local = False,
                evento = partido.evento

            )  
            jugador_partido.save()                
            partido.jugadores.add(jugador_partido)
            partido.save()

        print("La fasse a enviar es", partido.fase)        

        return JsonResponse({'mensaje':'Partido diligenciado correctamente.','fase':partido.fase})

class PartidoListView(ListView):
    model = Partido
    paginate_by = 20
    template_name = 'partido/fixture.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre

        categorias = []
        for categoria in Categoria.objects.all():
            if Grupo.objects.filter(categoria=categoria).exists():
                categorias.append(categoria)
        context['categorias'] = categorias
        context['partidos'] = Partido.objects.all()
        print("El context a enviar es: ", context)
        return context

class PartidoListEdit(ListView):
    model = Partido
    paginate_by = 20
    template_name = 'partido/lista_diligenciar.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre

        categorias = []
        for categoria in Categoria.objects.filter(evento=evento):
            if Grupo.objects.filter(categoria=categoria).exists():
                categorias.append(categoria)
        context['categorias'] = categorias
        context['partidos'] = Partido.objects.all()
        print("El context a enviar es: ", context)
        return context

class PartidoListDetail(View):
    model = Partido

    def get (self, request, pk, *args, **kwargs):
        # context = super().get_context_data(**kwargs)
        pk_partido = self.kwargs['pk']
        partido = Partido.objects.get(pk=pk_partido)

        jugadores_locales = []
        jugadores_visitantes = []
        for jugador in partido.jugadores.all():
            if jugador.jugador.equipo == partido.local:
                jugadores_locales.append(jugador)
            else:
                jugadores_visitantes.append(jugador)

        template = 'partido/detalle.html'
        return render(self.request, template, {'partido': partido, 'jugadores_locales': jugadores_locales,
                    'jugadores_visitantes':jugadores_visitantes })

class PartidosGenerate(TemplateView):
    template_name = 'partido/sorteo.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre
        categorias = []
        for categoria in Categoria.objects.filter(evento=evento):
            if Grupo.objects.filter(categoria=categoria).exists():
                categorias.append(categoria)
        context['categorias'] = categorias
        print("El context a enviar es: ", context)
        return context

class PartidoGenerar(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))

            if Partido.objects.filter(categoria=categoria, estado=True).exists():
                print("Ya tiene partidos creados y diligenciados")
                mensaje = "No es posible generar partidos debido a que ya hay algunos diligneciados"
            else:
                Partido.objects.filter(categoria=categoria).delete()
                print("Partidos borrados")
                fixture = []
                grupos = Grupo.objects.filter(categoria=categoria)
                print("Tengo {} grupos".format(grupos.count()))
                
                if grupos.count() == 1:
                    print("Entro a tengo un grupo = Liguilla")
                    print(grupos)
                    grupo = grupos[0]
                    numero_rondas = grupo.numero_rondas
                    print("el numero de rondas selecciinadas para la iguilla es:", numero_rondas)
                    if numero_rondas == 1:
                        print("entro a 1 ronda")
                        fixture = round_robin(grupo.equipos.all())
                    elif numero_rondas == 2:
                        print("entro a 2 rondas")
                        ida = round_robin(grupo.equipos.all().order_by('pk'))
                        print("de ida tenemos:", ida)
                        vuelta = round_robin(grupo.equipos.all().order_by('-pk'))
                        print("de velta tenemoos", vuelta)
                        fixture = ida + vuelta
                        print("el fixture completo es", fixture)

                    print("La generacion de partidos definitiva es: ", fixture)
                    partidos = []
                    for encuentro in fixture:
                        if isinstance(encuentro,list):
                            print("El encuentro seleccionado es ", encuentro)
                            print("El equipo local es ", encuentro[0])
                            print("El equipo visitnte es ", encuentro[1])

                            partido = Partido(
                                local = encuentro[0],
                                visitante = encuentro[1],
                                categoria = categoria,
                                grupo = grupo,
                                fase = 'Grupos',
                                evento = grupo.evento
                            )
                            print("El partido tiene a {} vs {} por la categoria {} ".format(partido.local, partido.visitante, partido.categoria))
                            partido.save()
                            partido_json = {
                                'pk': partido.pk,
                                'categoria' : partido.categoria.nombre,
                                'fase' : partido.fase,
                                'grupo' : partido.grupo.nombre,
                                'local' : partido.local.nombre,
                                'visitante' :partido.visitante.nombre,
                            }
                            partidos.append(partido_json)
                            print("llevo estos partidos: ", partidos)
                        else:
                            pass
                
                elif grupos.count() > 1:
                    print("entro a tengo varios grupos")
                    partidos = []
                    for grupo in grupos:
                        numero_rondas = grupo.numero_rondas
                        print("Estoy en el grupo: ", grupo)
                        if numero_rondas == 1:
                            print("entro a 1 ronda")
                            fixture = round_robin(grupo.equipos.all())
                            print("el fixture temporal es", fixture)
                        elif numero_rondas == 2:
                            print("entro a 2 rondas")
                            ida = round_robin(grupo.equipos.all().order_by('pk'))
                            print("de ida tenemos:", ida)
                            vuelta = round_robin(grupo.equipos.all().order_by('-pk'))
                            print("de velta tenemoos", vuelta)
                            fixture = ida + vuelta
                            print("el fixture completo es", fixture)
                        
                        for encuentro in fixture:
                            if isinstance(encuentro,list):
                                print("El encuentro seleccionado es ", encuentro)
                                print("El equipo local es ", encuentro[0])
                                print("El equipo visitnte es ", encuentro[1])

                                partido = Partido(
                                    local = encuentro[0],
                                    visitante = encuentro[1],
                                    categoria = categoria,
                                    grupo = grupo,
                                    fase = 'Grupos',
                                    evento = grupo.evento
                                )
                                print("El partido tiene a {} vs {} por la categoria {} ".format(partido.local, partido.visitante, partido.categoria))
                                partido.save()
                                partido_json = {
                                    'pk': partido.pk,
                                    'categoria' : partido.categoria.nombre,
                                    'fase' : partido.fase,
                                    'grupo' : partido.grupo.nombre,
                                    'local' : partido.local.nombre,
                                    'visitante' :partido.visitante.nombre,
                                }
                                partidos.append(partido_json)
                                print("llevo estos partidos: ", partidos)
                            else:
                                pass
            mensaje = "Partidos Generados Satisfactoriamente"

            return JsonResponse({'mensaje':mensaje, 'partidos': partidos})

        else:
            respuesta = {'mensaje':"Error"}
            return JsonResponse(respuesta, safe=False)

class PartidoDelete(View):

    def get(self, request, pk, *args, **kwargs):
        pk_partido = self.kwargs['pk']
        partido = Partido.objects.get(pk = pk_partido)
        print("El partido es", partido)
        jugadores = partido.jugadores
        print("integrantes", jugadores)
        jugadores_locales = jugadores.filter(local=True)
        jugadores_visitantes = jugadores.filter(local=False)
        
        print("Jugadores Locales ", jugadores_locales)
        print("Jugadores Visitantes ", jugadores_visitantes)
        template = 'partido/partido_confirm_delete.html'
        return render(self.request, template, {'partido':partido, 'jugadores_locales': jugadores_locales, 'jugadores_visitantes': jugadores_visitantes })

    def post(self, request, pk,*args, **kwargs):
        pk = self.kwargs['pk']
        object = Partido.objects.get(id=pk)
        print("El partido es", object)
        integrantes = object.jugadores.all()
        print("integrantes", integrantes)
        for integrante in integrantes:
            integrante.delete()
        object.delete()

        return HttpResponseRedirect(reverse_lazy('partido-list')) 

class PartidoValidarClasificacion(View):

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR VALIDAR CLASIFICADOS.......")
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))
            if Partido.objects.filter(categoria=categoria, estado=False).exists():
                respuesta = {"partidos_validados": False}
            else:
                respuesta = {"partidos_validados": True}
                fase = validar_fase(categoria)
                if fase == 'Grupos':
                    print("Esta en fase de grupos")
                    numero_grupos_en_categoria = nro_grupos_en_categoria(categoria)
                    print("Tengo el sgte numero de grupos: ", numero_grupos_en_categoria)
                    clasificados_directos = nro_equipos_clasificados_fase_grupos(numero_grupos_en_categoria)[0]
                    print("Clasifican directo: ", clasificados_directos)
                    clasificados_adicionales = nro_equipos_clasificados_fase_grupos(numero_grupos_en_categoria)[1]
                    print("Clasifican de complemento ", clasificados_adicionales)
                    equipos_clasificados = clasificados_por_categoria_en_grupos(clasificados_adicionales, categoria)
                else:
                    print("Esta en fase: ", fase)
                    equipos_clasificados = clasificados_por_categoria_fase(fase, categoria)
                print("los clasificados a la siguiente fase son: ", equipos_clasificados)
                if len(equipos_clasificados) > 0:
                    generar_partidos_fase(equipos_clasificados, categoria)

            return JsonResponse(respuesta, safe=False)

class JugadoresPartidoList(View ):

    def get (self, request, pk, *args, **kwargs):
        # context = super().get_context_data(**kwargs)
        pk_partido = self.kwargs['pk']
        partido = Partido.objects.get(pk = pk_partido)
        print("El partido es", partido)
        jugadores = partido.jugadores
        print("integrantes", jugadores)
        jugadores_locales = jugadores.filter(local=True)
        jugadores_visitantes = jugadores.filter(local=False)

        print("Jugadores Locales ", jugadores_locales)
        print("Jugadores Visitantes ", jugadores_visitantes)
        template = 'partido/detail.html'
        return render(self.request, template, {'partido':partido, 'jugadores_locales': jugadores_locales, 'jugadores_visitantes': jugadores_visitantes })

#endregion

#region EQUIPOS VIEWS

class EquipoCreate(CreateView):
    model = Equipo
    fields = '__all__'
    template_name = 'equipo/create.html'

    def get_context_data(self, **kwargs):
        context = super(EquipoCreate, self).get_context_data(**kwargs)
        evento = Evento.objects.get(pk=self.kwargs['pk'])

        departamentos = Departamento.objects.all()
        departamentos_list = []
        for departamento in departamentos:
            municipios = Municipio.objects.filter(departamento=departamento)
            departamentos_list.append({departamento.pk:[departamento.nombre,municipios]})

        context['categorias'] = Categoria.objects.filter(evento=evento)
        context['municipios'] = departamentos_list
        context['departamentos'] = Departamento.objects.all()
        context['tipos_id'] = TipoIdentificacion.objects.all()
        context['evento'] = evento

        return context

    def post(self, request, pk):
        print(">>>>EquipoCreate >>>El POST es: ",self.request.POST)
        equipo = Equipo(
            nombre=self.request.POST.get('nombre_equipo').title(),
            categoria = Categoria.objects.get(pk=self.request.POST.get('categoria')),
            municipio = Municipio.objects.get(pk=self.request.POST.get('municipio')),
            departamento = Departamento.objects.get(pk=self.request.POST.get('departamento')),
            creado_por = self.request.user,
            activo= True,
            evento = Evento.objects.get(pk=pk)
        )
        equipo.save()


        nombres_integrantes=self.request.POST.getlist('nombres_integrante')
        apellidos_integrantes=self.request.POST.getlist('apellidos_integrante')
        tipodoc_integrantes=self.request.POST.getlist('tipo_integrante')
        doc_integrantes=self.request.POST.getlist('documento_integrante')
        tel_integrantes=self.request.POST.getlist('tel_integrante')
        edad_integrantes=self.request.POST.getlist('edad_integrante')
        genero_integrantes=self.request.POST.getlist('genero_integrante')
        talla_integrantes=self.request.POST.getlist('talla_integrante')
        correo_integrantes=self.request.POST.getlist('correo_integrante')
        rol_integrantes=self.request.POST.getlist('rol')

        print("nombres_integrantes",nombres_integrantes)
        print("rol_integrantes",rol_integrantes)
        print("lista correos",correo_integrantes)
        print("tipo doc ", tipodoc_integrantes)

        for i in range(len(nombres_integrantes)):

            if rol_integrantes[i]=='1':
                jugador=True
                correo=''
                if genero_integrantes[i]=='1':
                    genero = 'Masculino'
                elif (genero_integrantes[i]=='2'):
                    genero = 'Femenino'
            else:
                jugador=False
                correo=correo_integrantes[i]
                genero =""

            print("Integrante No: {}. Es jugador: {}".format(i,str(jugador)))


            contacto = Contacto(
                nombres=nombres_integrantes[i].title(),
                apellidos=apellidos_integrantes[i].title(),
                telefono=tel_integrantes[i],
                es_jugador=jugador,
                equipo=equipo,
                sexo=genero,
                edad=edad_integrantes[i],
                tipo_identificacion=TipoIdentificacion.objects.get(pk=tipodoc_integrantes[i]),
                documento=doc_integrantes[i],
                talla=talla_integrantes[i],
                email = correo,
                activo= True
            )
            contacto.save()
            print("Integrante {} {} creado en BD ".format(contacto.nombres, contacto.apellidos))

        # variable = self.request.POST.get('nombre_equipo')

        return HttpResponseRedirect(reverse_lazy('equipo-list')) 

class EquipoUpdate( UpdateView ):
    model = Equipo
    fields = ['nombre','categoria','municipio','departamento']
    template_name = 'equipo/update.html'
    success_url = reverse_lazy('equipo-list')

class EquipoList( ListView ):

    model = Equipo
    paginate_by = 20
    template_name = 'equipo/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user=self.request.user 
        context['equipos'] = Equipo.objects.filter(creado_por=user, activo=True)
        return context

class EquipoDelete(View):

    def get(self, request, pk, *args, **kwargs):
        pk_equipo = self.kwargs['pk']
        equipo = Equipo.objects.get(pk = pk_equipo)
        print("El equipo es", equipo)
        integrantes = Contacto.objects.filter(equipo=equipo)
        print("integrantes", integrantes)
        jugadores = integrantes.filter(es_jugador=True, activo=True)
        tecnicos = integrantes.filter(es_jugador=False, activo=True)
        
        print("Jugadores ", jugadores)
        print("Tecnicos ", tecnicos)
        template = 'contactos/equipo_confirm_delete.html'
        return render(self.request, template, {'jugadores': jugadores, 'tecnicos': tecnicos })

    def post(self, request, pk,*args, **kwargs):
        pk = self.kwargs['pk']
        equipo = Equipo.objects.get(id=pk)
        print("El equipo es", equipo)
        integrantes = Contacto.objects.filter(equipo=equipo)
        print("integrantes", integrantes)
        for integrante in integrantes:
            integrante.activo = False
            integrante.save()
        equipo.activo = False 
        equipo.save()

        return HttpResponseRedirect(reverse_lazy('equipo-list')) 

class EquipoEstado(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            pk_equipo = request.GET.get('pk_equipo')
            if Equipo.objects.filter(pk=pk_equipo).exists():
                equipo = Equipo.objects.filter(pk=pk_equipo)
                opcion = request.GET.get('pk_equipo')

class AprobarEquipo(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            pk_equipo = request.GET.get('pk_equipo')
            if Equipo.objects.filter(pk=pk_equipo).exists():
                equipo = Equipo.objects.get(pk=pk_equipo)
                print("Equipo: ", equipo)
                equipo.aprobado = True
                equipo.save()
                respuesta = "OK Aprobado"
            else:
                print("No hay equipo")
                respuesta = "ERROR Aprobado"
        return JsonResponse({'respuesta': respuesta} , safe=False)

class RechazarEquipo(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            pk_equipo = request.GET.get('pk_equipo')
            if Equipo.objects.filter(pk=pk_equipo).exists():
                equipo = Equipo.objects.get(pk=pk_equipo)
                print("Equipo: ", equipo)
                equipo.aprobado = False
                equipo.save()
                respuesta = "OK Rechazado"
            else:
                print("No hay equipo")
                respuesta = "ERROR Rechazado"
        return JsonResponse({'respuesta': respuesta} , safe=False)

class EquiposCategoria(View):
    
        def get(self, request, *args, **kwargs):
            categoria_pk = request.GET.get('categoria_pk')
            print('PK: ',categoria_pk)
            if (Categoria.objects.filter(pk=categoria_pk).exists()):
                categoria = Categoria.objects.get(pk=categoria_pk)
                equipos_queryset = Equipo.objects.filter(categoria=categoria)

            equipos_list = []
            for equipo in equipos_queryset:
                equipos_list.append(
                    {
                        'pk': equipo.pk,
                        'nombre': equipo.nombre,
                    }
                )
            respuesta = {'equipos_local':equipos_list}

            print("EquiposLocal View: ", respuesta)
            return JsonResponse(respuesta, safe=False)

class EquiposGrupoManual(View):

    def post(self, request, *args, **kwargs):
        grupos = dict(request.POST)
        print("********************RECOJO GRUPOS ", grupos)

        for pk_grupo, equipos_grupos  in grupos.items():
            print("El grupo ", pk_grupo)
            print("equipos", equipos_grupos)
            # try:
            if(pk_grupo == 'csrfmiddlewaretoken'):
                pass
            else:
                print("Entro al try")
                grupo= Grupo.objects.get(pk=pk_grupo)
                print("el grupo selecccionado es", grupo)
                print("el grupo tiene estos equipos", grupo.equipos.all())
                grupo.equipos.clear()
                print("limpiamos los equipos del grupo")
                print("el grupo tiene estos equipos", grupo.equipos.all())
                for pk_equipo in equipos_grupos:
                    print("tomo el pk del equipo:", pk_equipo)
                    equipo = Equipo.objects.get(pk=pk_equipo)
                    print("el equipo seleccionado es", equipo)
                    grupo.equipos.add(equipo)
                    grupo.save()
                    print("temporalmente tengo los siguientes grupos", grupo.equipos.all())
                print("El grupo {} tiene estos equipos {}".format(grupo.nombre, grupo.equipos.all()))
            # except:
            #     print("salgo por un error, revisar views linea 586")

        return JsonResponse({'mensaje':'Equipos asignados correctaente.'})


class AdminEquiposList(ListView):
    model = Equipo
    paginate_by = 20
    template_name = 'admin/equipos_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre
        context['categorias'] = Categoria.objects.filter(evento=evento)

        context['equipos'] = Equipo.objects.filter(evento=evento, activo=True)
        context['AbonoForm'] = AbonoForm()

        return context

    def post(self, request, pk):
        print('XXXXXXXXXXXXXXX',request.POST)
        form_abono = AbonoForm(request.POST, request.FILES)
        print(" FORMULARIO.....", form_abono)
        print(" FORMULARIO ERROR.....", form_abono.errors)

        # pk_equipo=self.request.POST.get('id_equipo')
        # nuevo_abono=self.request.POST.get('abono_enviar')
        # nuevo_saldo=self.request.POST.get('saldo_enviar')
        # soporte = self.request.POST.get('abono_soporte')
        abono = form_abono.save()
        equipo = abono.equipo
        equipo.abono_inscripcion = equipo.abono_inscripcion+int(abono.valor)
        equipo.saldo_inscripcion =  equipo.saldo_inscripcion-int(abono.valor)
        equipo.save()

        # abono = Abono(
        #     equipo = equipo,
        #     valor = nuevo_abono,
        #     fecha = datetime.now(),
        #     archivo= soporte
        # )
        # abono.save()
        return HttpResponseRedirect(reverse_lazy('equipos-list'))

class AdminFaseInicial(View):

    def get(self, request, pk, *args, **kwargs):
        pk_evento = self.kwargs['pk']
        evento = Evento.objects.get(pk = pk_evento)
        print("El equipo es", evento)
        categoria_seleccionada = Categoria.objects.get(pk = request.GET.get('categoria'))
        grupos_lista = []
        for grupo in Grupo.objects.filter(categoria=categoria_seleccionada):
            equipos_lista = []
            for equipo in grupo.equipos.all():
                equipo_agregar = {
                    'nombre': "{} ({} - {})".format(equipo.nombre.title(),equipo.departamento.nombre[:3].upper(), equipo.municipio.nombre[:3].upper()),
                }
                equipos_lista.append(equipo_agregar)
            grupo_agregar = {
                'nombre': grupo.nombre,
                'equipos': equipos_lista
            }
            grupos_lista.append(grupo_agregar)

        categoria = {
            'pk' : categoria_seleccionada.pk,
            'nombre' : categoria_seleccionada.nombre,
            'equipos_inscritos' : Equipo.objects.filter(categoria = categoria_seleccionada),
            'grupos' : grupos_lista,
            'partidos' : Partido.objects.filter(categoria=categoria_seleccionada)
        }

        
        template = 'inicial.html'
        return render(request, template, {'categoria': categoria, 'pk_evento': pk_evento , 'nombre_evento':evento.nombre })


class VerSaldo(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            abonos_equipo = Abono.objects.filter(equipo__pk=request.GET.get('pk_equipo'))
            print("los abonos del equipo son:", abonos_equipo.count())
            obj_dato={}
            lista_json = []
            if abonos_equipo.count() > 0:
                for abono in abonos_equipo:
                    print("Tomo el abono tal :", abono)
                    print("el abono tiene los siguientes datos: {}".format(obj_dato))

                    lista_json.append({
                        "pk" : abono.pk,
                        "equipo" : abono.equipo.nombre,
                        "valor" : locale.currency(abono.valor, grouping = True ),
                        "fecha" : abono.fecha.strftime("%d/%m/%Y a las %H:%M:%S"),
                        "url_soporte" : abono.archivo.url if abono.archivo else "",

                    })
                    print("La lista JSON a enviar es: {} ".format(lista_json))
            else:
                lista_json.append({
                        "pk" : "",
                        "equipo" : "",
                        "valor" : "No registra",
                        "fecha" : "Abono",
                        "url_soporte" : ""
                    })
                
            print("El JSON a enviar es: ",lista_json)
            
            return JsonResponse({'abonos': lista_json})   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO
        else:
            return JsonResponse({'abonos': ''})   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO

class VerIntegrantes(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            jugadores_equipo = Contacto.objects.filter(equipo__pk=request.GET.get('pk_equipo'), es_jugador=True)
            tecnicos_equipo = Contacto.objects.filter(equipo__pk=request.GET.get('pk_equipo'), es_jugador=False)
            print("los jugadores del equipo son:", jugadores_equipo.count())
            print("los tecnicos del equipo son:", tecnicos_equipo.count())

            obj_dato={}
            lista_json_jugadores = []
            if jugadores_equipo.count() > 0:
                for jugador in jugadores_equipo:
                    print("Tomo el jugador tal :", jugador)
                    # print("el abono tiene los siguientes datos: {}".format(obj_dato))

                    lista_json_jugadores.append({
                        "id" : jugador.documento,
                        "nombre" : "{} {}".format(jugador.nombres, jugador.apellidos),
                        "genero" : jugador.sexo,
                        "edad" : jugador.edad,
                        "talla" : jugador.talla,

                    })
            else:
                lista_json_jugadores.append({
                        "id" : "",
                        "nombre": "No hay",
                        "genero" :"Jugadores ",
                        "edad" : "Inscritos",
                        "talla" : "",
                    })

            lista_json_tecnicos = []
            if tecnicos_equipo.count()> 0:
                for tecnico in tecnicos_equipo:
                    print("Tomo el tecnico tal :", tecnico)
                    # print("el abono tiene los siguientes datos: {}".format(obj_dato))

                    lista_json_tecnicos.append({
                        "id" : tecnico.documento,
                        "nombre" : "{} {}".format(tecnico.nombres, tecnico.apellidos),
                    })
            else:
                lista_json_tecnicos.append({
                        "id" : "No hay",
                        "nombre": "Tecnicos inscritos",
                    })
            lista_json = []
            lista_json.append(lista_json_jugadores)
            lista_json.append(lista_json_tecnicos)
            
            return JsonResponse({'integrantes': lista_json})   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO
        else:
            return JsonResponse({'integrantes': ''})   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO


#endregion

#region CALSIFICACION VIEWS

class Clasificacion(TemplateView):
    template_name = 'clasificacion.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre

        categorias = []
        for categoria in Categoria.objects.all():
            partidos = Partido.objects.filter(categoria=categoria, estado=True).count() #poner estado = true
            if partidos >= 1:
                categorias.append(categoria)

        context['categorias'] = categorias
        return context

class ClasificacionCategoria(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))
            grupos_cat = Grupo.objects.filter(categoria=categoria)
            grupos = {}
            print("Los gruipos de la cartegoria son: ", grupos_cat)
            for grupo in grupos_cat:
                print("#########################################################################")
                print("Tomo el grupo :", grupo)
                print("#########################################################################")
                grupo_json = {'nombre': grupo.nombre, 'pk':grupo.pk, 'equipos':[]}
                print("el JSON se ve de esta manera: ", grupo_json)
                equipos_list= get_estadisticas_grupo(grupo)
                grupos[grupo.nombre] = {'pk': grupo.pk, 'equipos': equipos_list}
                print("El grupo lleva esta informacion: ", grupos)
            print("Se retorna esto: ", grupos)
            return JsonResponse({'respuesta': grupos})

class ClasificacionView(TemplateView):
    template_name = 'clasificaciones.html'

#endregion

#region ESTADISTICAS VIEWS

class EstadisticasJugadores(TemplateView):
    template_name = 'estadisticas/jugadores.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre

        categorias = []
        for categoria in Categoria.objects.filter(evento=evento):
            partidos = Partido.objects.filter(categoria=categoria).count() #poner estado = true
            if partidos >= 1:
                categorias.append(categoria)

        context['categorias'] = categorias
        return context

class EstadisticasEquipos(TemplateView):
    template_name = 'estadisticas/equipos.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre

        categorias = []
        for categoria in Categoria.objects.filter(evento=evento):
            partidos = Partido.objects.filter(categoria=categoria).count() #poner estado = true
            if partidos >= 1:
                categorias.append(categoria)

        context['categorias'] = categorias
        return context

class EstadisticasCategoriaEquipos(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))
            equipos = Equipo.objects.filter(categoria=categoria)
            print("Los equipos de la cartegoria son: ", equipos)
            equipos_list = []
            for equipo in equipos:
                partidos = Partido.objects.filter(
                    Q(local=equipo, estado = True) |
                    Q(visitante=equipo, estado = True) 
                )
                print("Tomo el equipo :", equipo)
                jugados = 0
                ganados= 0
                empatados = 0
                perdidos = 0
                puntos = 0
                rendimiento = 0
                goles_a_favor = 0
                goles_en_contra = 0
                gol_diferencia = 0
                print("Los partidos de este equipo fueron: ", partidos)
                for partido in partidos:
                    print("Para este partido el ganador fue: ", partido.ganador)
                    jugados += 1
                    if partido.ganador == equipo:
                        ganados += 1
                    elif partido.ganador == None:
                        empatados += 1
                    else:
                        perdidos += 1

                    for jugador in partido.jugadores.all():
                        print("Revisamos cada jugador del partido para ver si fue GF o GC")
                        print("tomamos el jugador: ", jugador)
                        if jugador.gol_partido > 0:
                            print("Marco goles en el partido")
                            if jugador.jugador.equipo == equipo:
                                print("Jugador pertenece al equipo. Goles a Favor")
                                goles_a_favor += jugador.gol_partido
                            else:
                                goles_en_contra += jugador.gol_partido
                                print("Jugador no pertenece al equipo. Goles contra")
                    gol_diferencia = goles_a_favor - goles_en_contra
                    puntos = ganados*3 + empatados
                    rendimiento = round((puntos*100)/(jugados*3),2)
                print("Partidos Jugados: ", jugados)
                print("Partidos Ganados: ", ganados)
                print("Partidos Empatados: ", empatados)
                print("Partidos Perdidos: ", perdidos)
                print("GF: ", goles_a_favor)
                print("GC: ", goles_en_contra)
                print("GD: ", gol_diferencia)  
                
                equipo_json = {
                    'equipo': equipo.nombre,
                    'pj': jugados,
                    'pg': ganados,
                    'pe': empatados,
                    'pp': perdidos,
                    'gf': goles_a_favor,
                    'gc': goles_en_contra,
                    'gd': gol_diferencia,
                    'rdto': rendimiento
                    }
                print("el JSON a reotrnar del equipo es: ", equipo_json)
                equipos_list.append(equipo_json)
            print("Se retorna esto: ", equipos_list)
            return JsonResponse({'respuesta': equipos_list})

class EstadisticasCategoriaJugadores(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))
            jugadores = Contacto.objects.filter(equipo__categoria=categoria)
            print("Los jugadores de la cartegoria son: ", jugadores)
            jugadores_list = []
            for jugador in jugadores:
                partidos = JugadoresPartido.objects.filter(jugador=jugador)
                print("Tomo el jugador :", jugador)
                jugados = partidos.count()
                goles= 0
                amarillas = 0
                rojas = 0
                print("Los partidos de este jugador fueron: ", partidos)
                for partido in partidos:
                    goles += partido.gol_partido
                    amarillas += partido.amarilla_partido
                    rojas += partido.roja_partido 

                print("Partidos Jugados: ", jugados)
                print("Goles marcados: ", goles)
                print("Amarillas Recibidas: ", amarillas)
                print("Rojas Recibidas: ", rojas)

                if goles > 0 or amarillas > 0 or rojas > 0:
                    jugador_json = {
                        'jugador': "{} {}".format(jugador.nombres, jugador.apellidos),
                        'equipo': jugador.equipo.nombre,
                        'goles': goles,
                        'amarillas': amarillas,
                        'rojas': rojas,
                        }
                    print("el JSON a reotrnar del jugador es: ", jugador_json)
                    jugadores_list.append(jugador_json)
            print("Se retorna esto: ", jugadores_list)
            return JsonResponse({'respuesta': jugadores_list})

#endregion

#region GRUPOS VIEWS
class GruposView(TemplateView):
    template_name = 'admin/grupos/grupos.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre

        categorias = []
        for categoria in Categoria.objects.filter(evento=evento):
            grupos = Grupo.objects.filter(categoria=categoria).count()
            if grupos >= 2:
                categorias.append(categoria)

        context['categorias'] = categorias
        return context

class GruposCreateView(TemplateView):
    template_name = 'admin/grupos/grupos_create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre

        categorias = []
        for categoria in Categoria.objects.filter(evento=evento):
            equipos = Equipo.objects.filter(categoria=categoria).count()
            if equipos >= 6:
                categorias.append(categoria)

        context['categorias'] = categorias
        return context

class RecomendacionGrupos(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            print("Traigo :", request.GET.get('pk_categoria'))
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))
            equipos_aprobados = Equipo.objects.filter(categoria=categoria, aprobado = True)
            distribucion_grupos =numero_grupos(equipos_aprobados.count())
            try:
                print("distribucion de equipos quedo [grupos de 4, grupos de 3 y grupos de 5", distribucion_grupos)
                return JsonResponse({'grupos_4': distribucion_grupos[0], "grupos_3":distribucion_grupos[1] ,"grupos_5":distribucion_grupos[2]})
            except:
                print("distribucion de equipos quedo [grupos de 4 y grupos de 5", distribucion_grupos)
                return JsonResponse({'grupos_4': distribucion_grupos[0], "grupos_5":distribucion_grupos[1]})


class VerDistribucionGrupos(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            print("Traigo :", request.GET.get('pk_categoria'))
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))
            grupos_lista = []
            for grupo in Grupo.objects.filter(categoria=categoria):
                equipos_lista = []
                for equipo in grupo.equipos.all():
                    equipos_lista.append(
                        {
                            'id': equipo.pk,
                            'nombre': "{} ({} - {})".format(equipo.nombre.title(),equipo.departamento.nombre[:3].upper(), equipo.municipio.nombre[:3].upper()),
                        }
                    )
                grupos_lista.append(
                    {
                        'id': grupo.pk,
                        'nombre': grupo.nombre.title(),
                        'equipos': equipos_lista
                    }
                )
            return JsonResponse({'pk': categoria.pk,'grupos': grupos_lista})

class CategoriaGenerarLiguilla(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            print("pk categoria",request.GET.get('pk_categoria'))
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))
            equipos_aprobados = Equipo.objects.filter(categoria=categoria, aprobado = True)
            numero_equipos_aprobados = equipos_aprobados.count()
            equipos_categoria_lista = list(equipos_aprobados)
            random.shuffle(equipos_categoria_lista)
            # try:
            if Grupo.objects.filter(categoria=categoria).exists():
                print("Ya tiene Liga creada")
                Grupo.objects.filter(categoria=categoria).delete()
            else:
                print("NO tiene Liga creada")
            grupo = Grupo(
                nombre = "Liguilla {}".format(categoria.evento.nombre.title()),
                categoria = categoria,
                numero_equipos = numero_equipos_aprobados,
                evento = categoria.evento,
                numero_rondas = int(request.GET.get('numero_rondas_liguilla'))
            )
            grupo.save()
            
            for equipo in equipos_categoria_lista:
                grupo.equipos.add(equipo)
                grupo.save()

            print("Liguilla Creada")
            return JsonResponse({'mensaje': "Liguilla Generada satisfactoriamente"})
            # except:
            #     print("No puedo borrar liguilla")
            #     return JsonResponse({'mensaje': "Inposible generar lliguilla. Partidos ya creados"})

class CategoriaGenerarGruposManual(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            # print("Traigo :", request.GET.get('pk_categoria'))
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))
            equipos_aprobados = Equipo.objects.filter(categoria=categoria, aprobado = True)
            recomendacion = True if request.GET.get('recomendacion') == 'si' else False
            numero_rondas = request.GET.get('numero_rondas_grupos')
            # print("LA CATEGORIA ES ", categoria)form-manual
            # try:
            if Grupo.objects.filter(categoria=categoria).exists():
                print("YA tiene Grupos creados")
                Grupo.objects.filter(categoria=categoria).delete()
            else:
                print("NO tiene grupos creados")

            if recomendacion:
                print("aprobo recomendacion")
                generar_grupos_auto(categoria, numero_rondas)
            else:    
                print("NO aprobo recomendacion")
                numero_grupos = int(request.GET.get('numero_grupos'))
                numero_equipos_por_grupo = int(request.GET.get('numero_equipos_por_grupo'))
                generar_grupos_manual(categoria, numero_grupos, numero_equipos_por_grupo, equipos_aprobados, numero_rondas)
            
            equipos = []
            for equipo in equipos_aprobados:
                equipos.append(
                    {
                        'id': equipo.pk,
                        'nombre': "{} ({} - {})".format(equipo.nombre.title(),equipo.departamento.nombre[:3].title(), equipo.municipio.nombre[:3].title()),
                    }
                )
            grupos = []
            for grupo in Grupo.objects.filter(categoria=categoria):
                grupos.append(
                    {
                        'id': grupo.pk,
                        'nombre': grupo.nombre.title(),
                        'numero_equipos': [x for x in range(0, grupo.numero_equipos)],
                    }
                )
            return JsonResponse({'pk':categoria.pk, 'equipos': equipos ,
                                    'grupos': grupos,'mensaje': "Grupos generados satisfactoriamente. Procede a asignacion manual"})

            # except:
            #     print("No puedo borrar grupos")
            #     return JsonResponse({'mensaje': "Imposible generar grupos. Partidos ya creados"})


class CategoriaGenerarGruposAutomatico(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            # print("Traigo :", request.GET.get('pk_categoria'))
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))
            equipos_aprobados = Equipo.objects.filter(categoria=categoria, aprobado = True)
            recomendacion = True if request.GET.get('recomendacion') == 'si' else False
            locacion = True if request.GET.get('locacion') == 'si' else False
            numero_rondas = int(request.GET.get('numero_rondas_grupos'))
            print("el numero de rondas es: ",numero_rondas)

            # print("LA CATEGORIA ES ", categoria)form-manual
            # try:
            if Grupo.objects.filter(categoria=categoria).exists():
                print("YA tiene Grupos creados")
                Grupo.objects.filter(categoria=categoria).delete()
            else:
                print("NO tiene grupos creados")

            if recomendacion:
                print("aprobo recomendacion")
                generar_grupos_auto(categoria, numero_rondas)
            else:    
                print("NO aprobo recomendacion")
                numero_grupos = int(request.GET.get('numero_grupos'))
                numero_equipos_por_grupo = int(request.GET.get('numero_equipos_por_grupo'))
                generar_grupos_manual(categoria, numero_grupos, numero_equipos_por_grupo, equipos_aprobados, numero_rondas)

            grupos = Grupo.objects.filter(categoria=categoria)
            asignacion_grupos_auto(equipos_aprobados,grupos,locacion)

            grupos_lista = []
            for grupo in Grupo.objects.filter(categoria=categoria):
                equipos_lista = []
                for equipo in grupo.equipos.all():
                    equipos_lista.append(
                        {
                            'id': equipo.pk,
                            'nombre': "{} ({} - {})".format(equipo.nombre.title(),equipo.departamento.nombre[:3].title(), equipo.municipio.nombre[:3].title()),
                        }
                    )
                grupos_lista.append(
                    {
                        'id': grupo.pk,
                        'nombre': grupo.nombre.title(),
                        'equipos': equipos_lista
                    }
                )
            return JsonResponse({'pk': categoria.pk,'grupos': grupos_lista,
                                'mensaje': "Asignacion automatica de equipos generada"})

            # except:
            #     print("No puedo borrar grupos")
            #     return JsonResponse({'mensaje': "Imposible generar grupos. Partidos ya creados"})

class CategoriaFases(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            # print("Traigo :", request.GET.get('pk_categoria'))
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))
            fases = []
            for partido in Partido.objects.filter(categoria=categoria):
                print("llego con las sgtes fases: ", fases)
                print("tengo el partido {} con fase {}".format(partido, partido.fase))
                if partido.fase not in fases:
                    fases.append(partido.fase)
            return JsonResponse({'fases':fases})

class FasePartidos(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            # print("Traigo :", request.GET.get('pk_categoria'))
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))
            fase = request.GET.get('fase')
            print("La fase recogida es ", fase)
            partidos = []
            if fase == "Grupos":
                for partido in Partido.objects.filter(categoria=categoria, fase=fase):
                    partido_fase = {
                        'pk': partido.pk,
                        'categoria' : partido.categoria.nombre,
                        'fase' : partido.fase,
                        'grupo' : partido.grupo.nombre,
                        'local' : partido.local.nombre,
                        'visitante' :partido.visitante.nombre,
                        'estado': partido.estado
                    }
                    partidos.append(partido_fase)
            else:
                for partido in Partido.objects.filter(categoria=categoria, fase=fase):
                    partido_fase = {
                        'pk': partido.pk,
                        'categoria' : partido.categoria.nombre,
                        'fase' : partido.fase,
                        'grupo' : "----------",
                        'local' : partido.local.nombre,
                        'visitante' :partido.visitante.nombre,
                        'estado': partido.estado
                    }
                    partidos.append(partido_fase)
            return JsonResponse({'partidos':partidos})

class GruposCreadosView(TemplateView):

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            categoria = Categoria.objects.get(pk=request.GET.get('pk_categoria'))

            grupos = []
            grupos_cat = Grupo.objects.filter(categoria=categoria)
            if grupos_cat.count() > 0:
                for grupo in grupos_cat:
                    grupos.append(
                        {
                            'id': grupo.pk,
                            'nombre': grupo.nombre.title(),
                            'equipos': list(grupo.equipos.values_list('id', 'nombre','departamento__nombre','municipio__nombre')),
                            'numero_equipos': grupo.numero_equipos
                        }
                    )

                respuesta = {'grupos':grupos, 'grupos_creados': True}

                print("CategoriaProducto View: ", respuesta)
            else:
                print("No hay equipos para visualizar")
                respuesta = {'grupos_creados': False}
                print("Se responde los siguiente: ", respuesta)

        return JsonResponse(respuesta, safe=False)

#endregion


#region INTEGRANTES VIEWS
class IntegranteCreate( CreateView ):
    model = Contacto
    fields = '__all__'
    template_name = 'integrante/create.html'
    success_url = reverse_lazy('integrante-list')

class IntegranteUpdate( UpdateView ):
    model = Contacto
    fields = ['nombres','apellidos','sexo','telefono','documento','tipo_identificacion','talla','edad']
    template_name = 'integrante/update.html'
    # success_url = reverse_lazy('integrante-list')

    def get_success_url(self):
        return reverse_lazy('integrante-list', kwargs={'pk':self.object.equipo.pk})

class TecnicoUpdate( UpdateView ):
    model = Contacto
    fields = ['nombres','apellidos','telefono','documento','tipo_identificacion','email']
    template_name = 'integrante/update_tecnico.html'
    # success_url = reverse_lazy('integrante-list')

    def get_success_url(self):
        return reverse_lazy('integrante-list', kwargs={'pk':self.object.equipo.pk})
    
class IntegranteList(View ):

    def get (self, request, pk, *args, **kwargs):
        # context = super().get_context_data(**kwargs)
        pk_equipo = self.kwargs['pk']
       
        equipo = Equipo.objects.get(pk = pk_equipo)
        print("El equipo es", equipo)
        integrantes = Contacto.objects.filter(equipo=equipo)
        print("integrantes", integrantes)
        jugadores = integrantes.filter(es_jugador=True, activo=True)
        tecnicos = integrantes.filter(es_jugador=False, activo=True)
        # context['jugadores'] = jugadores
        tipos_id= TipoIdentificacion.objects.all()
        # context['now'] = timezone.now()
        print("Jugadores ", jugadores)
        print("Tecnicos ", tecnicos)
        
        template = 'integrante/list.html'
        return render(self.request, template, {'jugadores': jugadores, 'tecnicos': tecnicos,'tipos_id':tipos_id, 'pk_equipo':pk_equipo })
    
    def post(self, request, pk,*args, **kwargs):
        print('XXXXXXXXXXXXXXX',request.POST)
        nombres_integrantes=self.request.POST.get('Nombres_llenar')
        apellidos_integrantes=self.request.POST.get('Apellidos_llenar')
        tipodoc_integrantes=self.request.POST.get('id_tipo_llenar')
        doc_integrantes=self.request.POST.get('Identificación_llenar')
        tel_integrantes=self.request.POST.get('tel_jugador_llenar')
        edad_integrantes=self.request.POST.get('edad_integrante')
        genero_integrantes=self.request.POST.get('genero_llenar')
        talla_integrantes=self.request.POST.get('talla_llenar')
        correo_integrantes=self.request.POST.get('email_llenar')
        rol_integrantes=self.request.POST.get('rol_llenar')
        equipo_integrantes=self.request.POST.get('equipo_integrante')

        
        if rol_integrantes=='1':
                jugador=True
                correo=''
                if genero_integrantes=='1':
                    genero = 'Masculino'
                elif (genero_integrantes=='2'):
                    genero = 'Femenino'
        else:
            jugador=False
            correo=correo_integrantes
            genero=""

        if genero_integrantes=='1':
            genero = 'Masculino'
        elif (genero_integrantes=='2'):
            genero = 'Femenino'

        contacto = Contacto(
            nombres=nombres_integrantes.title(),
            apellidos=apellidos_integrantes.title(),
            telefono=tel_integrantes,
            es_jugador=jugador,
            equipo=Equipo.objects.get(pk=equipo_integrantes, activo=True),
            sexo=genero,
            edad=edad_integrantes,
            tipo_identificacion=TipoIdentificacion.objects.get(pk=tipodoc_integrantes),
            documento=doc_integrantes,
            talla=talla_integrantes,
            email = correo,
            activo= True
        )
        contacto.save()
        return HttpResponseRedirect(reverse_lazy('integrante-list', kwargs={'pk':contacto.equipo.pk}))

        # return HttpResponseRedirect(reverse_lazy('equipo-list'))

class IntegranteDelete( DeleteView):
    model = Contacto
    success_url = reverse_lazy('integrante-list')

    def get_success_url(self):
        return reverse_lazy('integrante-list', kwargs={'pk':self.object.equipo.pk})

class TecnicoDelete( DeleteView):
    model = Contacto
    success_url = reverse_lazy('integrante-list')

    def get_success_url(self):
        return reverse_lazy('integrante-list', kwargs={'pk':self.object.equipo.pk})

class AdminJugadoresList(ListView):
    model = Contacto
    paginate_by = 20
    template_name = 'admin/jugadores_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre

        context['jugadores'] = Contacto.objects.filter(
            Q(activo = True, es_jugador = True, equipo__isnull = False) 
            & Q(equipo__evento = evento)
        )
        return context

class AdminTecnicosList(ListView):
    model = Contacto
    paginate_by = 20
    template_name = 'admin/tecnicos_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre

        context['tecnicos'] = Contacto.objects.filter(
            Q(activo = True, es_jugador = False, equipo__isnull = False) 
            & Q(equipo__evento = evento)
        )
        return context       

class JugadoresEquipos(View):
        def get(self, request, *args, **kwargs):
            equipo_pk = request.GET.get('pk_equipo')
            try:
                print('PK: ',equipo_pk)
                print("El equipo es: ", Equipo.objects.get(pk=equipo_pk))
                if (Equipo.objects.filter(pk=equipo_pk).exists()):
                    print("El quipo existe")
                    equipo = Equipo.objects.get(pk=equipo_pk)
                    print("El equipo es ", equipo)
                    jugadores_queryset = Contacto.objects.filter(equipo=equipo, es_jugador=True)

                    jugadores_list = []
                    for jugador in jugadores_queryset:
                        jugadores_list.append(
                            {
                                'documento': jugador.documento,
                                'nombres': jugador.nombres,
                                'apellidos': jugador.apellidos,
                            }
                        )
                    respuesta = {'jugadores':jugadores_list}

                    print("EquiposLocal View: ", respuesta) 

                return JsonResponse(respuesta, safe=False)
            
            except:
                return JsonResponse(respuesta, safe=False)

class PartidoJugadoresEquiposVer(View):
        def get(self, request, *args, **kwargs):
            pk_partido = request.GET.get('pk_partido')
            # try:
            print('Triago PK del partido: ',pk_partido)
            print("El partido es: ", Partido.objects.get(pk=pk_partido))
            if (Partido.objects.filter(pk=pk_partido).exists()):
                print("El partido existe")
                partido = Partido.objects.get(pk=pk_partido)
                partido_jugadores = partido.jugadores.all()
                partido_jugadores_local = partido_jugadores.filter(local=True)
                partido_jugadores_visitante = partido_jugadores.filter(local=False)

                jugadores_local = []
                for jugador in partido_jugadores_local:
                    jugadores_local.append(
                        {
                            'documento': jugador.jugador.documento,
                            'nombres': jugador.jugador.nombres,
                            'apellidos': jugador.jugador.apellidos,
                            'gol_partido': jugador.gol_partido,
                            'amarilla_partido':jugador.amarilla_partido,
                            'roja_partido': jugador.roja_partido,
                            'numero' : jugador.numero
                        }
                    )

                jugadores_visitante = []
                for jugador in partido_jugadores_visitante:
                    jugadores_visitante.append(
                        {
                            'documento': jugador.jugador.documento,
                            'nombres': jugador.jugador.nombres,
                            'apellidos': jugador.jugador.apellidos,
                            'gol_partido': jugador.gol_partido,
                            'amarilla_partido':jugador.amarilla_partido,
                            'roja_partido': jugador.roja_partido,
                            'numero' : jugador.numero
                        }
                    )
                respuesta = {'jugadores_local':jugadores_local,'jugadores_visitante':jugadores_visitante }

                print("EquiposLocal View: ", respuesta) 

            return JsonResponse(respuesta, safe=False)
            
            # except:
            #     return JsonResponse(respuesta, safe=False)

#endregion

class MunicipiosDepartamento(View):

    def get(self, request, *args, **kwargs):
        depto_pk = request.GET.get('depto_pk')
        print('PK: ',depto_pk)
        if (Departamento.objects.filter(pk=depto_pk).exists()):
            departamento = Departamento.objects.get(pk=depto_pk)
            municipios_queryset = Municipio.objects.filter(departamento=departamento)

        municipios_list = []
        for municipio in municipios_queryset:
            municipios_list.append(
                {
                    'pk': municipio.pk,
                    'nombre': municipio.nombre,
                }
            )
        respuesta = {'municipios':municipios_list}

        print("CategoriaProducto View: ", respuesta)
        return JsonResponse(respuesta, safe=False)
