from apps.campeonato.models import Grupo, JugadoresPartido, Partido, Abono, Equipo
from django.contrib import admin


@admin.register(Partido)
class PartidoAdmin(admin.ModelAdmin):
    list_display = ('pk', 'fase','estado','local', 'visitante', 'fecha','ganador', 'grupo')

@admin.register(JugadoresPartido)
class JugadoresPartidoAdmin(admin.ModelAdmin):
    list_display = ('pk', 'jugador', 'gol_partido', 'amarilla_partido','roja_partido')

@admin.register(Grupo)
class GrupoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'categoria', 'numero_equipos')

@admin.register(Abono)
class AbonoAdmin(admin.ModelAdmin):
    list_display = ('pk', 'equipo', 'valor', 'fecha','archivo')

@admin.register(Equipo)
class EquipoAdmin(admin.ModelAdmin):
    list_display = ('id','nombre', 'categoria')