from django.contrib.auth.models import User
from django.db import models
from django.urls.base import reverse
from typing import Any

from apps.contactos.models import Contacto, Departamento, Municipio
from apps.evento.models import Categoria, Evento

class JugadoresPartido(models.Model):
    jugador = models.ForeignKey(Contacto, models.DO_NOTHING)
    gol_partido = models.IntegerField(blank=True, null=True)
    amarilla_partido = models.IntegerField(blank=True, null=True)
    roja_partido = models.IntegerField(blank=True, null=True)
    numero = models.IntegerField(blank=True, null=True)
    local = models.BooleanField(default=True)
    evento = models.ForeignKey(Evento, models.DO_NOTHING)

    class Meta:
        ordering = ['-id']
        verbose_name = 'Jugador_Partido'
        verbose_name_plural = 'Jugadores_Partido'

    def __str__(self):
        return "{}".format(self.jugador)

class Equipo(models.Model):
    nombre = models.CharField(max_length=512)
    categoria = models.ForeignKey(Categoria, models.DO_NOTHING, blank=True, null=True)
    municipio = models.ForeignKey(Municipio, models.DO_NOTHING, blank=True, null=True)
    departamento = models.ForeignKey(Departamento, models.DO_NOTHING, blank=True, null=True)
    observaciones = models.TextField(default="", blank=True, null=True)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="equipo_creado_por")
    abono_inscripcion=models.PositiveIntegerField(blank=True, null=True ,default=0)
    saldo_inscripcion = models.PositiveIntegerField(blank=True, null=True ,default=1500000)
    activo = models.BooleanField(blank=True, default=True)
    aprobado = models.BooleanField(blank=True, null=True)
    evento = models.ForeignKey(Evento, models.DO_NOTHING)

    def obtener_jugadores_equipo(self):
        return Contacto.objects.filter(equipo=self, es_jugador=True)

    def obtener_cuerpo_tecnico_equipo(self):
        return Contacto.objects.filter(equipo=self, es_jugador=False)

    def __str__(self):
        return '{}'.format(self.nombre)

class Abono(models.Model):
    equipo = models.ForeignKey(Equipo, models.DO_NOTHING)
    valor = models.PositiveIntegerField()
    fecha = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    archivo = models.FileField(upload_to='static/campeonato', blank=True, null=True)
    evento = models.ForeignKey(Evento, models.DO_NOTHING)

    def __str__(self):
        return '{} {}'.format(self.equipo, self.valor)

    class Meta:
        ordering = ['id']
        verbose_name = 'Abono'
        verbose_name_plural = 'Abonos'

class Grupo(models.Model):
    nombre = models.CharField(max_length=512)
    categoria = models.ForeignKey(Categoria, models.CASCADE)
    equipos = models.ManyToManyField(Equipo, related_name="Integrantes")
    numero_equipos = models.PositiveIntegerField(default=0)
    numero_clasificados = models.PositiveIntegerField(default=2)
    equipos_clasificados = models.ManyToManyField(Equipo, related_name="Clasificados")
    evento = models.ForeignKey(Evento, models.DO_NOTHING)
    numero_rondas = models.PositiveIntegerField(default=1)

    class Meta:
        ordering = ['id']
        verbose_name = 'Grupo'
        verbose_name_plural = 'Grupos'

    def __str__(self):
        return " {} Grupo {}".format(self.categoria, self.pk)

class Partido(models.Model):
    numero_partido = models.PositiveIntegerField(blank=True, null=True)
    fase = models.CharField(max_length=512, blank=True, null=True)
    categoria = models.ForeignKey(Categoria, models.DO_NOTHING)
    local = models.ForeignKey(Equipo, models.DO_NOTHING, related_name="Equipo_Local")
    visitante = models.ForeignKey(Equipo, models.DO_NOTHING, related_name="Equipo_Visitante")
    arbitro1 = models.CharField(max_length=512, blank=True, null=True)
    arbitro2= models.CharField(max_length=512, blank=True, null=True)
    arbitro3= models.CharField(max_length=512, blank=True, null=True)
    cronogramista = models.CharField(max_length=512, blank=True, null=True)
    coordinador = models.CharField(max_length=512, blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True)
    ganador = models.ForeignKey(Equipo, models.DO_NOTHING, blank=True, null=True, related_name="Equipo_Ganador")
    jugadores = models.ManyToManyField(JugadoresPartido)
    soporte = models.FileField(upload_to='static/planillas', blank=True, null=True)
    grupo = models.ForeignKey(Grupo, models.DO_NOTHING, blank=True, null=True)
    estado = models.BooleanField(default=False)
    evento = models.ForeignKey(Evento, models.DO_NOTHING)

    class Meta:
        ordering = ['-id']
        verbose_name = 'Partido'
        verbose_name_plural = 'Partidos'

    def __str__(self):
        return "{} {}".format(self.categoria, self.numero_partido)

class ActivarClasificados(models.Model):
    fase = models.CharField(max_length=512, blank=True, null=True)
    activar = models.BooleanField(default=False)