from django.urls import include, path
from . import views
from apps.campeonato import views

urlpatterns = [
    # URLS de Usuario Administrador Equipos
    path('',                                        views.IndexView.as_view(),                  name='index'),
    path('mis-equipos/',                            views.EquipoList.as_view(),                 name='equipo-list'),
    path('evento/<int:pk>/inscribirme',             views.EquipoCreate.as_view(),               name='equipo-create'),
    path('planilla/departamento/municipio/list/',   views.MunicipiosDepartamento.as_view(),     name='depto-municipios'),
    path('equipo/<int:pk>/integrantes/',            views.IntegranteList.as_view(),             name='integrante-list'),
    path('equipo/jugador/update/<int:pk>',          views.IntegranteUpdate.as_view(),           name='integrante-update'),
    path('equipo/jugador/delete/<int:pk>',          views.IntegranteDelete.as_view(),           name='integrante-delete'),
    path('equipo/tecnico/update/<int:pk>',          views.TecnicoUpdate.as_view(),              name='tecnico-update'),
    path('equipo/tecnico/delete/<int:pk>',          views.TecnicoDelete.as_view(),              name='tecnico-delete'),
    path('equipo/<int:pk>/actualizar-info',         views.EquipoUpdate.as_view(),               name='equipo-update'),
    path('equipo/<int:pk>/borrar',                  views.EquipoDelete.as_view(),               name='equipo-delete'),

    #URLs de Usuario Administrador de Eventos
    path('evento/<int:pk>/',                    views.AdminFaseInicial.as_view(),                   name="campeonato" ),
    path('verintegrantes/',                     views.VerIntegrantes.as_view(),                     name="ver-integrantes" ),
    path('aprobar-equipo',                      views.AprobarEquipo.as_view(),                      name='aprobar-equipo'),
    path('rechazar-equipo',                     views.RechazarEquipo.as_view(),                     name='rechazar-equipo'),
    path('categoria/grupos/recomendacion',      views.RecomendacionGrupos.as_view(),                name='recomendacion-grupos'),
    path('equipos/grupos/manual',               views.EquiposGrupoManual.as_view(),                 name="equipos-grupo-manual"),
    path('categoria/generacion/grupos/auto',    views.CategoriaGenerarGruposAutomatico.as_view(),   name="grupos-auto"),
    path('categoria/sorteo/liguilla',           views.CategoriaGenerarLiguilla.as_view(),           name="categoria-generar-liguilla"),
    path('categoria/generacion/grupos/manual',  views.CategoriaGenerarGruposManual.as_view(),       name="grupos-manual"),
    path('categoria/grupos/ver',                views.VerDistribucionGrupos.as_view(),              name='ver-grupos'),
    path('categoria/partidos/generar',          views.PartidoGenerar.as_view(),                     name='partidos-generar'),
    path('categoria/fase/partidos',             views.FasePartidos.as_view(),                       name="fase-partidos"),
    path('categoria/fases',                     views.CategoriaFases.as_view(),                     name="categoria-fases"),

    path('partido/datos-consulta',              views.PartidoConsultaDatos.as_view(),               name='partido-datos-consulta'),
    path('equipos/jugadores/list/',             views.JugadoresEquipos.as_view(),                   name='equipos-jugadores'),
    path('planilla/equipos/jugadores/list',     views.PartidoJugadoresEquiposVer.as_view(),         name='partido-equipos-jugadores'),
    path('partidos/validar-clasificacion',      views.PartidoValidarClasificacion.as_view(),        name='partidos-validar-clasificacion'),
    path('partido/diligenciar',                 views.PartidoEdit.as_view(),                        name='partido-edit'),

    path('clasificacion/categoria',             views.ClasificacionCategoria.as_view(),             name='clasificacion-categoria'),

    path('estadisticas/categoria/equipos',      views.EstadisticasCategoriaEquipos.as_view(),       name='estadisticas-categoria-equipos'),
    path('estadisticas/categoria/jugadores',    views.EstadisticasCategoriaJugadores.as_view(),     name='estadisticas-jugadores'),




# RECICLADO
    # path('evento/<int:pk>/partidos/sorteo',                          views.PartidosGenerate.as_view(),      name='partido-generate'),
    # path('evento/<int:pk>/partidos/fixture',                         views.PartidoListView.as_view(),       name='partido-list-view'),
    # path('evento/<int:pk>/partidos/diligenciar',                     views.PartidoListEdit.as_view(),       name='partido-list-edit'),
    # path('partidos/detalle/planilla-<int:pk>',       views.PartidoListDetail.as_view(),     name='partido-list-detail'),
    # path('categoria/equipos/list/',   views.EquiposCategoria.as_view(),   name='categoria_equipos'),
    # path('partido/jugadores/list/<int:pk>/',   views.JugadoresPartidoList.as_view(),   name='jugadores-partido-list'),
    # path('partido/delete/<int:pk>',            views.PartidoDelete.as_view(),          name='partido-delete'),
    # path('evento/<int:pk>/clasificacion/ver',                   views.Clasificacion.as_view(),         name='clasificacion'),
    # path('evento/<int:pk>/estadisticas/equipos', views.EstadisticasEquipos.as_view(), name='estadisticas-equipos'),
    # path('evento/<int:pk>/estadisticas/jugadores', views.EstadisticasJugadores.as_view(), name='estadisticas-categoria-jugadores'),
    # # path('estadisticas/categoria', views.EstadisticasCategoria.as_view(), name='estadisticas-categoria'),
    # path('clasificacion/',  views.ClasificacionView.as_view(),  name='clas'),
    # path('equipo/estado',   views.EquipoEstado.as_view(),            name='equipo-estado'),
    # path('planilla/integrante/create/',          views.IntegranteCreate.as_view(),  name='integrante-create'),
    # path('evento/<int:pk>/equipos/',    views.AdminEquiposList.as_view(),    name="equipos-list" ),
    # path('evento/<int:pk>/jugadores/',  views.AdminJugadoresList.as_view(),  name="jugadores-list" ),
    # path('evento/<int:pk>/tecnicos/',   views.AdminTecnicosList.as_view(),   name="tecnicos-list" ),
    # path('versaldo/',        views.VerSaldo.as_view(),        name="ver-saldo" ),
    # path('evento/<int:pk>/grupos/ver',                 views.GruposView.as_view(),          name="grupos"),
    # path('evento/<int:pk>/grupos/sorteo',           views.GruposCreateView.as_view(),    name='grupos-sorteo'),
    # # path('categoria/sorteo/grupos',        views.CategoriaGenerarGrupos.as_view(),     name="grupos-categoria"),
    # path('grupos/categoria/list',   views.GruposCreadosView.as_view(),   name="grupos-categoria-list"),
    ]