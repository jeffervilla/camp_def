from apps import evento
from apps.contactos.models import *
from apps.campeonato.models import *
from django.db.models import Q
import itertools
import random
import string
'''
Funcion que crea un algoritmo de todos contra todos para los integrantes de un grupo,
retorna una lista con los enfrentamientos
'''
def round_robin(clubes):
    index_clubes = 0

    auxT = len(clubes)
    impar= True if auxT%2 != 0 else False

    if impar:
        auxT += 1

    totalP = int(auxT/2) # total de partidos de una jornada
    jornada = []
    indiceInverso = auxT-2

    for i in range(1,auxT):
        
        for indiceP in range(0,totalP):
            print("Partido ", indiceP+1)
            if index_clubes > auxT-2:
                index_clubes = 0

            if indiceInverso < 0:
                indiceInverso = auxT-2

            if indiceP == 0: # seria el partido inicial de cada fecha
                if impar:
                    jornada.append(clubes[index_clubes])
                else:
                    if (i+1)%2 == 0:
                        partido = [clubes[index_clubes], clubes[auxT-1]]
                        print("El partido queda: ", partido)
                    else:
                        partido = [clubes[auxT-1], clubes[index_clubes]]
                        print("El partido queda: ", partido)
                    jornada.append(partido)
            else:
                partido = [clubes[index_clubes], clubes[indiceInverso]]
                print("El partido queda: ", partido)
                jornada.append(partido)
                indiceInverso -= 1
            index_clubes += 1

    return jornada

# Funcion que retorna el parametro de ordenamiento de para el metodo sort de una lista
def parametro_ordenamiento(equipo):
    return equipo['pts']

'''
Funcion que retorna la lista de equipos ordenada de mayor a menor con primer parametro puntos
segundo parametro diferencia de gol
'''
def clasificacion_ordenada(equipos):
    equipos.sort(reverse=True, key=parametro_ordenamiento)
    for i in range(len(equipos)):
        # print("Tomo el equipo ", i+1)
        # print("El equipo actual es: {} con {} pts".format(equipos[i], equipos[i]['pts']))
        try:
            equipo_arriba = equipos[i]
            equipo_abajo = equipos[i+1]
            # print("El equipo siguiente es: {} con {} pts".format(equipos[i+1], equipos[i+1]['pts']))
            if equipos[i]['pts'] == equipos[i+1]['pts']:
                if equipos[i]['gd'] < equipos[i+1]['gd']:
                    equipos[i] = equipo_abajo
                    equipos[i+1] = equipo_arriba
        except:
            pass
    # print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
    # print("El listado de equipos ordenado por puntos es: ", equipos)
    # print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
    return equipos

'''
Funcion que retorna una lista con las estadisticas de cada uno de los equipos pertenecientes al
grupo
'''
def get_estadisticas_grupo(grupo):
    equipos = grupo.equipos.all()
    equipos_list= []
    for equipo in equipos:
        # print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||")
        # print("Tomo el equipo :", equipo)
        # print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||")
        jugados = 0
        ganados= 0
        empatados = 0
        perdidos = 0
        puntos = 0
        goles_a_favor = 0
        goles_en_contra = 0
        gol_diferencia = 0

        partidos = Partido.objects.filter(
            Q(local=equipo, estado = True, fase="Grupos") |
            Q(visitante=equipo, estado = True , fase="Grupos") 
        )
        # print("Los partidos de este equipo fueron: ", partidos)
        for partido in partidos:
            # print("Para este partido el ganador fue: ", partido.ganador)
            jugados += 1
            if partido.ganador == equipo:
                ganados += 1
            elif partido.ganador == None:
                empatados += 1
            else:
                perdidos += 1

            for jugador in partido.jugadores.all():
                # print("Revisamos cada jugador del partido para ver si fue GF o GC")
                # print("tomamos el jugador: ", jugador)
                if jugador.gol_partido > 0:
                    # print("Marco goles en el partido")
                    if jugador.jugador.equipo == equipo:
                        # print("Jugador pertenece al equipo. Goles a Favor")
                        goles_a_favor += jugador.gol_partido
                    else:
                        goles_en_contra += jugador.gol_partido
                        # print("Jugador no pertenece al equipo. Goles contra")
            gol_diferencia = goles_a_favor - goles_en_contra
            puntos = ganados*3 + empatados
        # print("Partidos Jugados: ", jugados)
        # print("Partidos Ganados: ", ganados)
        # print("Partidos Empatados: ", empatados)
        # print("Partidos Perdidos: ", perdidos)
        # print("Puntos: ", puntos)
        # print("GF: ", goles_a_favor)
        # print("GC: ", goles_en_contra)
        # print("GD: ", gol_diferencia)
        equipo_json = {
            'equipo': equipo.nombre,
            'pj': jugados,
            'pg': ganados,
            'pe': empatados,
            'pp': perdidos,
            'gf': goles_a_favor,
            'gc': goles_en_contra,
            'gd': gol_diferencia,
            'pts': puntos
            }
        # print("el JSON a reotrnar del equipo es: ", equipo_json)
        equipos_list.append(equipo_json)
        # print("El Listado de equipos temporal a enviar es:" , equipos_list)
    print("El Listado de equipos a enviar del grupo {}  es: {}".format(grupo.nombre.title(), equipos_list))
    return clasificacion_ordenada(equipos_list)

#################################################################################################

fases = ['Grupos', 'Dieciseisavos','Octavos','Cuartos','Semis','Final']

#Detecto en que fase se encuentra el campeonato
def validar_fase(categoria):
    for fase in fases:
        if Partido.objects.filter(categoria=categoria, estado=True, fase=fase).exists():
            print("Existen partidos de la categoria {} con fase: {}".format(categoria,fase))
            fase_categoria = fase
        else:
            print("NOO Existen partidos de la categoria {} con fase: {}".format(categoria,fase))
    return fase_categoria


def nro_grupos_en_categoria(categoria):
    return Grupo.objects.filter(categoria=categoria).count()

#Retorno una lista con el primer elemento que son los clasificados directos y el segundo los adicionales
def nro_equipos_clasificados_fase_grupos(nro_grupos_en_categoria):
    factores_creacion_llaves = [4,8,16]
    clasificados_iniciales = nro_grupos_en_categoria * 2
    for factor in factores_creacion_llaves:
        if factor - clasificados_iniciales == 0:
            return [factor,0]
        elif factor - clasificados_iniciales < 0:
            pass
        elif factor - clasificados_iniciales > 0:
            clasificados_adicionales = factor - clasificados_iniciales
            return [clasificados_iniciales, clasificados_adicionales]

#Retorno una lista con los equipos clasificados: los directos y los mejores equipos adicionales
def clasificados_por_categoria_en_grupos(clasificados_adicionales, categoria):
    grupos = Grupo.objects.filter(categoria=categoria)
    primer_lugar = []
    segundo_lugar = []
    tercer_lugar = []
    cuarto_lugar =  []

    for grupo in grupos:
        grupo_ordenado = get_estadisticas_grupo(grupo)
        primer_lugar.append(grupo_ordenado[0])
        segundo_lugar.append(grupo_ordenado[1])
        tercer_lugar.append(grupo_ordenado[2])
        try:
            cuarto_lugar.append(grupo_ordenado[3])
            print("Los clasificados en cuarto lugar son: ", cuarto_lugar)
        except:
            print("El grupo no cuenta con 4to lugar")
    
    # print("Los clasificados en primer lugar son: ", primer_lugar)
    # print("Los clasificados en segundo lugar son: ", segundo_lugar)
    # print("Los clasificados en tercer lugar son: ", tercer_lugar)
    
    # print("Aqui comienzo a ordenar para obtener los clasificados adicionales")

    clasificados_primeros = clasificacion_ordenada(primer_lugar)
    clasificados_segundos = clasificacion_ordenada(segundo_lugar)
    clasificados_terceros = clasificacion_ordenada(tercer_lugar)

    # print("Los clasificados ordenados en primer lugar son: ", clasificados_primeros)
    # print("Los clasificados ordenados en segundo lugar son: ", clasificados_segundos)
    # print("Los clasificados ordenados en tercer lugar son: ", clasificados_terceros)

    try:
        clasificados_cuartos = clasificacion_ordenada(cuarto_lugar)
        # print("Los clasificados ordenados en cuarto lugar son: ", clasificados_cuartos)
    except:
        print("El grupo no cuenta con 4tos lugares")

    try:
        clasificados_terceros = clasificacion_ordenada(tercer_lugar)[:clasificados_adicionales]
        clasificados_cuartos = []
        # print("alcanza con los terceros para los clasifiacdos adicionales")
        # print("Los clasificados terceros definitivos son: ", clasificados_terceros)
    except:
        print("El numero de clasificados adicionales supera al numero de terceros clasificados, tomamos los cuartos")
        clasificados_cuartos[:1]

    try:
        clasificados = clasificados_primeros + clasificados_segundos +clasificados_terceros + clasificados_cuartos
        # print("Claificados incluidos los cuartos lugares")
    except:
        print("No tengo una lista de cuartos puestos para agregar")
        clasificados = clasificados_primeros + clasificados_segundos + clasificados_terceros

    return clasificados

#Retoorna una lista con los equipos clasificados de la fase seleccionada
def clasificados_por_categoria_fase(fase, categoria):
    clasificados = []
    partidos = Partido.objects.filter(categoria=categoria, estado=True,fase=fase)
    for partido in partidos:
        clasificados.append(partido.ganador) 
    return clasificados

#detecto la fase de un campeonato y genero los partidos de la siguiente fase
def generar_partidos_fase(clasificados, categoria):
    fases = {32:'Dieciseisavos' , 16:'Octavos' , 8:'Cuartos' , 4:'Semis', 2:'Final'}
    print("Los clasificados que traigo son: ", clasificados)
    numero_clasificados = len(clasificados)
    fase = fases[numero_clasificados]
    print("@@222222222222qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq")
    print("La fase que llevan los siguientes partidos es:", fase)
    for i in range(numero_clasificados//2):
        try:
            local = Equipo.objects.get(categoria=categoria, nombre=clasificados[i]['equipo'])
            visitante = Equipo.objects.get(categoria=categoria, nombre=clasificados[len(clasificados)-1-i]['equipo'])
            print("Asigno equipos local y visitante para fase grupos")
        except:
            print("asigno para fase diferente de grupos")
            local = clasificados[i]
            visitante = clasificados[len(clasificados)-1-i]

        partido = Partido(
            local=local,
            visitante= visitante,
            categoria=categoria,
            fase= fase,
            evento=categoria.evento
        )
        partido.save()
        print("Partido creado")
    
#Retorno una lista con la cantidad de equipos que deberian crearse en el siguiente orden 
#[numero de grupos de 4 equipos, numero de grupos de 3 equipos, nmero de grupos de 5 equipos]
def numero_grupos(numero_equipos):

    if numero_equipos < 10:
        factores = [4,3,5]
        numero_grupos_factores = [0,0,0]
        resto = 0
        numero_grupos = 0
        grupos = []
        multiplicador = [1,2,3,4,5,6,7,8]

        #Recorremos cada uno de los factores
        for j in range(len(factores)):
            # print("factor ", factores[j])
            if numero_equipos % factores[j] == 0:
                numero_equipos_grupo = factores[j]
                numero_grupos = numero_equipos // numero_equipos_grupo
                numero_grupos_factores[j]=numero_grupos
                # print("El numero de grupos es: ", numero_grupos_factores)
                return numero_grupos_factores

            else:
                for mult in multiplicador:
                    indicador = factores[j] * mult
                    if indicador < numero_equipos:
                        resto = numero_equipos - indicador
                        if resto % 3 == 0:
                            grupos3 = resto // 3
                            numero_grupos_factores = [mult,grupos3,0]
                            # print("Los grupos son ", numero_grupos_factores  )
                            return numero_grupos_factores

                        elif resto % 5 == 0:
                            grupos5 = resto // 5
                            numero_grupos_factores = [mult,0,grupos5]
                            # print("Los grupos son ", numero_grupos_factores  )
                            return numero_grupos_factores

    elif numero_equipos == 11:
        numero_grupos_factores= [2,1,0]
        return numero_grupos_factores

    elif numero_equipos == 10:
        numero_grupos_factores= [0,0,2]
        return numero_grupos_factores
                            
    else:
        factores=[4,5]
        numero_grupos_factores = [0,0]
        resto = 0
        numero_grupos = 0
        grupos = []
        multiplicador = [1,2,3,4,5,6,7,8]

        for j in range(len(factores)):
            # print("factor ", factores[j])
            if numero_equipos % factores[j] == 0:
                numero_equipos_grupo = factores[j]
                numero_grupos = numero_equipos // numero_equipos_grupo
                numero_grupos_factores[j]=numero_grupos
                # print("El numero de grupos es: ", numero_grupos_factores)
                return numero_grupos_factores

            else:
                for mult in multiplicador:
                    indicador = factores[j] * mult
                    if indicador < numero_equipos:
                        resto = numero_equipos - indicador
                        if resto % 5 == 0:
                            grupos5 = resto // 5
                            numero_grupos_factores = [mult,grupos5]
                            # print("Los grupos son ", numero_grupos_factores  )
                            return numero_grupos_factores
                        else:
                            pass

#Retorna el numero necesario de equipos clasificados para formar la siguiente fase
def nro_clasificados(nro_grupos):
    opciones_clasf = [4,8,16,32]
    nro_clasificados_grupo = nro_grupos * 2
    for opt in opciones_clasf:
        if nro_clasificados_grupo < opt:
            clasificados = opt
        elif opt == nro_clasificados_grupo:
            clasificados = opt
            break
        else:
            pass

#Crea los gruspo de acuerdo al algooritmo de creacion de grupos automatico    
def generar_grupos_auto(categoria, rondas):
    print("Entro a generar grupos auto *************************")
    equipos_aprobados = Equipo.objects.filter(categoria=categoria, aprobado = True)
    cantidad_equipos_aprobados = equipos_aprobados.count()
    print("la cantidad de equipos aprobados de la categoria ", equipos_aprobados)
    print("TENGO {} EQUIPOS".format(cantidad_equipos_aprobados))
    if cantidad_equipos_aprobados > 5:
        print("Cantiddad de equipos maayor que cinco")
        equipos_aprobados_lista = list(equipos_aprobados)
        # print("El listado de equipos original es: ", equipos_aprobados_lista)
        random.shuffle(equipos_aprobados_lista)
        print("Lista de equipos desordenada ", equipos_aprobados_lista)
        nombres_grupo = list(string.ascii_uppercase)

        distribucion_grupos =numero_grupos(cantidad_equipos_aprobados)
        # print("distribucion de equipos quedo [grupos de 4, grupos de 3 y grupos de 5", distribucion_grupos)
        cantidad_grupos_total = sum(distribucion_grupos)
        # print("la cantidad de grupos total es ", cantidad_grupos_total)
        asignacion_grupos=[]

        for i in range(len(distribucion_grupos)):
            for j in range(distribucion_grupos[i]):
                if i == 0:
                    asignacion_grupos.append(4)
                elif i == 1:
                    if len(distribucion_grupos)==2:
                        asignacion_grupos.append(5)
                    else :
                        asignacion_grupos.append(3)
                else:
                    asignacion_grupos.append(5)
        
        # print("La asignacion de grupos sera : ", asignacion_grupos)

        for i in range(cantidad_grupos_total):
            grupo = Grupo(
                nombre = "Grupo {}".format(nombres_grupo[i]),
                categoria = categoria,
                numero_equipos = asignacion_grupos[i],
                evento = categoria.evento,
                numero_rondas = rondas
            )
            grupo.save()

        print("Se crearon {} grupos de la categoria {}".format(cantidad_grupos_total, categoria))

#Crea los grupos de acuerdo a los parametros ingresados por el usuario
def generar_grupos_manual(categoria, numero_grupos , numero_equipos_por_grupo, equipos_aprobados, rondas):
    print("Entro a generar grupos manual *************************")
    print("numero grupos ", numero_grupos)
    print("numero de equipos por grupo ", numero_equipos_por_grupo)

    distribucion_grupos=[]
    cantidad_equipos_aprobados = equipos_aprobados.count()
    print("la cantidad de equipos aprobados de la categoria ", equipos_aprobados)
    print("TENGO {} EQUIPOS".format(cantidad_equipos_aprobados))
    if cantidad_equipos_aprobados > 5:
        print("Cantiddad de equipos maayor que cinco")
        equipos_aprobados_lista = list(equipos_aprobados)
        # print("El listado de equipos original es: ", equipos_aprobados_lista)
        random.shuffle(equipos_aprobados_lista)
        print("Lista de equipos desordenada ", equipos_aprobados_lista)
        nombres_grupo = list(string.ascii_uppercase)
        equipos_esperados = numero_grupos*numero_equipos_por_grupo
        
        #Generamos la cantiddad de grupos de X equipos
        #distribucion_grupos = [2] -> tiene 2 grupos de la misma cantidad de equipos
        #distribucion_grupos = [2,1] -> tiene 2 grupos de una cantidad de equipos y 1 de una cantidad diferente menor
        #distribucion_grupos = [2,3] -> tiene 2 grupos de una cantidad de equipos y 3 de una cantidad diferente mayor

        if equipos_esperados == cantidad_equipos_aprobados:
            print("equipos exactos con numero de grupos")
            distribucion_grupos = [numero_grupos]
        elif equipos_esperados < cantidad_equipos_aprobados:
            print("necesario sumar equipos a algunos grupos")
            diferencia = cantidad_equipos_aprobados - equipos_esperados
            distribucion_grupos = [numero_grupos-diferencia, diferencia]
        elif equipos_esperados > cantidad_equipos_aprobados:
            print("necesario sustraer equipos a algunos grupos")
            diferencia = equipos_esperados - cantidad_equipos_aprobados
            distribucion_grupos = [numero_grupos-diferencia, diferencia]
        
        print("distribucion de equipos quedo ", distribucion_grupos)


        cantidad_grupos_total = numero_grupos
        # print("la cantidad de grupos total es ", cantidad_grupos_total)
        asignacion_grupos=[]

        #genero una lista de la cantidad de equipos por grupo
        #si tengo 2 grupos de 3 equipos y 1 de 4 qeuedaria :  
        #asignacion_grupos = [3,3,4]

        for i in range(len(distribucion_grupos)):
            for j in range(distribucion_grupos[i]):
                if i == 0:
                    asignacion_grupos.append(numero_equipos_por_grupo)
                elif i == 1:
                    if equipos_esperados < cantidad_equipos_aprobados:
                        asignacion_grupos.append(numero_equipos_por_grupo+1)
                    else:
                        asignacion_grupos.append(numero_equipos_por_grupo-1)

        print("La asignacion de grupos sera : ", asignacion_grupos)

        #Recorro cada posicion de la asignacion de grupo y creo un grupo con esa cantidad de equipos
        for i in range(cantidad_grupos_total):
            grupo = Grupo(
                nombre = "Grupo {}".format(nombres_grupo[i]),
                categoria = categoria,
                numero_equipos = asignacion_grupos[i],
                evento = categoria.evento,
                numero_rondas = rondas
            )
            grupo.save()

        print("Se crearon {} grupos de la categoria {}".format(cantidad_grupos_total, categoria))

def asignacion_grupos_auto(equipos_aprobados, grupos, excluir_locacion):
    equipos_aprobados_lista = list(equipos_aprobados)
    random.shuffle(equipos_aprobados_lista)
    print("Lista de equipos desordenada ", equipos_aprobados_lista)
    print("########################################################################################")
    print("########################################################################################")
    print("########################################################################################")
    print("El ultimo equipo de la lista es: ", equipos_aprobados_lista[-1])

    for equipo in equipos_aprobados_lista:
        print("-------------------------------------------------------------")
        guardado_grupo = False
        print("El equipo seleccionado es: {} de {} {}".format(equipo.nombre, equipo.departamento, equipo.municipio))
        intentos = 0
        for grupo in grupos:
            print("-------------------------------------------------------------")
            cant_equipos_grupo = grupo.equipos.all().count()
            equipos_grupo =  grupo.equipos.all()
            print("tomo el grupo ", grupo.nombre)
            print("El grupo tiene {} equipos".format(cant_equipos_grupo))
            print("El numero de equipos que debe tener el grupo es :",grupo.numero_equipos)
            
            if grupo.numero_equipos > cant_equipos_grupo:
                print("Le faltan integrantes al grupo")
                print("Tiene {} intentos".format(intentos))
                if excluir_locacion  == False:
                        if guardado_grupo == False:
                            print("El grupo esta vacio. Guardo directo")
                            grupo.equipos.add(equipo)
                            grupo.save()
                            guardado_grupo = True
                            print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
                            # print("-------------------------------------------------------------")
                        break
                else:
                    if intentos ==0 and grupo != grupos[len(grupos)-1]:
                        print("Primer intento. Grupo diferente al ultimo")
                        if cant_equipos_grupo > 0:
                            print("el grupo no esta vacio")
                            print("Los equipos del grupo son {} : {}".format(cant_equipos_grupo, equipos_grupo))
                            igual_depto = False
                            for equipo_grupo in equipos_grupo:
                                print("Entro a comparar los departamentos y municipios")
                                if equipo.departamento == equipo_grupo.departamento:
                                    igual_depto = True
                                    print("Tiene el mismo departamento")
                                    break
                                print("Depto del equipo diferente de los dptos del grupo")
                            if igual_depto:
                                print("Entro a validar cuando los tienen los mismos deppartamentos")
                                intentos += 1
                                if grupo == grupos[grupos.count()-1] and cant_equipos_grupo+1 == grupo.numero_equipos:
                                    print("se forza a guardar, no hay mas equipos")
                                    grupo.equipos.add(equipo)
                                    grupo.save()
                                    guardado_grupo = True
                                    print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
                            else:
                                if guardado_grupo == False:
                                    print("se puede guardar")
                                    grupo.equipos.add(equipo)
                                    grupo.save()
                                    guardado_grupo = True
                                    print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
                                    # print("-------------------------------------------------------------")
                                break
                        else:
                            if guardado_grupo == False:
                                print("El grupo esta vacio. Guardo directo")
                                grupo.equipos.add(equipo)
                                grupo.save()
                                guardado_grupo = True
                                print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
                                # print("-------------------------------------------------------------")
                            break
                    elif intentos ==0 and grupo == grupos[len(grupos)-1]:
                        print("No Tengo intentos. Pero es el ultimo grupo")
                        if guardado_grupo == False:
                            print("El grupo esta vacio. Guardo directo")
                            grupo.equipos.add(equipo)
                            grupo.save()
                            guardado_grupo = True
                            print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
                            # print("-------------------------------------------------------------")
                        break

                    elif intentos >=0 and grupo != grupos[len(grupos)-1]:
                        print("Tengo mas de un intento. Pero no es el ultimo grupo")
                        if cant_equipos_grupo > 0:
                            print("el grupo no esta vacio")
                            print("Los equipos del grupo son {} : {}".format(cant_equipos_grupo, equipos_grupo))
                            igual_depto = False
                            for equipo_grupo in equipos_grupo:
                                print("Entro a comparar los departamentos y municipios")
                                if equipo.departamento == equipo_grupo.departamento:
                                    igual_depto = True
                                    print("Tiene el mismo departamento")
                                    break
                                print("Depto del equipo diferente de los dptos del grupo")
                            if igual_depto:
                                print("Entro a validar cuando los tienen los mismos deppartamentos")
                                intentos += 1
                                if grupo == grupos[grupos.count()-1] and cant_equipos_grupo+1 == grupo.numero_equipos:
                                    print("se forza a guardar, no hay mas equipos")
                                    grupo.equipos.add(equipo)
                                    grupo.save()
                                    guardado_grupo = True
                                    print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
                            else:
                                if guardado_grupo == False:
                                    print("se puede guardar")
                                    grupo.equipos.add(equipo)
                                    grupo.save()
                                    guardado_grupo = True
                                    print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
                                    # print("-------------------------------------------------------------")
                                break
                        else:
                            if guardado_grupo == False:
                                print("El grupo esta vacio. Guardo directo")
                                grupo.equipos.add(equipo)
                                grupo.save()
                                guardado_grupo = True
                                print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
                                # print("-------------------------------------------------------------")
                            break
                    
                    elif intentos >=0 and grupo == grupos[len(grupos)-1]:
                        print("Tengo mas de un intento y estoy en el ultimo grupo")
                        if guardado_grupo == False:
                            print("El grupo esta vacio. Guardo directo")
                            grupo.equipos.add(equipo)
                            grupo.save()
                            guardado_grupo = True
                            print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())



def asignacion_grupos_manual(categoria, equipos_aprobados):
    grupos = Grupo.objects.filter(categoria=categoria)
    
        
    #     boton = request.GET.get('boton')

    #     if boton == "manual":

    #         equipos = []
    #         for equipo in Equipo.objects.filter(categoria=categoria):
    #             equipos.append(
    #                 {
    #                     'id': equipo.pk,
    #                     'nombre': "{} ({} - {})".format(equipo.nombre.upper(),equipo.departamento.nombre[:3].upper(), equipo.municipio.nombre[:3].upper()),
    #                 }
    #             )
    #         grupos = []
    #         for grupo in Grupo.objects.filter(categoria=categoria):
    #             grupos.append(
    #                 {
    #                     'id': grupo.pk,
    #                     'nombre': grupo.nombre.upper(),
    #                     'numero_equipos': [x for x in range(0, grupo.numero_equipos)],
    #                 }
    #             )
    #         return JsonResponse({'pk':categoria.pk, 'equipos': equipos , 'grupos': grupos, 'equipos_creados':True})
        
    #     else:
    #         grupos = Grupo.objects.filter(categoria=categoria)
    #         print("########################################################################################")
    #         print("########################################################################################")
    #         print("########################################################################################")
    #         print("")
    #         print("El ultimo equipo de la lista es: ", equipos_cat_list[-1])

    #         for equipo in equipos_cat_list:
    #             print("-------------------------------------------------------------")
    #             guardado_grupo = False
    #             print("El equipo seleccionado es: {} de {} {}".format(equipo.nombre, equipo.departamento, equipo.municipio))
    #             intentos = 0
    #             for grupo in grupos:
    #                 print("-------------------------------------------------------------")
    #                 cant_equipos_grupo = grupo.equipos.all().count()
    #                 equipos_grupo =  grupo.equipos.all()
    #                 print("tomo el grupo ", grupo.nombre)
    #                 print("El grupo tiene {} equipos".format(cant_equipos_grupo))
    #                 print("El numero de equipos que debe tener el grupo es :",grupo.numero_equipos)
                    
    #                 if grupo.numero_equipos > cant_equipos_grupo:
    #                     print("Le faltan integrantes al grupo")
    #                     print("Tiene {} intentos".format(intentos))
    #                     if intentos ==0 and grupo != grupos[len(grupos)-1]:
    #                         print("Primer intento. Grupo diferente al ultimo")
    #                         if cant_equipos_grupo > 0:
    #                             print("el grupo no esta vacio")
    #                             print("Los equipos del grupo son {} : {}".format(cant_equipos_grupo, equipos_grupo))
    #                             igual_depto = False
    #                             for equipo_grupo in equipos_grupo:
    #                                 print("Entro a comparar los departamentos y municipios")
    #                                 if equipo.departamento == equipo_grupo.departamento:
    #                                     igual_depto = True
    #                                     print("Tiene el mismo departamento")
    #                                     break
    #                                 print("Depto del equipo diferente de los dptos del grupo")
    #                             if igual_depto:
    #                                 print("Entro a validar cuando los tienen los mismos deppartamentos")
    #                                 intentos += 1
    #                                 if grupo == grupos[grupos.count()-1] and cant_equipos_grupo+1 == grupo.numero_equipos:
    #                                     print("se forza a guardar, no hay mas equipos")
    #                                     grupo.equipos.add(equipo)
    #                                     grupo.save()
    #                                     guardado_grupo = True
    #                                     print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
    #                             else:
    #                                 if guardado_grupo == False:
    #                                     print("se puede guardar")
    #                                     grupo.equipos.add(equipo)
    #                                     grupo.save()
    #                                     guardado_grupo = True
    #                                     print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
    #                                     # print("-------------------------------------------------------------")
    #                                 break
    #                         else:
    #                             if guardado_grupo == False:
    #                                 print("El grupo esta vacio. Guardo directo")
    #                                 grupo.equipos.add(equipo)
    #                                 grupo.save()
    #                                 guardado_grupo = True
    #                                 print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
    #                                 # print("-------------------------------------------------------------")
    #                             break
    #                     elif intentos ==0 and grupo == grupos[len(grupos)-1]:
    #                         print("No Tengo intentos. Pero es el ultimo grupo")
    #                         if guardado_grupo == False:
    #                             print("El grupo esta vacio. Guardo directo")
    #                             grupo.equipos.add(equipo)
    #                             grupo.save()
    #                             guardado_grupo = True
    #                             print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
    #                             # print("-------------------------------------------------------------")
    #                         break

    #                     elif intentos >=0 and grupo != grupos[len(grupos)-1]:
    #                         print("Tengo mas de un intento. Pero no es el ultimo grupo")
    #                         if cant_equipos_grupo > 0:
    #                             print("el grupo no esta vacio")
    #                             print("Los equipos del grupo son {} : {}".format(cant_equipos_grupo, equipos_grupo))
    #                             igual_depto = False
    #                             for equipo_grupo in equipos_grupo:
    #                                 print("Entro a comparar los departamentos y municipios")
    #                                 if equipo.departamento == equipo_grupo.departamento:
    #                                     igual_depto = True
    #                                     print("Tiene el mismo departamento")
    #                                     break
    #                                 print("Depto del equipo diferente de los dptos del grupo")
    #                             if igual_depto:
    #                                 print("Entro a validar cuando los tienen los mismos deppartamentos")
    #                                 intentos += 1
    #                                 if grupo == grupos[grupos.count()-1] and cant_equipos_grupo+1 == grupo.numero_equipos:
    #                                     print("se forza a guardar, no hay mas equipos")
    #                                     grupo.equipos.add(equipo)
    #                                     grupo.save()
    #                                     guardado_grupo = True
    #                                     print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
    #                             else:
    #                                 if guardado_grupo == False:
    #                                     print("se puede guardar")
    #                                     grupo.equipos.add(equipo)
    #                                     grupo.save()
    #                                     guardado_grupo = True
    #                                     print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
    #                                     # print("-------------------------------------------------------------")
    #                                 break
    #                         else:
    #                             if guardado_grupo == False:
    #                                 print("El grupo esta vacio. Guardo directo")
    #                                 grupo.equipos.add(equipo)
    #                                 grupo.save()
    #                                 guardado_grupo = True
    #                                 print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())
    #                                 # print("-------------------------------------------------------------")
    #                             break
                        
    #                     elif intentos >=0 and grupo == grupos[len(grupos)-1]:
    #                         print("Tengo mas de un intento y estoy en el ultimo grupo")
    #                         if guardado_grupo == False:
    #                             print("El grupo esta vacio. Guardo directo")
    #                             grupo.equipos.add(equipo)
    #                             grupo.save()
    #                             guardado_grupo = True
    #                             print("El grupo queda con los siguientes integrantes: ", grupo.equipos.all())


    #         return JsonResponse({'mensaje': "Grupos Automaticos Generados satisfactoriamente", "equipos_creados":True})
    # else:
    #     print("Tengo menos de 6 equipos")
