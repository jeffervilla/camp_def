from django.db import models
from django.contrib.auth.models import User, Group

# Create your models here.
# class Role(models.Model):
#     nombre = models.CharField(max_length=32)
#     descripcion = models.CharField(max_length=256, blank=True)
#     administracion = models.BooleanField(default=False)
#     parametros = models.BooleanField(default=False)
#     casos = models.BooleanField(default=False)
#     class Meta(object):
#         ordering = ['nombre']
#         verbose_name = 'role'
#         verbose_name_plural = 'roles'
#
#     def __str__(self):
#         return str(self.nombre)
#===============================================================================
class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT, primary_key=True)
    telefono = models.CharField(max_length=64, blank=True, default='')
    # role = models.ForeignKey(Role, on_delete=models.PROTECT, null=True)

    def __str__(self):
        ordering = ['user']
        return str(self.user.username)
#===============================================================================
class EstadoTicket(models.Model):
    id = models.CharField(primary_key=True, max_length=16)
    nombre = models.CharField(max_length=32)
    color = models.CharField(max_length=32)
    descripcion = models.CharField(max_length=256, blank=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'estado - caso'
        verbose_name_plural = 'estados - caso'

    def __str__(self):
        return str(self.nombre)

#===============================================================================
class TipoUrgencia(models.Model):
    id = models.CharField(primary_key=True, max_length=16)
    nombre = models.CharField(max_length=32)
    tiempo_solucion = models.IntegerField(default=1, help_text="Tiempo en Minutos")
    descripcion = models.CharField(max_length=256, blank=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'tipo - urgencia'
        verbose_name_plural = 'tipo - urgencias'

    def __str__(self):
        return str(self.nombre)
#===============================================================================

class Ticket(models.Model):
    nombre = models.CharField(max_length=64)
    urgencia = models.ForeignKey(TipoUrgencia, on_delete=models.PROTECT)
    descripcion = models.TextField(max_length=1024, blank=True)
    solicitante = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, related_name='solicitante')
    notas = models.TextField(max_length=1024, blank=True)
    responsable = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, related_name='responsable')
    solucion = models.TextField(max_length=1024, blank=True)
    estado = models.ForeignKey(EstadoTicket,blank=True, null=True, on_delete=models.PROTECT)
    retroalimentacion = models.TextField(max_length=1024, blank=True)
    f_inicio = models.DateTimeField(verbose_name='fecha_inicio', blank=True, null=True)
    f_fin = models.DateTimeField(verbose_name='fecha fin', blank=True, null=True)
    f_registro = models.DateTimeField(verbose_name="fecha registro", auto_now_add=True,)
    evaluacion_cliente = models.BigIntegerField(blank=True,null=True)

    class Meta:
        ordering = ['-pk']
        verbose_name = 'Ticket'
        verbose_name_plural = 'Tickets'

    def __str__(self):
        return str(self.pk)

#===============================================================================
class AuditoriaTicketEstado(models.Model):
    ticket = models.ForeignKey(Ticket, on_delete=models.PROTECT)
    estado = models.ForeignKey(EstadoTicket, on_delete=models.PROTECT)
    usuario = models.ForeignKey(User, on_delete=models.PROTECT, null=True, related_name='usuario_cambio')
    usuario_responsable = models.ForeignKey(User, on_delete=models.PROTECT, null=True ,related_name='usuario_responsable')
    observaciones = models.CharField(max_length=1024, blank=True)
    f_registro = models.DateTimeField(verbose_name="fecha registro", auto_now_add=True,)

    class Meta:
        ordering = ['ticket']
        verbose_name = 'AuditoriaTicketEstado'
        verbose_name_plural = 'AuditoriaTicketEstado'

    def __str__(self):
        return str(self.pk)
#===============================================================================
class Observacion(models.Model):
    ticket = models.ForeignKey(Ticket, on_delete=models.PROTECT)
    registro = models.CharField(max_length=1024, blank=True)
    f_registro = models.DateTimeField(verbose_name="fecha registro", auto_now_add=True,)
    class Meta:
        ordering = ['ticket']
        verbose_name = 'ObservacionTicket'
        verbose_name_plural = 'ObservacionesTicket'

    def __str__(self):
        return str(self.pk)
