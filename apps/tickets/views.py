import json
from config.utils import render_to_pdf
from datetime import datetime, timezone
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import get_template
from django.urls import reverse
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, UpdateView, FormView, DeleteView
from .functions import getPaginatorItems
from .forms_ticket import (TicketUpdateForm)
from .models import *


# # Create your views here.
# @login_required(login_url='login')
# def index(request):
#     return render(request, 'tickets/index.html', {'usuario': request.user })

class Tickets(ListView):
    model = Ticket
    context_object_name = 'tickets'
    template_name = 'tickets/list.html'
    paginate_by = 10
    def get_queryset(self): #Uso el filtro para que según el usuario vea:
        user = self.request.user
        if user.groups.filter(name__in=['Tecnicos']).exists():#SI ES TECNICO SOLO LOS CASOS QUE TIENE
            object_list = Ticket.objects.filter( Q(responsable=user) | Q(solicitante=user) ).exclude(estado=EstadoTicket.objects.get(id=50)).exclude( estado=EstadoTicket.objects.get(id=70))
            try:
                busqueda = self.request.GET['buscar']
            except:
                busqueda = ''
            if(busqueda !=''):
                object_list = object_list.filter(nombre__icontains = busqueda) \
                    | object_list.filter(descripcion__icontains = busqueda)

            return object_list

        elif user.groups.filter(name__in=['Administradores']).exists():#SI ES EL SUPERVISOR TODOS
            object_list = Ticket.objects.all()
            try:
                busqueda = self.request.GET['buscar']
            except:
                busqueda = ''
            if(busqueda !=''):
                object_list = object_list.filter(nombre__icontains = busqueda) \
                    | object_list.filter(descripcion__icontains = busqueda)

            return object_list

        elif user.groups.filter(name__in=['Supervisor']).exists():#SI ES EL SUPERVISOR TODOS
            object_list = Ticket.objects.all()
            try:
                busqueda = self.request.GET['buscar']
            except:
                busqueda = ''
            if(busqueda !=''):
                object_list = object_list.filter(nombre__icontains = busqueda) \
                    | object_list.filter(descripcion__icontains = busqueda)

            return object_list

        elif user.groups.filter(name__in=['Usuarios']).exists():#SI ES EL USUARIO solo LO QUE HAYA ABIERTO
            object_list = Ticket.objects.filter(solicitante=user)
            try:
                busqueda = self.request.GET['buscar']
            except:

                busqueda = ''
            if(busqueda !=''):
                object_list = object_list.filter(nombre__icontains = busqueda) \
                    | object_list.filter(descripcion__icontains = busqueda)
            return object_list
        else:
            object_list = []
            return object_list

    def get_context_data(self, **kwargs):
        # print ("===============================================================")
        # print ("kwargs: ", **kwargs )
        # print ("===============================================================")
        context = super(Tickets, self).get_context_data(**kwargs)
        # print ("===============================================================")
        # print ("Paso de la instancia de la clase superior en get context " )
        # print ("===============================================================")
        if not context.get('is_paginated', False):
            try:
                busqueda = self.request.GET['buscar']
                print("encontro buscar...", busqueda)
            except:
                busqueda = ''
            context.update({'busqueda':busqueda})
            return context

        paginator = context.get('paginator')
        num_pages = paginator.num_pages
        try:
            busqueda = self.request.GET['buscar']
            print("encontro buscar...", busqueda)
        except:
            busqueda = ''
        current_page = context.get('page_obj')
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]
        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context.update({'pages': pages, 'busqueda':busqueda})
        return context

class TicketCreate( LoginRequiredMixin, CreateView):
    model = Ticket
    fields = '__all__'
    template_name = 'tickets/create.html'

    def form_valid(self, form):
        user = self.request.user
        if form.instance.estado==None:
            form.instance.estado= EstadoTicket.objects.get(id=10)
        form.instance.solicitante = user
        return super().form_valid(form)
    def get_success_url(self):
        ticket= Ticket.objects.get(pk=self.object.id)
        auditoria = AuditoriaTicketEstado(ticket=ticket, estado=EstadoTicket.objects.get(id=10), usuario=self.request.user,
                    usuario_responsable=ticket.responsable, observaciones=ticket.descripcion )
        auditoria.save()
        return reverse('tickets')


class TicketUpdate(UpdateView): #Funciona  para asignar un Ticket a un  responsable
    model = Ticket
    form_class = TicketUpdateForm
    template_name = 'tickets/update.html'
    success_url = '/ticket'
    def form_valid(self, form):
        print(" Se añadio responsable ")
        form.instance.estado= EstadoTicket.objects.get(id=20) #el ticket se pasa a "asignado"
        ticket= Ticket.objects.get(pk=self.object.id)
        auditoria = AuditoriaTicketEstado(ticket=ticket, estado=EstadoTicket.objects.get(id=20), usuario=self.request.user,
                    usuario_responsable=self.object.responsable, observaciones=form.instance.notas )
        auditoria.save()
        return super().form_valid(form)
    def get_context_data(self, **kwargs): # Paso ambos contextos
        context = super(TicketUpdate, self).get_context_data(**kwargs)
        context['observaciones'] = Observacion.objects.filter(ticket=self.object.id)
        return context

class TicketReasignado(UpdateView):
    model = Ticket
    fields = '__all__'
    template_name = 'tickets/update.html'
    success_url= '/ticket'

class TicketDelete(DeleteView):
    model = Ticket
    template_name = 'tickets/delete.html'

class GeneratePDF(View):
    def get(self, request, *args, **kwargs):
        template = get_template('tickets/pdfview.html')
        context = {
            "cliente_name": "Andres Garcia ", #Nombre del cliente
            "cliente_tel": "3166128479 ", #tel del cliente
            "cliente_email": "andres@grupoda.com ", #email del cliente
            "fecha_factura": date.today(),          #fecha de la facturacion
            "cuerpo_mensaje":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "cuerpo_notas":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "nombre_asesor":"Yineth Vergara",
            "valor_subtotal": "100",
            "valor_iva":"19",
            "valor_Total":"119"
        }
        # html = template.render(context)
        pdf = render_to_pdf('tickets/pdfview.html', context)
        if pdf:
            return HttpResponse(pdf, content_type='application/pdf')
        return HttpResponse("Archivo no encontrado")
        # return HttpResponse(html)

class TicketGestion(View): #esta funcion hace todos los cambios de estado de tickets
    def get(self, request, *args, **kwargs):
        estado = request.GET['estado_id']
        ticket = Ticket.objects.get(pk=request.GET['id'])
        if request.is_ajax():
            if estado == 'ASIGNADO': #Asignado PAso a Aceptado
                ticket.estado=EstadoTicket.objects.get(id=30)
                nuevo_estado = "Aceptado"
                ticket.save()
            if estado == 'ACEPTADO': #ACEPTADO PAso a EN EJECUCION Toma tiempo inicio
                ticket.estado=EstadoTicket.objects.get(id=40)
                ticket.f_inicio= datetime.now(timezone.utc)
                nuevo_estado = "En ejecución"
                ticket.save()
            if estado == 'EN_EVALUACION': #Pasa a en evaluacion si se soluciono
                ticket.estado=EstadoTicket.objects.get(id=50)
                ticket.solucion=request.GET['mensaje']
                nuevo_estado = request.GET['mensaje']
                ticket.save()
            if estado == 'NO_CONCLUIDO': #pasa ene valuacion si no se pudo solucionar
                ticket.estado=EstadoTicket.objects.get(id=80)
                ticket.solucion=request.GET['mensaje']
                nuevo_estado = request.GET['mensaje']
                ticket.save()
            if estado == 'FINALIZADO': #SI SE SOLUCIONO
                ticket.estado=EstadoTicket.objects.get(id=70)
                ticket.retroalimentacion = request.GET['mensaje']
                ticket.calificacion = request.GET['calificacion']
                ticket.f_fin= datetime.now(timezone.utc)
                nuevo_estado = request.GET['mensaje']
                ticket.save()

            auditoria = AuditoriaTicketEstado(ticket=ticket, estado=ticket.estado, usuario=self.request.user,
                        usuario_responsable=ticket.responsable, observaciones=nuevo_estado)
            auditoria.save()

        else:
            print("Resquest not is ajax")
            return render(self.request, 'contactos/contacto.html', {})
        return JsonResponse({'status': 'ok', 'nuevo_estado': nuevo_estado })

class TicketVer(View): #esta funcion hace consultas que vienen de AJAX
    def get(self, request, *args, **kwargs):
        tipo_consulta = request.GET['tipo']
        if request.is_ajax():
            ticket = Ticket.objects.get(pk=request.GET['pk'])
            objeto_dato = {}
            lista_json = []
            objeto_dato["nombre"]= ticket.nombre
            objeto_dato["urgencia"]= ticket.urgencia.nombre
            objeto_dato["descripcion"]= ticket.descripcion
            objeto_dato["estado"]= ticket.estado.nombre
            objeto_dato["notas"]= ticket.notas
            if ticket.responsable:
                objeto_dato["responsable"]= ticket.responsable.get_full_name()
            objeto_dato["solucion"]= ticket.solucion
            lista_json.append(objeto_dato)
            print ("respondiendo JSON:..", lista_json )
            return JsonResponse({'ticket':lista_json})
        else:
            print("Resquest not is ajax")
            return render(self.request, 'contactos/contacto.html', {})
        return JsonResponse({'status': 'ok', 'nuevo_estado': nuevo_estado })

class TicketObservacion(View): #esta funcion guarda las observaciones de todos los tickets
    def get(self, request, *args, **kwargs):
        nota = request.GET['observacion']
        ticket = Ticket.objects.get(pk=request.GET['id'])
        print("llego:.........", nota)
        if request.is_ajax():
            observacion = Observacion(ticket=ticket, registro=nota )
            observacion.save()
        else:
            print("Resquest not is ajax")
            return render(self.request, 'contactos/contacto.html', {})
        return JsonResponse({'status': 'ok', 'observacion': nota })
