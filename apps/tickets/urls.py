from django.urls import include, path
from . import views

urlpatterns = [
    # path('',                            views.index,                     name='index'),
    path('ticket/',                     views.Tickets.as_view(),         name='tickets'),
    path('ticket/create/',              views.TicketCreate.as_view(),    name='ticket-create'),
    path('ticket/update/<int:pk>/',     views.TicketUpdate.as_view(),    name='ticket-update'),
    path('ticket/<int:pk>/delete/',     views.TicketDelete.as_view(),    name='ticket-delete'),
    path('ticket/gestion/',             views.TicketGestion.as_view(),   name='gestion'),
    path('ticket/observacion/',         views.TicketObservacion.as_view(), name='observacion'),
    path('ticket/ver/',                 views.TicketVer.as_view(),       name='ticket-ver'),
    path('pdf/',                        views.GeneratePDF.as_view(),     name='ver-pdf' ),
    # path('<int:id>/modificar/',views.TicketModificar.as_view(), name='edit-ticket'),
    # path('<int:id>/ver',       views.TicketVer.as_view(), name='ver-ticket'),
    ]
