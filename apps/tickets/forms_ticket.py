from django import forms

from .models import Ticket, User

#===============================================================================
class TicketUpdateForm(forms.ModelForm):

    class Meta:
        model = Ticket
        fields = ['nombre', 'urgencia', 'descripcion', 'solicitante', 'notas', 'responsable',
          'solucion',  'estado'
        ]
    def __init__(self, *args, **kwargs):
        super(TicketUpdateForm, self).__init__(*args, **kwargs)
        self.fields['responsable'].queryset = User.objects.filter(groups__name__in=['Tecnicos'])

#===============================================================================
