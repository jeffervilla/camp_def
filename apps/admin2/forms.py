from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import (UserCreationForm, UserChangeForm,
    UsernameField, ReadOnlyPasswordHashField, PasswordChangeForm
)
from django.contrib.auth.models import User, Group
from django.core.exceptions import ValidationError

from django.utils.translation import gettext, gettext_lazy as _

from .models import UserProfile
from apps.contactos.models import Contacto


class UserCreateForm(UserCreationForm):
    group = forms.ModelMultipleChoiceField(queryset=Group.objects.all())
    class Meta:
        model = User
        # fields = ('username', 'first_name', 'last_name', 'email')
        fields = ('username', 'first_name', 'email')
        field_classes = {'username': UsernameField}

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        groups = self.cleaned_data.get('group')
        if commit:
            user.save()
            user.groups.set(groups)
            user_profile = UserProfile(
                usuario=user,
            )
            user_profile.save()
        return user


class UserUpdateForm(UserChangeForm):
    # group = forms.ModelMultipleChoiceField(queryset=Group.objects.all())
    class Meta:
        model = User
        # fields = ('username', 'password', 'first_name', 'last_name', 'email')
        fields = ('username', 'password', 'first_name', 'email')

class UserPasswordChangeForm(forms.Form):
    """
    A form used to change the password of a user in the admin interface.
    """
    error_messages = {
        'password_mismatch': _('The two password fields didn’t match.'),
    }
    password1 = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password', 'autofocus': True}),
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password (again)"),
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        password_validation.validate_password(password2)
        return password2

    def save(self, user_to_update, commit=True):
        """Save the new password."""
        password = self.cleaned_data["password1"]
        user_to_update.set_password(password)
        if commit:
            user_to_update.save()
            message = 'Usuario modificado con exito!'.format(user_to_update.username)
        return user_to_update, message

class NewUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']
        # fields = '__all__'
        labels = {
                'username': 'Nombre de Usuario' ,
                'first_name':'Nombres',
                # 'last_name':'documento',
                }
    def clean(self):
        username = self.cleaned_data.get('username')
        if User.objects.filter(username=username).exists():
            print(" ERROR USUARIO= CORREO YA EXISTE")
            raise ValidationError({'username': ["Correo Electronico ya existe!",]})

        # if User.objects.filter(last_name=last_name).exists():
        #     print(" ERROR APELLIDO= NO DOCUMENTO YA EXISTE")
        #     raise ValidationError({'last_name': ["No. documento ya existe!",]})
        # return self.cleaned_data

    def save(self):
        # print("POST recuperado", self.request.POST)
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data["username"]
        print("cleaned data", self.cleaned_data)
        user.save()
        
        return user