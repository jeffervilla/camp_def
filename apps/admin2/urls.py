from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import views as auth_views
from django.urls import include, path
# from .views import RegistrationView, UsernameValidationView, EmailValidationView, LogoutView, VerificationView, LoginView
from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('login/', auth_views.LoginView.as_view(template_name='admin2/login.html', ), name='login'),
    path('logout/', auth_views.logout_then_login, name='logout'),
    path('login/register/',views.RegisterView.as_view(), name='register'),
    path('admin/usuario/', views.UserListView.as_view(), name='usuario'),
    path('admin/usuario/add/', views.UserCreateView.as_view(), name='usuario-add'),
    path('admin/usuario/<int:pk>/update/', views.UserUpdateView.as_view(), name='usuario-update'),
    path('admin/usuario/<int:pk>/password/', views.AdminChangePasswordView.as_view(), name='usuario-password-change'),
    path('admin/usuario/<int:pk>/delete/', views.UserDeleteView.as_view(), name='usuario-delete'),
    

    path('grupo/', views.GroupListView.as_view(), name='grupo'),
    path('grupo/add/', views.GroupCreateView.as_view(), name='grupo-add'),
    path('grupo/<int:pk>/update/', views.GroupUpdateView.as_view(), name='grupo-update'),
    path('grupo/<int:pk>/delete/', views.GroupDeleteView.as_view(), name='grupo-delete'),

    path('nuevo-usuario/',               views.NewUser.as_view(),          name='new_user'),

    path('terminosycondiciones/',         views.TerminosyCondiciones.as_view(),  name='terminosycondiciones'),

    path('validate-username', csrf_exempt(views.UsernameValidationView.as_view()),
            name="validate-username"),
    path('validate-email', csrf_exempt(views.EmailValidationView.as_view()),
        name='validate_email'),
    path('activate/<uidb64>/<token>',
        views.VerificationView.as_view(), name='activate'),



]



    
    # path('login/', auth_views.LoginView.as_view(template_name='admin2/login.html',), name="login"),
    # path('register', views.RegisterView.as_view(), name="register"),
    # path('logout', auth_views.LogoutView.as_view(), name="logout"),

