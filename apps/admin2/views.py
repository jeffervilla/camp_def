from apps.contactos.models import Contacto
import unicodedata

from django.contrib import messages
from django.contrib.auth import password_validation
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User, Group
from django.contrib.auth.views import PasswordChangeView

from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.translation import gettext, gettext_lazy as _
from django.views import View
from django.views.generic import DetailView, ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, FormView, UpdateView

from django.shortcuts import render, redirect
from django.views import View
import json
from django.http import JsonResponse
from django.contrib.auth.models import User
import json
from django.http import JsonResponse
from django.contrib.auth.models import User
# from validate_email import validate_email
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.messages.constants import ERROR
from django.core.mail import EmailMessage
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text, DjangoUnicodeDecodeError
from django.core.mail import send_mail
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.template.loader import render_to_string
from config.utils import account_activation_token
from django.urls import reverse
from django.contrib import auth


from .forms import UserCreateForm, UserUpdateForm, UserPasswordChangeForm, NewUserForm


@method_decorator(login_required, name='dispatch')
# class IndexView(TemplateView):
#     template_name = 'login.html'

class RegisterView(TemplateView):
    template_name = 'admin2/register.html'

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.view_user', raise_exception=True), name='dispatch')
class UserListView(ListView):
    """docstring for UserListView """
    model = User
    queryset = User.objects.filter(is_active=True).order_by('username')
    context_object_name = 'users'
    template_name = 'admin2/user.html'

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.add_user', raise_exception=True), name='dispatch')
class UserCreateView(FormView):
    """docstring for ."""
    template_name = 'admin2/register.html'
    form_class = UserCreateForm
    success_url = '/admin/usuario/'

    def form_valid(self, form):
        password2 = form.clean_password2()
        form.save()
        messages.add_message(self.request, messages.SUCCESS, 'Usuario creado con exito!')
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.change_user', raise_exception=True), name='dispatch')
class UserUpdateView(UpdateView):
    """docstring for UserListView """
    model = User
    form_class = UserUpdateForm
    template_name = 'admin2/user_update.html'
    success_url = '/admin/usuario/'

    def form_valid(self, form):
        messages.add_message(self.request, messages.INFO, 'Usuario modificado con exito!')
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.change_user', raise_exception=True), name='dispatch')
class AdminChangePasswordView(View):
    form_class = UserPasswordChangeForm
    template_name = 'admin2/user_password_change.html'
    success_url = '/admin/usuario/{}/update/'

    def get(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        form = self.form_class()
        return render(request, self.template_name, {'user': user_to_update, 'form': form})

    def post(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        form = self.form_class(self.request.POST)
        if form.is_valid():
            form.save(user_to_update)
            messages.add_message(self.request, messages.INFO, 'Se ha actualizado la contraseña para el Usuario {}!'.format(user_to_update.username))
            return redirect(self.success_url.format(user_to_update.pk))
        else:
            return render(request, self.template_name, {'user': user_to_update, 'form': form})




class EmailValidationView(View):
    def post(self, request):
        data = json.loads(request.body)
        email = data['email']
        if not validate_email(email):
            return JsonResponse({'email_error': 'Email is invalid'}, status=400)
        if User.objects.filter(email=email).exists():
            return JsonResponse({'email_error': 'sorry email in use,choose another one '}, status=409)
        return JsonResponse({'email_valid': True})


class UsernameValidationView(View):
    def post(self, request):
        data = json.loads(request.body)
        username = data['username']
        if not str(username).isalnum():
            return JsonResponse({'username_error': 'username should only contain alphanumeric characters'}, status=400)
        if User.objects.filter(username=username).exists():
            return JsonResponse({'username_error': 'sorry username in use,choose another one '}, status=409)
        return JsonResponse({'username_valid': True})


class VerificationView(View):
    def get(self, request, uidb64, token):
        try:
            id = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=id)

            if not account_activation_token.check_token(user, token):
                return redirect('login'+'?message='+'User already activated')

            if user.is_active:
                return redirect('login')
            user.is_active = True
            user.save()

            messages.success(request, 'Account activated successfully')
            return redirect('login')

        except Exception as ex:
            pass

        return redirect('login')


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.delete_user', raise_exception=True), name='dispatch')
class UserDeleteView(View):
    form_class = UserPasswordChangeForm
    template_name = 'admin2/user_delete.html'
    success_url = '/admin/usuario/'

    def get(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        return render(request, self.template_name, {'user': user_to_update})

    def post(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        user_to_update.is_active = False
        user_to_update.save()
        messages.add_message(self.request, messages.WARNING, 'Se ha desactivado el usuario {}, para reactivar por favor contacte con su administrador!'.format(user_to_update.username))
        return redirect(self.success_url)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.view_group', raise_exception=True), name='dispatch')
class GroupListView(ListView):
    """docstring for UserListView """
    model = Group
    context_object_name = 'groups'
    template_name = 'admin2/group.html'

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.add_group', raise_exception=True), name='dispatch')
class GroupCreateView(FormView):
    """docstring for ."""
    template_name = 'admin2/user_create.html'
    form_class = UserCreateForm
    success_url = '/admin/usuario/'

    def form_valid(self, form):
        password2 = form.clean_password2()
        form.save()
        messages.add_message(self.request, messages.SUCCESS, 'Usuario creado con exito!')
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.change_group', raise_exception=True), name='dispatch')
class GroupUpdateView(UpdateView):
    """docstring for UserListView """
    model = User
    form_class = UserUpdateForm
    template_name = 'admin2/user_update.html'
    success_url = '/admin/usuario/'

    def form_valid(self, form):
        messages.add_message(self.request, messages.INFO, 'Usuario modificado con exito!')
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.delete_group', raise_exception=True), name='dispatch')
class GroupDeleteView(View):
    form_class = UserPasswordChangeForm
    template_name = 'admin2/user_delete.html'
    success_url = '/admin/usuario/'

    def get(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        return render(request, self.template_name, {'user': user_to_update})

    def post(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        user_to_update.is_active = False
        user_to_update.save()
        messages.add_message(self.request, messages.WARNING, 'Se ha desactivado el usuario {}, para reactivar por favor contacte con su administrador!'.format(user_to_update.username))
        return redirect(self.success_url)

class NewUser(CreateView):
    model = User
    form_class = NewUserForm
    template_name='admin2/new_user.html'

    def post(self, request, *args, **kwargs):
        print("POSt del post: ", self.request.POST)
        my_form = NewUserForm(self.request.POST)

        if (my_form.is_valid()):
            user_created = my_form.save()
            grupo = Group.objects.get(name = 'UsuariosWeb')
            messages.success = (request,"usuario creado correctamente")
                            
            print("Voy a crear el contacto")

            contacto = Contacto(
                nombres = '{}'.format(user_created.first_name.title()),
                usuario = user_created,
                email = user_created.username,
                telefono =  self.request.POST.get('telefono'),
                grupo =  grupo,
                es_jugador = False,
            )
            contacto.save()
            print("USuario creado", user_created.pk)
            
            return redirect('login')


        else: 
            print("Errores de formulario", my_form.errors)
            messages.error = (request,"usuario no creado")
            
            return redirect('new_user')
            

        
        

class TerminosyCondiciones(TemplateView):
    template_name = 'admin2/terminosycondiciones.html'


