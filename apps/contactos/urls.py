from django.urls import path

from . import views

urlpatterns = [
    path('contacto/', views.ContactoListView.as_view(), name='contacto'),
    path('contacto/add/', views.ContactoAddView.as_view(), name='contacto-add'),
    path('contacto/<int:pk>/detail/', views.ContactoDetailView.as_view(), name='contacto-detail'),
    path('contacto/<int:pk>/update/', views.ContactoUpdateView.as_view(), name='contacto-update'),
    path('contacto/test-template/', views.TestTemplateView.as_view(), name='test-template'),
]
