from django.urls import reverse_lazy, reverse
from django.shortcuts import render, redirect
from django.views.generic  import (
    View, ListView, CreateView, UpdateView, DeleteView, FormView, TemplateView, DetailView
)
from .forms import AgregarContactoForm
from .models import *

# Create your views here.

def index(request):
    return render(request, 'contactos/base_content.html', {})


class ContactoListView(View):

    """docstring for UserListView """
    def get(self, request, *args, **kwargs):
        display = self.request.GET.get('display')
        print ("Display: ", display)
        if display == None:
            display = 'kanban'
        template = 'contactos/contacto_list.html'
        contactos = Contacto.objects.all()
        return render(self.request, template, {'contactos': contactos, 'display': display })

class ContactoAddView(CreateView):
    model = Contacto
    template_name = 'contactos/contacto_add.html'
    fields = '__all__'

    def get_success_url(self):
        return reverse('contacto')

class ContactoDetailView(DetailView):
    template_name = 'contactos/contacto_detail.html'
    model =  Contacto
    fields = '__all__'
    success_url = '/contacto/'

class ContactoUpdateView(UpdateView):
    template_name = 'contactos/contacto_update.html'
    model = Contacto
    fields = '__all__'
    success_url = '/contacto/'

class TestTemplateView(TemplateView):
    template_name = 'test_template.html'
