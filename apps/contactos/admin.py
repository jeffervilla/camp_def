from django.contrib import admin
from .models import *

@admin.register(Contacto)
class ContactoAdmin(admin.ModelAdmin):
    list_display = ('documento','nombres','apellidos','es_jugador','equipo', 'edad')

@admin.register(ContactoRelacion)
class ContactoRelacionAdmin(admin.ModelAdmin):
    list_display = ('contacto', 'contacto_relacionado', 'relacion')

admin.site.register(Pais)
admin.site.register(PaisEstado)
admin.site.register(Industria)
admin.site.register(Relacion)

