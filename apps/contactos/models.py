from apps.evento.models import Evento
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User, Group
from django.utils import tree
# from apps.campeonato.models import Equipo

class TipoIdentificacion(models.Model):
    nombre = models.CharField(max_length=64)

    class Meta:
        ordering = ['id']
        verbose_name = 'Tipo de Identificacion'
        verbose_name_plural = 'Tipos de Identificacion'
        

    def __str__(self):
        return '{}'.format(self.nombre)

class Departamento(models.Model):
    nombre = models.CharField(max_length=64)

    class Meta:
        ordering = ['id']
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'

    def __str__(self):
        return '{}'.format(self.nombre)

class Municipio(models.Model):
    nombre = models.CharField(max_length=64)
    departamento = models.ForeignKey(Departamento, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'Municipio'
        verbose_name_plural = 'Municipio'

    def __str__(self):
        return '{}'.format(self.nombre)

class Pais(models.Model):
    nombre = models.CharField(unique=True, max_length=512)
    codigo = models.CharField(unique=True, max_length=2, blank=True, null=True)
    formato_direccion = models.TextField(blank=True, null=True)
    codigo_telefono = models.IntegerField(blank=True, null=True)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="pais_creado_por")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="pais_modificado_por")
    modificado_fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['nombre']
        verbose_name = 'Pais'
        verbose_name_plural = 'Paises'

    def __str__(self):
        return self.nombre
# ==============================================================================
class PaisEstado(models.Model):
    pais = models.ForeignKey(Pais, models.DO_NOTHING)
    nombre = models.CharField(max_length=512)
    codigo = models.CharField(max_length=128)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="paisestado_creado_por")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="paisestado_modificado_por")
    modificado_fecha = models.DateTimeField(blank=True, null=True)
    class Meta:
        ordering = ['nombre']
        verbose_name = 'Estado'
        verbose_name_plural = 'Estado'
        unique_together = (('pais', 'codigo'),)

    def __str__(self):
        return self.nombre
# ==============================================================================
class Industria(models.Model):
    nombre = models.CharField(max_length=512, blank=True, null=True)
    activo = models.BooleanField(blank=True, default=True)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="industria_creado_por")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="industria_modificado_por")
    modificado_fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['nombre']
        verbose_name = 'Industria'
        verbose_name_plural = 'Industrias'

    def __str__(self):
        return self.nombre
# ==============================================================================
class Relacion(models.Model):
    nombre = models.CharField(max_length=512, blank=True, null=True)
    activo = models.BooleanField(blank=True, default=True)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="relacion_creado_por")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="relacion_modificado_por")
    modificado_fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['nombre']
        verbose_name = 'Relacion'
        verbose_name_plural = 'Relaciones'

    def __str__(self):
        return self.nombre
# ==============================================================================
class Contacto(models.Model):
    MASCULINO='Masculino'
    FEMENINO='Femenino'
    SEXO_OP=[(MASCULINO, 'Masculino'),(FEMENINO,'femenino')]
    nombres = models.CharField(max_length=512)
    apellidos = models.CharField(max_length=512, blank=True, null=True)
    imagen = models.ImageField(upload_to='contactos/perfil/' ,blank=True, null=True)
    documento = models.CharField(max_length=512, blank=True, null=True, help_text="Identification number of partner")
    sexo = models.CharField(max_length=16, choices=SEXO_OP, blank=True)
    cargo = models.CharField(max_length=254, default="", blank=True, null=True)
    idioma = models.CharField(max_length=512, default="", blank=True, null=True)
    zona_horaria = models.CharField(max_length=512, default="", blank=True, null=True)
    usuario = models.OneToOneField(User, models.DO_NOTHING, blank=True, null=True)
    sitioweb = models.CharField(max_length=512, default="", blank=True, null=True)
    activo = models.BooleanField(blank=True, default=True)
    cliente = models.BooleanField(blank=True, default=False)
    proveedor = models.BooleanField(blank=True, default=False)
    empleado = models.BooleanField(blank=True, default=False)
    ciudad = models.CharField(max_length=512, default="", blank=True, null=True)
    estado = models.ForeignKey(PaisEstado, models.DO_NOTHING, blank=True, null=True)
    pais = models.ForeignKey(Pais, models.DO_NOTHING, blank=True, null=True)
    codigo_postal = models.CharField(max_length=512, default="", blank=True, null=True)
    direccion = models.CharField(max_length=512, default="", blank=True, null=True)
    telefono = models.CharField(max_length=512, default="", blank=True, null=True)
    email = models.CharField(max_length=512, default="", blank=True, null=True)
    es_compania = models.BooleanField(blank=True, default=False)
    industria = models.ManyToManyField(Industria, blank=True)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="contacto_creado_por")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="contacto_modificado_por")
    modificado_fecha = models.DateTimeField(blank=True, null=True)
    token_logueo = models.CharField(max_length=512, default="", blank=True, null=True)
    tipo_logueo = models.CharField(max_length=512, default="", blank=True, null=True)
    vencimiento_logueo = models.DateTimeField(blank=True, null=True)
    observaciones = models.TextField(default="", blank=True, null=True)
    
    es_jugador = models.BooleanField(blank=True, default=True)
    equipo = models.ForeignKey('campeonato.Equipo', models.DO_NOTHING, blank=True, null=True)
    tipo_identificacion = models.ForeignKey(TipoIdentificacion, on_delete=models.SET_NULL, blank=True, null=True)
    talla = models.CharField(max_length=512, blank=True, null=True)
    edad = models.IntegerField(blank=True, null=True)

    grupo = models.ForeignKey(Group, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        ordering = ['nombres']
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contactos'
        # unique_together = ['equipo', 'es_jugador']

    def __str__(self):
        return self.nombres

    def getContactosCount(self):
        return 0
        # return ContactoRelacion.objects.filter(
        #     Q(contacto=self.pk) | Q(contacto_relacionado=self.pk),
        #     Q(contacto__es_empresa=False) | Q(contacto_relacionado__es_empresa=False),
        # ).count()

    def getContactoRelacion(self):
        contacto_relaciones = ContactoRelacion.objects.filter(
            Q(contacto=self.pk) | Q(contacto_relacionado=self.pk),
        )
        return contacto_relaciones


# ==============================================================================
class ContactoRelacion(models.Model):
    contacto = models.ForeignKey(Contacto, models.DO_NOTHING, related_name="conrela_contacto")
    contacto_relacionado = models.ForeignKey(Contacto, models.DO_NOTHING, related_name="conrela_contacto2")
    relacion = models.ForeignKey(Relacion, models.DO_NOTHING)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="conrela_creado_por")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="conrela_modificado_por")
    modificado_fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'relacion-contacto'
        verbose_name_plural = 'relaciones-contacto'

    def __str__(self):
        return "{} - {}".format(self.contacto.nombre, self.contacto_relacionado.nombre)



# ===========================================CONTACTO INFO ===============================
# class Contacto(models.Model):
#     MASCULINO='M'
#     FEMENINO='F'
#     SEXO_OP=[(MASCULINO, 'Masculino'),(FEMENINO,'femenino')]
#     nombre = models.CharField(max_length=512)
#     imagen = models.ImageField(upload_to='contactos/perfil/' ,blank=True, null=True)
#     documento = models.CharField(unique=True, max_length=512, blank=True, null=True, help_text="Identification number of partner")
#     sexo = models.CharField(max_length=16, choices=SEXO_OP, blank=True)
#     compania = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
#     display_nombre = models.CharField(max_length=512, blank=True, null=True)
#     cargo = models.CharField(max_length=254, blank=True, null=True)
#     referencia = models.CharField(max_length=512, blank=True, null=True)
#     idioma = models.CharField(max_length=512, blank=True, null=True)
#     zona_horaria = models.CharField(max_length=512, blank=True, null=True)
#     usuario = models.OneToOneField(User, models.DO_NOTHING, blank=True, null=True)
#     sitioweb = models.CharField(max_length=512, blank=True, null=True)
#     limite_credito = models.FloatField(blank=True, null=True)
#     codigo_barras = models.CharField(max_length=512, blank=True, null=True)
#     activo = models.BooleanField(blank=True, default=True)
#     cliente = models.BooleanField(blank=True, default=False)
#     proveedor = models.BooleanField(blank=True, default=False)
#     empleado = models.BooleanField(blank=True, default=False)
#     direccion = models.CharField(max_length=512, blank=True, null=True)
#     codigo_postal = models.CharField(max_length=512, blank=True, null=True)
#     ciudad = models.CharField(max_length=512, blank=True, null=True)
#     estado = models.ForeignKey(PaisEstado, models.DO_NOTHING, blank=True, null=True)
#     pais = models.ForeignKey(Pais, models.DO_NOTHING, blank=True, null=True)
#     email = models.CharField(max_length=512, blank=True, null=True)
#     telefono = models.CharField(max_length=512, blank=True, null=True)
#     movil = models.CharField(max_length=512, blank=True, null=True)
#     es_compania = models.BooleanField(blank=True, default=False)
#     industria = models.ForeignKey(Industria, models.DO_NOTHING, blank=True, null=True)
#     color = models.IntegerField(blank=True, null=True)
#     creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="contacto_creado_por")
#     fecha_creacion = models.DateTimeField(auto_now_add=True)
#     modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="contacto_modificado_por")
#     modificado_fecha = models.DateTimeField(blank=True, null=True)
#     token_logueo = models.CharField(max_length=512, blank=True, null=True)
#     tipo_logueo = models.CharField(max_length=512, blank=True, null=True)
#     vencimiento_logueo = models.DateTimeField(blank=True, null=True)
#     observaciones = models.CharField(max_length=512, blank=True, null=True)
#
#     class Meta:
#         ordering = ['nombre']
#         verbose_name = 'Contacto'
#         verbose_name_plural = 'Contactos'
#
#     def __str__(self):
#         return self.nombre
#
#     def getContactosCount(self):
#         return 0
#         # return ContactoRelacion.objects.filter(
#         #     Q(contacto=self.pk) | Q(contacto_relacionado=self.pk),
#         #     Q(contacto__es_empresa=False) | Q(contacto_relacionado__es_empresa=False),
#         # ).count()
#
#     def getContactoRelacion(self):
#         contacto_relaciones = ContactoRelacion.objects.filter(
#             Q(contacto=self.pk) | Q(contacto_relacionado=self.pk),
#         )
#         return contacto_relaciones
