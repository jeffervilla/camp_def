from django.conf.urls import url
from django.urls import include, path
from . import views
from apps.evento import views

urlpatterns = [
    #Recepcion de Todos los usuarios
    path('home/',       views.Interfaz,     name='home'),

    # URLS de Usuario Administrador Equipos
    path('evento/detalle/<int:pk>',     views.EventoDetalle.as_view(),      name="evento-detalle"),


    #URLs de Usuario Administrador de Eventos
    path('crear-evento',                views.EventoCrear.as_view(),                    name="evento-crear"),
    path('disciplina/subdisciplinas',   views.SubdisciplinasDeDisciplina.as_view(),     name="subdisciplinas"),
    path('evento/<int:pk>/categorias',  views.CategoriaList.as_view(),                  name="categoria-list"),
    path('evento/borrar',               views.EventoBorrar.as_view(),                   name="evento-borrar"),
    # path('torneos_publicos',)






    

    path('categoria/crear', views.CategoriaCrear.as_view(), name="categoria-crear"),
    path('categoria/<int:pk>/detalle', views.CategoriaDetalle.as_view(), name="categoria-detalle"),
    # path('escenario/crear', views.EscenarioCrear.as_view(), name="torneo-crear"),
]
