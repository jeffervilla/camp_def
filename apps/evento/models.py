from django.contrib.auth.models import User
from django.db import models
from django.utils import tree

class Rama(models.Model):
    nombre = models.CharField(max_length=512)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True)
    activo = models.BooleanField(blank=True, default=True)
    observaciones = models.TextField(default="", blank=True, null=True)

    def __str__(self):
        return str(self.nombre)

    class Meta:
        ordering = ['id']
        verbose_name = 'Rama'
        verbose_name_plural = 'Ramas'

class Subdisciplina(models.Model):
    nombre = models.CharField(max_length=512)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True)
    activo = models.BooleanField(blank=True, default=True)
    observaciones = models.TextField(default="", blank=True, null=True)

    def __str__(self):
        return str(self.nombre)

    class Meta:
        ordering = ['id']
        verbose_name = 'Subcdisciplina'
        verbose_name_plural = 'Subcdisciplinas'

class Disciplina(models.Model):
    nombre = models.CharField(max_length=512)
    subdisciplinas = models.ManyToManyField(Subdisciplina)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True)
    activo = models.BooleanField(blank=True, default=True)
    observaciones = models.TextField(default="", blank=True, null=True)

    def __str__(self):
        return str(self.nombre)

    class Meta:
        ordering = ['id']
        verbose_name = 'Disciplina'
        verbose_name_plural = 'Disciplinas'

class Evento(models.Model):
    nombre = models.CharField(max_length=512)
    disciplina = models.ForeignKey(Disciplina , models.DO_NOTHING, default=None)
    subdisciplina = models.ForeignKey(Subdisciplina , models.DO_NOTHING, default=None)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True)
    activo = models.BooleanField(blank=True, default=True)
    finalizado = models.BooleanField(blank=True, default=False)
    observaciones = models.TextField(default="", blank=True, null=True)

    def __str__(self):
        return str(self.nombre)

    class Meta:
        ordering = ['id']
        verbose_name = 'Evento'
        verbose_name_plural = 'Eventos'

class Categoria(models.Model):
    MASCULINO='M'
    FEMENINO='F'
    MIXTO='MX'
    GENERO = [(MIXTO,'Mixto'),(MASCULINO,'Masculino'),(FEMENINO,'Femenino')]
    nombre = models.CharField(max_length=64)
    genero = models.CharField(max_length=16, choices=GENERO, blank=True)
    edad_min = models.CharField(max_length=64, blank=True, null=True)
    edad_max = models.CharField(max_length=64, blank=True, null=True)
    fecha_inscripcion_maxima = models.DateField(blank=True, null=True)
    disponible = models.BooleanField(default=True)
    evento = models.ForeignKey(Evento, models.DO_NOTHING, blank=True, null=True )

    class Meta:
        ordering = ['id']
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'

    def __str__(self):
        return '{} {}'.format(self.nombre, self.genero)

class Escenario(models.Model):
    nombre = models.CharField(max_length=512)
    hora_inicio= models.TimeField(blank=True, null=True)
    hora_fin= models.TimeField(blank=True, null=True)
    dias_disponibilidad = models.PositiveIntegerField(default=0)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True)
    disponible = models.BooleanField(blank=True, default=True)
    observaciones = models.TextField(default="", blank=True, null=True)
    evento = models.ForeignKey(Evento, models.DO_NOTHING, blank=True, null=True )

    def __str__(self):
        return str(self.nombre)

    class Meta:
        ordering = ['id']
        verbose_name = 'Escenario'
        verbose_name_plural = 'Escenarios'
