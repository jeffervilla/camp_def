from apps.evento.models import Disciplina, Escenario, Rama, Subdisciplina, Evento, Categoria
from django.contrib import admin

# Register your models here.
@admin.register(Evento)
class TorneoAdmin(admin.ModelAdmin):
    list_display = ('pk', 'nombre', 'disciplina', 'subdisciplina','activo','finalizado')

@admin.register(Subdisciplina)
class SubdisciplinaAdmin(admin.ModelAdmin):
    list_display = ('pk', 'nombre', 'activo')

@admin.register(Disciplina)
class DisciplinaAdmin(admin.ModelAdmin):
    list_display = ('pk', 'nombre', 'activo')

@admin.register(Escenario)
class EscenarioAdmin(admin.ModelAdmin):
    list_display = ('pk', 'nombre', 'disponible')

@admin.register(Rama)
class RamaAdmin(admin.ModelAdmin):
    list_display = ('pk', 'nombre', 'activo')

@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    list_display = ('id','nombre')