from apps.campeonato.models import Equipo
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic  import TemplateView, View, ListView

from apps.evento.forms import FormularioEvento
from apps.evento.models import Escenario, Rama, Evento, Disciplina, Subdisciplina, Categoria

@login_required(login_url='login')
def Interfaz(request):
    eventos_usuario = Evento.objects.filter(creado_por=request.user, activo = True)
    eventos_activos = Evento.objects.filter(activo=True)
    primera_categoria_eventos_usuario = []

    for evento in eventos_usuario:
        print("Tomo el evento ", evento)
        print("el evento tiene estas categorias", Categoria.objects.filter(evento = evento))
        if Categoria.objects.filter(evento = evento).exists():
            primera_categoria_eventos_usuario.append(Categoria.objects.filter(evento = evento)[0])

    print("Las primeras categirias de cada evcento son : ", primera_categoria_eventos_usuario)

    return render(request, 'indexxx.html', {'usuario': request.user, 'eventos_usuario':eventos_usuario,
    'eventos_activos': eventos_activos,  'primera_categoria_eventos_usuario': primera_categoria_eventos_usuario,
    'range': len(eventos_usuario), 'ramas':['Masculino', 'Femenino', 'Mixto']
    })

class EventoCrear(View):

    def get(self, request, *args, **kwargs):
        context = {}
        context['disciplinas'] = Disciplina.objects.all()
        context['ramas'] = Rama.objects.all()
        template = 'evento/nuevo.html'
        return render(self.request, template, context)

    def post(self, request, **kwargs):
        print(">>>> EventoCrear >>> Recojo el POST ", self.request.POST)
        nombre_evento = self.request.POST.get('nombre_torneo')
        print(self.request.POST.get('disciplina'))
        pk_disciplina_evento = self.request.POST.get('disciplina')
        pk_subdisciplina_evento = self.request.POST.get('subdisciplina')

        evento = Evento(
            nombre = nombre_evento.upper(),
            disciplina = Disciplina.objects.get(pk=pk_disciplina_evento),
            subdisciplina = Subdisciplina.objects.get(pk=pk_subdisciplina_evento),
            creado_por = self.request.user,
            finalizado = False
        )
        evento.save()
        print(">>>> EventoCrear >>> Evento creado por Mi",evento)

        nombres_categoria=self.request.POST.getlist('nombre_categoria')
        print(">>>> EventoCrear >>> nombres_categoria",nombres_categoria)

        unica_categoria = self.request.POST.getlist('unica')
        print(">>>> EventoCrear >>> unica_categoria",unica_categoria)

        rama_categoria = self.request.POST.getlist('rama_seleccionada')
        Edad_min_categoria = self.request.POST.getlist('Edad_min')
        Edad_max_categoria = self.request.POST.getlist('Edad_max')
        fecha_inscripcion_max_categoria = self.request.POST.getlist('fecha_inscripcion_max')

        for i in range(len(nombres_categoria)):
            print("Voy a crear la categoria ", i)
            if unica_categoria[i] == 'Si':
                print("entro a crear categoria unica")
                categoria = Categoria.objects.create(
                    nombre =nombres_categoria[i],
                    genero ='MX',
                    evento = evento
                    # fecha_inscripcion_maxima = fecha_inscripcion_max_categoria[i],
                )
                #aca ya qesrta el modelo en la base de datos
            elif unica_categoria[i] == 'No':
                print("entro a crear categoria normal")
                print("aqui esta el genero",rama_categoria[i])
                if rama_categoria[i] == 'Masculino':
                    genero = 'M'
                
                elif rama_categoria[i] == 'Femenino':
                    genero = 'F'
                elif rama_categoria[i] == 'Mixto':
                    genero = 'MX'

                categoria = Categoria(
                    nombre=nombres_categoria[i],
                    genero=genero,
                    edad_min= Edad_min_categoria[i],
                    edad_max = Edad_max_categoria[i],
                    fecha_inscripcion_maxima = fecha_inscripcion_max_categoria[i],
                    evento = evento      
                )
                categoria.save()
            else:
                print("Error en Creacion de Categorias", categoria)
                
            
            
        
        nombres_escenario = self.request.POST.getlist('nombre_escenario')
        for i in range(len(nombres_escenario)):
            escenario = Escenario(
                nombre=nombres_escenario[i],
                evento = evento
            )
            
            escenario.save()
            
        
        return HttpResponseRedirect(reverse_lazy('home'))

class EventoDetalle(View):

    def get(self, request, *args, **kwargs):
        pk_evento = self.kwargs['pk']
        context = {}
        context['evento'] = Evento.objects.get(pk=pk_evento)
        context['categorias'] = Categoria.objects.filter(evento__pk=pk_evento)
        context['escenarios'] = Escenario.objects.filter(evento__pk=pk_evento)
        template = 'evento/detalle.html'
        return render(self.request, template, context)

class SubdisciplinasDeDisciplina(View):
    def get(self, request, *args, **kwargs):
        disciplina_pk = request.GET.get('disciplina_pk')
        print('PK: ',disciplina_pk)
        if (Disciplina.objects.filter(pk=disciplina_pk).exists()):
            disciplina = Disciplina.objects.get(pk=disciplina_pk)
            subdisciplinas = disciplina.subdisciplinas.all()
        subdisciplinas_list = []
        for subdisciplina in subdisciplinas:
            subdisciplinas_list.append(
                {
                    'pk': subdisciplina.pk,
                    'nombre': subdisciplina.nombre,
                }
            )
        respuesta = {'subdisciplinas':subdisciplinas_list}

        print("subdisciplinas lista: ", respuesta)
        return JsonResponse(respuesta, safe=False)

class EventoBorrar(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
            pk_evento = request.GET.get('pk_evento')
            if (Evento.objects.filter(pk=pk_evento).exists()):
                evento = Evento.objects.get(pk=pk_evento)
                evento.activo = False
                print("Evento Borradado")
                evento.save()
                mensaje = "Evento Borrado Satisfactoriamente"
            else:
                mensaje = "El evento no existe"
                print("Evento No exciste")

        return JsonResponse({'mensaje':mensaje})



class CategoriaCrear(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("LLEGO A CONSULTAR.......")
        
        mensaje = "Partidos Generados Satisfactoriamente"

        return JsonResponse({'mensaje':mensaje})

class CategoriaList(View):
   def get(self, request, *args, **kwargs):
        pk_evento = self.kwargs['pk']
        context = {}
        context['evento'] = Evento.objects.get(pk=pk_evento)
        categorias = []
        for categoria in Categoria.objects.filter(evento__pk=pk_evento):
            categoria_info = {
                'pk' : categoria.pk,
                'nombre' : categoria.nombre,
                'numero_equipos_inscritos' : Equipo.objects.filter(categoria = categoria).count(),
                'numero_equipos_aprobados' : Equipo.objects.filter(categoria = categoria, aprobado = True).count(),
                'genero' : categoria.get_genero_display(),
            }
            categorias.append(categoria_info)
        context['categorias'] = categorias
        evento = Evento.objects.get(pk=self.kwargs['pk'])
        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre
        template = 'categoria/categoria_list.html'
        return render(self.request, template, context)


class CategoriaDetalle(ListView):
    template_name = "admin/fases/inicial.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        evento = Evento.objects.get(pk=self.kwargs['pk'])
        categoria = Categoria.objects.get(pk=self.kwargs['pk_categoria'])
        pk_categoria = self.kwargs['pk_categoria']

        context['pk_evento'] = evento.pk
        context['nombre_evento'] = evento.nombre
        context['categorias'] = Categoria.objects.filter(evento=evento)

        context['equipos'] = Equipo.objects.filter(categoria=categoria)

        return context


# class EscenarioCrear(View):
#     def get(self, request, *args, **kwargs):
#         if request.is_ajax():
#             print("LLEGO A CONSULTAR.......")