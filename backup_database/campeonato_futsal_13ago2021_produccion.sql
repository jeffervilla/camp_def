--
-- PostgreSQL database dump
--

-- Dumped from database version 11.6
-- Dumped by pg_dump version 11.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin2_userprofile; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.admin2_userprofile (
    id integer NOT NULL,
    usuario_id integer NOT NULL
);


ALTER TABLE public.admin2_userprofile OWNER TO grupoda2;

--
-- Name: admin2_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.admin2_userprofile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin2_userprofile_id_seq OWNER TO grupoda2;

--
-- Name: admin2_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.admin2_userprofile_id_seq OWNED BY public.admin2_userprofile.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO grupoda2;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO grupoda2;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO grupoda2;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO grupoda2;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO grupoda2;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO grupoda2;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO grupoda2;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO grupoda2;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO grupoda2;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO grupoda2;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO grupoda2;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO grupoda2;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: campeonato_abono; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.campeonato_abono (
    id integer NOT NULL,
    valor integer NOT NULL,
    fecha timestamp with time zone,
    archivo character varying(100),
    equipo_id integer NOT NULL,
    evento_id integer NOT NULL,
    CONSTRAINT campeonato_abono_valor_check CHECK ((valor >= 0))
);


ALTER TABLE public.campeonato_abono OWNER TO grupoda2;

--
-- Name: campeonato_abono_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.campeonato_abono_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campeonato_abono_id_seq OWNER TO grupoda2;

--
-- Name: campeonato_abono_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.campeonato_abono_id_seq OWNED BY public.campeonato_abono.id;


--
-- Name: campeonato_activarclasificados; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.campeonato_activarclasificados (
    id integer NOT NULL,
    fase character varying(512),
    activar boolean NOT NULL
);


ALTER TABLE public.campeonato_activarclasificados OWNER TO grupoda2;

--
-- Name: campeonato_activarclasificados_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.campeonato_activarclasificados_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campeonato_activarclasificados_id_seq OWNER TO grupoda2;

--
-- Name: campeonato_activarclasificados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.campeonato_activarclasificados_id_seq OWNED BY public.campeonato_activarclasificados.id;


--
-- Name: campeonato_equipo; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.campeonato_equipo (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    observaciones text,
    abono_inscripcion integer,
    saldo_inscripcion integer,
    activo boolean NOT NULL,
    aprobado boolean,
    categoria_id integer,
    creado_por_id integer,
    departamento_id integer,
    evento_id integer NOT NULL,
    municipio_id integer,
    CONSTRAINT campeonato_equipo_abono_inscripcion_check CHECK ((abono_inscripcion >= 0)),
    CONSTRAINT campeonato_equipo_saldo_inscripcion_check CHECK ((saldo_inscripcion >= 0))
);


ALTER TABLE public.campeonato_equipo OWNER TO grupoda2;

--
-- Name: campeonato_equipo_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.campeonato_equipo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campeonato_equipo_id_seq OWNER TO grupoda2;

--
-- Name: campeonato_equipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.campeonato_equipo_id_seq OWNED BY public.campeonato_equipo.id;


--
-- Name: campeonato_grupo; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.campeonato_grupo (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    numero_equipos integer NOT NULL,
    numero_clasificados integer NOT NULL,
    categoria_id integer NOT NULL,
    evento_id integer NOT NULL,
    CONSTRAINT campeonato_grupo_numero_clasificados_check CHECK ((numero_clasificados >= 0)),
    CONSTRAINT campeonato_grupo_numero_equipos_check CHECK ((numero_equipos >= 0))
);


ALTER TABLE public.campeonato_grupo OWNER TO grupoda2;

--
-- Name: campeonato_grupo_equipos; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.campeonato_grupo_equipos (
    id integer NOT NULL,
    grupo_id integer NOT NULL,
    equipo_id integer NOT NULL
);


ALTER TABLE public.campeonato_grupo_equipos OWNER TO grupoda2;

--
-- Name: campeonato_grupo_equipos_clasificados; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.campeonato_grupo_equipos_clasificados (
    id integer NOT NULL,
    grupo_id integer NOT NULL,
    equipo_id integer NOT NULL
);


ALTER TABLE public.campeonato_grupo_equipos_clasificados OWNER TO grupoda2;

--
-- Name: campeonato_grupo_equipos_clasificados_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.campeonato_grupo_equipos_clasificados_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campeonato_grupo_equipos_clasificados_id_seq OWNER TO grupoda2;

--
-- Name: campeonato_grupo_equipos_clasificados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.campeonato_grupo_equipos_clasificados_id_seq OWNED BY public.campeonato_grupo_equipos_clasificados.id;


--
-- Name: campeonato_grupo_equipos_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.campeonato_grupo_equipos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campeonato_grupo_equipos_id_seq OWNER TO grupoda2;

--
-- Name: campeonato_grupo_equipos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.campeonato_grupo_equipos_id_seq OWNED BY public.campeonato_grupo_equipos.id;


--
-- Name: campeonato_grupo_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.campeonato_grupo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campeonato_grupo_id_seq OWNER TO grupoda2;

--
-- Name: campeonato_grupo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.campeonato_grupo_id_seq OWNED BY public.campeonato_grupo.id;


--
-- Name: campeonato_jugadorespartido; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.campeonato_jugadorespartido (
    id integer NOT NULL,
    gol_partido integer,
    amarilla_partido integer,
    roja_partido integer,
    numero integer,
    local boolean NOT NULL,
    evento_id integer NOT NULL,
    jugador_id integer NOT NULL
);


ALTER TABLE public.campeonato_jugadorespartido OWNER TO grupoda2;

--
-- Name: campeonato_jugadorespartido_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.campeonato_jugadorespartido_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campeonato_jugadorespartido_id_seq OWNER TO grupoda2;

--
-- Name: campeonato_jugadorespartido_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.campeonato_jugadorespartido_id_seq OWNED BY public.campeonato_jugadorespartido.id;


--
-- Name: campeonato_partido; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.campeonato_partido (
    id integer NOT NULL,
    numero_partido integer,
    fase character varying(512),
    arbitro1 character varying(512),
    arbitro2 character varying(512),
    arbitro3 character varying(512),
    cronogramista character varying(512),
    coordinador character varying(512),
    fecha timestamp with time zone,
    soporte character varying(100),
    estado boolean NOT NULL,
    categoria_id integer NOT NULL,
    evento_id integer NOT NULL,
    ganador_id integer,
    grupo_id integer,
    local_id integer NOT NULL,
    visitante_id integer NOT NULL,
    CONSTRAINT campeonato_partido_numero_partido_check CHECK ((numero_partido >= 0))
);


ALTER TABLE public.campeonato_partido OWNER TO grupoda2;

--
-- Name: campeonato_partido_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.campeonato_partido_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campeonato_partido_id_seq OWNER TO grupoda2;

--
-- Name: campeonato_partido_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.campeonato_partido_id_seq OWNED BY public.campeonato_partido.id;


--
-- Name: campeonato_partido_jugadores; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.campeonato_partido_jugadores (
    id integer NOT NULL,
    partido_id integer NOT NULL,
    jugadorespartido_id integer NOT NULL
);


ALTER TABLE public.campeonato_partido_jugadores OWNER TO grupoda2;

--
-- Name: campeonato_partido_jugadores_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.campeonato_partido_jugadores_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campeonato_partido_jugadores_id_seq OWNER TO grupoda2;

--
-- Name: campeonato_partido_jugadores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.campeonato_partido_jugadores_id_seq OWNED BY public.campeonato_partido_jugadores.id;


--
-- Name: contactos_contacto; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_contacto (
    id integer NOT NULL,
    nombres character varying(512) NOT NULL,
    apellidos character varying(512),
    imagen character varying(100),
    documento character varying(512),
    sexo character varying(16) NOT NULL,
    cargo character varying(254),
    idioma character varying(512),
    zona_horaria character varying(512),
    sitioweb character varying(512),
    activo boolean NOT NULL,
    cliente boolean NOT NULL,
    proveedor boolean NOT NULL,
    empleado boolean NOT NULL,
    ciudad character varying(512),
    codigo_postal character varying(512),
    direccion character varying(512),
    telefono character varying(512),
    email character varying(512),
    es_compania boolean NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    token_logueo character varying(512),
    tipo_logueo character varying(512),
    vencimiento_logueo timestamp with time zone,
    observaciones text,
    es_jugador boolean NOT NULL,
    talla character varying(512),
    edad integer,
    creado_por_id integer,
    equipo_id integer,
    estado_id integer,
    grupo_id integer,
    modificado_por_id integer,
    pais_id integer,
    tipo_identificacion_id integer,
    usuario_id integer
);


ALTER TABLE public.contactos_contacto OWNER TO grupoda2;

--
-- Name: contactos_contacto_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_contacto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_contacto_id_seq OWNER TO grupoda2;

--
-- Name: contactos_contacto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_contacto_id_seq OWNED BY public.contactos_contacto.id;


--
-- Name: contactos_contacto_industria; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_contacto_industria (
    id integer NOT NULL,
    contacto_id integer NOT NULL,
    industria_id integer NOT NULL
);


ALTER TABLE public.contactos_contacto_industria OWNER TO grupoda2;

--
-- Name: contactos_contacto_industria_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_contacto_industria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_contacto_industria_id_seq OWNER TO grupoda2;

--
-- Name: contactos_contacto_industria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_contacto_industria_id_seq OWNED BY public.contactos_contacto_industria.id;


--
-- Name: contactos_contactorelacion; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_contactorelacion (
    id integer NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    contacto_id integer NOT NULL,
    contacto_relacionado_id integer NOT NULL,
    creado_por_id integer,
    modificado_por_id integer,
    relacion_id integer NOT NULL
);


ALTER TABLE public.contactos_contactorelacion OWNER TO grupoda2;

--
-- Name: contactos_contactorelacion_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_contactorelacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_contactorelacion_id_seq OWNER TO grupoda2;

--
-- Name: contactos_contactorelacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_contactorelacion_id_seq OWNED BY public.contactos_contactorelacion.id;


--
-- Name: contactos_departamento; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_departamento (
    id integer NOT NULL,
    nombre character varying(64) NOT NULL
);


ALTER TABLE public.contactos_departamento OWNER TO grupoda2;

--
-- Name: contactos_departamento_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_departamento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_departamento_id_seq OWNER TO grupoda2;

--
-- Name: contactos_departamento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_departamento_id_seq OWNED BY public.contactos_departamento.id;


--
-- Name: contactos_industria; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_industria (
    id integer NOT NULL,
    nombre character varying(512),
    activo boolean NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    creado_por_id integer,
    modificado_por_id integer
);


ALTER TABLE public.contactos_industria OWNER TO grupoda2;

--
-- Name: contactos_industria_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_industria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_industria_id_seq OWNER TO grupoda2;

--
-- Name: contactos_industria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_industria_id_seq OWNED BY public.contactos_industria.id;


--
-- Name: contactos_municipio; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_municipio (
    id integer NOT NULL,
    nombre character varying(64) NOT NULL,
    departamento_id integer
);


ALTER TABLE public.contactos_municipio OWNER TO grupoda2;

--
-- Name: contactos_municipio_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_municipio_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_municipio_id_seq OWNER TO grupoda2;

--
-- Name: contactos_municipio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_municipio_id_seq OWNED BY public.contactos_municipio.id;


--
-- Name: contactos_pais; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_pais (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    codigo character varying(2),
    formato_direccion text,
    codigo_telefono integer,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    creado_por_id integer,
    modificado_por_id integer
);


ALTER TABLE public.contactos_pais OWNER TO grupoda2;

--
-- Name: contactos_pais_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_pais_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_pais_id_seq OWNER TO grupoda2;

--
-- Name: contactos_pais_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_pais_id_seq OWNED BY public.contactos_pais.id;


--
-- Name: contactos_paisestado; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_paisestado (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    codigo character varying(128) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    creado_por_id integer,
    modificado_por_id integer,
    pais_id integer NOT NULL
);


ALTER TABLE public.contactos_paisestado OWNER TO grupoda2;

--
-- Name: contactos_paisestado_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_paisestado_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_paisestado_id_seq OWNER TO grupoda2;

--
-- Name: contactos_paisestado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_paisestado_id_seq OWNED BY public.contactos_paisestado.id;


--
-- Name: contactos_relacion; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_relacion (
    id integer NOT NULL,
    nombre character varying(512),
    activo boolean NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    creado_por_id integer,
    modificado_por_id integer
);


ALTER TABLE public.contactos_relacion OWNER TO grupoda2;

--
-- Name: contactos_relacion_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_relacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_relacion_id_seq OWNER TO grupoda2;

--
-- Name: contactos_relacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_relacion_id_seq OWNED BY public.contactos_relacion.id;


--
-- Name: contactos_tipoidentificacion; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_tipoidentificacion (
    id integer NOT NULL,
    nombre character varying(64) NOT NULL
);


ALTER TABLE public.contactos_tipoidentificacion OWNER TO grupoda2;

--
-- Name: contactos_tipoidentificacion_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_tipoidentificacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_tipoidentificacion_id_seq OWNER TO grupoda2;

--
-- Name: contactos_tipoidentificacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_tipoidentificacion_id_seq OWNED BY public.contactos_tipoidentificacion.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO grupoda2;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO grupoda2;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO grupoda2;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO grupoda2;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO grupoda2;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO grupoda2;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO grupoda2;

--
-- Name: evento_categoria; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.evento_categoria (
    id integer NOT NULL,
    nombre character varying(64) NOT NULL,
    genero character varying(16) NOT NULL,
    fecha_limite_inferior date,
    fecha_limite_superior date,
    fecha_maxima_inscripcion date,
    disponible boolean NOT NULL,
    evento_id integer
);


ALTER TABLE public.evento_categoria OWNER TO grupoda2;

--
-- Name: evento_categoria_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.evento_categoria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evento_categoria_id_seq OWNER TO grupoda2;

--
-- Name: evento_categoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.evento_categoria_id_seq OWNED BY public.evento_categoria.id;


--
-- Name: evento_disciplina; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.evento_disciplina (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    activo boolean NOT NULL,
    observaciones text,
    creado_por_id integer
);


ALTER TABLE public.evento_disciplina OWNER TO grupoda2;

--
-- Name: evento_disciplina_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.evento_disciplina_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evento_disciplina_id_seq OWNER TO grupoda2;

--
-- Name: evento_disciplina_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.evento_disciplina_id_seq OWNED BY public.evento_disciplina.id;


--
-- Name: evento_disciplina_subdisciplinas; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.evento_disciplina_subdisciplinas (
    id integer NOT NULL,
    disciplina_id integer NOT NULL,
    subdisciplina_id integer NOT NULL
);


ALTER TABLE public.evento_disciplina_subdisciplinas OWNER TO grupoda2;

--
-- Name: evento_disciplina_subdisciplinas_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.evento_disciplina_subdisciplinas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evento_disciplina_subdisciplinas_id_seq OWNER TO grupoda2;

--
-- Name: evento_disciplina_subdisciplinas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.evento_disciplina_subdisciplinas_id_seq OWNED BY public.evento_disciplina_subdisciplinas.id;


--
-- Name: evento_escenario; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.evento_escenario (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    hora_inicio time without time zone,
    hora_fin time without time zone,
    dias_disponibilidad integer NOT NULL,
    disponible boolean NOT NULL,
    observaciones text,
    creado_por_id integer,
    evento_id integer,
    CONSTRAINT evento_escenario_dias_disponibilidad_check CHECK ((dias_disponibilidad >= 0))
);


ALTER TABLE public.evento_escenario OWNER TO grupoda2;

--
-- Name: evento_escenario_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.evento_escenario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evento_escenario_id_seq OWNER TO grupoda2;

--
-- Name: evento_escenario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.evento_escenario_id_seq OWNED BY public.evento_escenario.id;


--
-- Name: evento_evento; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.evento_evento (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    activo boolean NOT NULL,
    finalizado boolean NOT NULL,
    observaciones text,
    creado_por_id integer,
    disciplina_id integer NOT NULL,
    subdisciplina_id integer NOT NULL
);


ALTER TABLE public.evento_evento OWNER TO grupoda2;

--
-- Name: evento_evento_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.evento_evento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evento_evento_id_seq OWNER TO grupoda2;

--
-- Name: evento_evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.evento_evento_id_seq OWNED BY public.evento_evento.id;


--
-- Name: evento_rama; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.evento_rama (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    activo boolean NOT NULL,
    observaciones text,
    creado_por_id integer
);


ALTER TABLE public.evento_rama OWNER TO grupoda2;

--
-- Name: evento_rama_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.evento_rama_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evento_rama_id_seq OWNER TO grupoda2;

--
-- Name: evento_rama_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.evento_rama_id_seq OWNED BY public.evento_rama.id;


--
-- Name: evento_subdisciplina; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.evento_subdisciplina (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    activo boolean NOT NULL,
    observaciones text,
    creado_por_id integer
);


ALTER TABLE public.evento_subdisciplina OWNER TO grupoda2;

--
-- Name: evento_subdisciplina_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.evento_subdisciplina_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evento_subdisciplina_id_seq OWNER TO grupoda2;

--
-- Name: evento_subdisciplina_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.evento_subdisciplina_id_seq OWNED BY public.evento_subdisciplina.id;


--
-- Name: tickets_auditoriaticketestado; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_auditoriaticketestado (
    id integer NOT NULL,
    observaciones character varying(1024) NOT NULL,
    f_registro timestamp with time zone NOT NULL,
    estado_id character varying(16) NOT NULL,
    ticket_id integer NOT NULL,
    usuario_id integer,
    usuario_responsable_id integer
);


ALTER TABLE public.tickets_auditoriaticketestado OWNER TO grupoda2;

--
-- Name: tickets_auditoriaticketestado_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.tickets_auditoriaticketestado_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tickets_auditoriaticketestado_id_seq OWNER TO grupoda2;

--
-- Name: tickets_auditoriaticketestado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.tickets_auditoriaticketestado_id_seq OWNED BY public.tickets_auditoriaticketestado.id;


--
-- Name: tickets_estadoticket; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_estadoticket (
    id character varying(16) NOT NULL,
    nombre character varying(32) NOT NULL,
    color character varying(32) NOT NULL,
    descripcion character varying(256) NOT NULL
);


ALTER TABLE public.tickets_estadoticket OWNER TO grupoda2;

--
-- Name: tickets_observacion; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_observacion (
    id integer NOT NULL,
    registro character varying(1024) NOT NULL,
    f_registro timestamp with time zone NOT NULL,
    ticket_id integer NOT NULL
);


ALTER TABLE public.tickets_observacion OWNER TO grupoda2;

--
-- Name: tickets_observacion_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.tickets_observacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tickets_observacion_id_seq OWNER TO grupoda2;

--
-- Name: tickets_observacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.tickets_observacion_id_seq OWNED BY public.tickets_observacion.id;


--
-- Name: tickets_ticket; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_ticket (
    id integer NOT NULL,
    nombre character varying(64) NOT NULL,
    descripcion text NOT NULL,
    notas text NOT NULL,
    solucion text NOT NULL,
    retroalimentacion text NOT NULL,
    f_inicio timestamp with time zone,
    f_fin timestamp with time zone,
    f_registro timestamp with time zone NOT NULL,
    evaluacion_cliente bigint,
    estado_id character varying(16),
    responsable_id integer,
    solicitante_id integer,
    urgencia_id character varying(16) NOT NULL
);


ALTER TABLE public.tickets_ticket OWNER TO grupoda2;

--
-- Name: tickets_ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.tickets_ticket_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tickets_ticket_id_seq OWNER TO grupoda2;

--
-- Name: tickets_ticket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.tickets_ticket_id_seq OWNED BY public.tickets_ticket.id;


--
-- Name: tickets_tipourgencia; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_tipourgencia (
    id character varying(16) NOT NULL,
    nombre character varying(32) NOT NULL,
    tiempo_solucion integer NOT NULL,
    descripcion character varying(256) NOT NULL
);


ALTER TABLE public.tickets_tipourgencia OWNER TO grupoda2;

--
-- Name: tickets_userprofile; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_userprofile (
    user_id integer NOT NULL,
    telefono character varying(64) NOT NULL
);


ALTER TABLE public.tickets_userprofile OWNER TO grupoda2;

--
-- Name: admin2_userprofile id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.admin2_userprofile ALTER COLUMN id SET DEFAULT nextval('public.admin2_userprofile_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: campeonato_abono id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_abono ALTER COLUMN id SET DEFAULT nextval('public.campeonato_abono_id_seq'::regclass);


--
-- Name: campeonato_activarclasificados id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_activarclasificados ALTER COLUMN id SET DEFAULT nextval('public.campeonato_activarclasificados_id_seq'::regclass);


--
-- Name: campeonato_equipo id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_equipo ALTER COLUMN id SET DEFAULT nextval('public.campeonato_equipo_id_seq'::regclass);


--
-- Name: campeonato_grupo id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo ALTER COLUMN id SET DEFAULT nextval('public.campeonato_grupo_id_seq'::regclass);


--
-- Name: campeonato_grupo_equipos id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo_equipos ALTER COLUMN id SET DEFAULT nextval('public.campeonato_grupo_equipos_id_seq'::regclass);


--
-- Name: campeonato_grupo_equipos_clasificados id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo_equipos_clasificados ALTER COLUMN id SET DEFAULT nextval('public.campeonato_grupo_equipos_clasificados_id_seq'::regclass);


--
-- Name: campeonato_jugadorespartido id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_jugadorespartido ALTER COLUMN id SET DEFAULT nextval('public.campeonato_jugadorespartido_id_seq'::regclass);


--
-- Name: campeonato_partido id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido ALTER COLUMN id SET DEFAULT nextval('public.campeonato_partido_id_seq'::regclass);


--
-- Name: campeonato_partido_jugadores id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido_jugadores ALTER COLUMN id SET DEFAULT nextval('public.campeonato_partido_jugadores_id_seq'::regclass);


--
-- Name: contactos_contacto id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto ALTER COLUMN id SET DEFAULT nextval('public.contactos_contacto_id_seq'::regclass);


--
-- Name: contactos_contacto_industria id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto_industria ALTER COLUMN id SET DEFAULT nextval('public.contactos_contacto_industria_id_seq'::regclass);


--
-- Name: contactos_contactorelacion id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion ALTER COLUMN id SET DEFAULT nextval('public.contactos_contactorelacion_id_seq'::regclass);


--
-- Name: contactos_departamento id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_departamento ALTER COLUMN id SET DEFAULT nextval('public.contactos_departamento_id_seq'::regclass);


--
-- Name: contactos_industria id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_industria ALTER COLUMN id SET DEFAULT nextval('public.contactos_industria_id_seq'::regclass);


--
-- Name: contactos_municipio id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_municipio ALTER COLUMN id SET DEFAULT nextval('public.contactos_municipio_id_seq'::regclass);


--
-- Name: contactos_pais id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais ALTER COLUMN id SET DEFAULT nextval('public.contactos_pais_id_seq'::regclass);


--
-- Name: contactos_paisestado id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado ALTER COLUMN id SET DEFAULT nextval('public.contactos_paisestado_id_seq'::regclass);


--
-- Name: contactos_relacion id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_relacion ALTER COLUMN id SET DEFAULT nextval('public.contactos_relacion_id_seq'::regclass);


--
-- Name: contactos_tipoidentificacion id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_tipoidentificacion ALTER COLUMN id SET DEFAULT nextval('public.contactos_tipoidentificacion_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: evento_categoria id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_categoria ALTER COLUMN id SET DEFAULT nextval('public.evento_categoria_id_seq'::regclass);


--
-- Name: evento_disciplina id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_disciplina ALTER COLUMN id SET DEFAULT nextval('public.evento_disciplina_id_seq'::regclass);


--
-- Name: evento_disciplina_subdisciplinas id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_disciplina_subdisciplinas ALTER COLUMN id SET DEFAULT nextval('public.evento_disciplina_subdisciplinas_id_seq'::regclass);


--
-- Name: evento_escenario id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_escenario ALTER COLUMN id SET DEFAULT nextval('public.evento_escenario_id_seq'::regclass);


--
-- Name: evento_evento id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_evento ALTER COLUMN id SET DEFAULT nextval('public.evento_evento_id_seq'::regclass);


--
-- Name: evento_rama id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_rama ALTER COLUMN id SET DEFAULT nextval('public.evento_rama_id_seq'::regclass);


--
-- Name: evento_subdisciplina id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_subdisciplina ALTER COLUMN id SET DEFAULT nextval('public.evento_subdisciplina_id_seq'::regclass);


--
-- Name: tickets_auditoriaticketestado id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado ALTER COLUMN id SET DEFAULT nextval('public.tickets_auditoriaticketestado_id_seq'::regclass);


--
-- Name: tickets_observacion id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_observacion ALTER COLUMN id SET DEFAULT nextval('public.tickets_observacion_id_seq'::regclass);


--
-- Name: tickets_ticket id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket ALTER COLUMN id SET DEFAULT nextval('public.tickets_ticket_id_seq'::regclass);


--
-- Data for Name: admin2_userprofile; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.admin2_userprofile (id, usuario_id) FROM stdin;
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_group (id, name) FROM stdin;
2	UsuariosWeb
3	Arbitros
1	UsuarioAdmin
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add group	2	add_group
6	Can change group	2	change_group
7	Can delete group	2	delete_group
8	Can view group	2	view_group
9	Can add permission	3	add_permission
10	Can change permission	3	change_permission
11	Can delete permission	3	delete_permission
12	Can view permission	3	view_permission
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add user-profile	7	add_userprofile
26	Can change user-profile	7	change_userprofile
27	Can delete user-profile	7	delete_userprofile
28	Can view user-profile	7	view_userprofile
29	Can add Tipo de Identificacion	8	add_tipoidentificacion
30	Can change Tipo de Identificacion	8	change_tipoidentificacion
31	Can delete Tipo de Identificacion	8	delete_tipoidentificacion
32	Can view Tipo de Identificacion	8	view_tipoidentificacion
33	Can add Pais	9	add_pais
34	Can change Pais	9	change_pais
35	Can delete Pais	9	delete_pais
36	Can view Pais	9	view_pais
37	Can add Municipio	10	add_municipio
38	Can change Municipio	10	change_municipio
39	Can delete Municipio	10	delete_municipio
40	Can view Municipio	10	view_municipio
41	Can add Industria	11	add_industria
42	Can change Industria	11	change_industria
43	Can delete Industria	11	delete_industria
44	Can view Industria	11	view_industria
45	Can add relacion-contacto	12	add_contactorelacion
46	Can change relacion-contacto	12	change_contactorelacion
47	Can delete relacion-contacto	12	delete_contactorelacion
48	Can view relacion-contacto	12	view_contactorelacion
49	Can add Relacion	13	add_relacion
50	Can change Relacion	13	change_relacion
51	Can delete Relacion	13	delete_relacion
52	Can view Relacion	13	view_relacion
53	Can add Departamento	14	add_departamento
54	Can change Departamento	14	change_departamento
55	Can delete Departamento	14	delete_departamento
56	Can view Departamento	14	view_departamento
57	Can add Estado	15	add_paisestado
58	Can change Estado	15	change_paisestado
59	Can delete Estado	15	delete_paisestado
60	Can view Estado	15	view_paisestado
61	Can add Contacto	16	add_contacto
62	Can change Contacto	16	change_contacto
63	Can delete Contacto	16	delete_contacto
64	Can view Contacto	16	view_contacto
65	Can add user profile	17	add_userprofile
66	Can change user profile	17	change_userprofile
67	Can delete user profile	17	delete_userprofile
68	Can view user profile	17	view_userprofile
69	Can add ObservacionTicket	18	add_observacion
70	Can change ObservacionTicket	18	change_observacion
71	Can delete ObservacionTicket	18	delete_observacion
72	Can view ObservacionTicket	18	view_observacion
73	Can add AuditoriaTicketEstado	19	add_auditoriaticketestado
74	Can change AuditoriaTicketEstado	19	change_auditoriaticketestado
75	Can delete AuditoriaTicketEstado	19	delete_auditoriaticketestado
76	Can view AuditoriaTicketEstado	19	view_auditoriaticketestado
77	Can add Ticket	20	add_ticket
78	Can change Ticket	20	change_ticket
79	Can delete Ticket	20	delete_ticket
80	Can view Ticket	20	view_ticket
81	Can add estado - caso	21	add_estadoticket
82	Can change estado - caso	21	change_estadoticket
83	Can delete estado - caso	21	delete_estadoticket
84	Can view estado - caso	21	view_estadoticket
85	Can add tipo - urgencia	22	add_tipourgencia
86	Can change tipo - urgencia	22	change_tipourgencia
87	Can delete tipo - urgencia	22	delete_tipourgencia
88	Can view tipo - urgencia	22	view_tipourgencia
89	Can add activar clasificados	23	add_activarclasificados
90	Can change activar clasificados	23	change_activarclasificados
91	Can delete activar clasificados	23	delete_activarclasificados
92	Can view activar clasificados	23	view_activarclasificados
93	Can add Jugador_Partido	24	add_jugadorespartido
94	Can change Jugador_Partido	24	change_jugadorespartido
95	Can delete Jugador_Partido	24	delete_jugadorespartido
96	Can view Jugador_Partido	24	view_jugadorespartido
97	Can add Partido	25	add_partido
98	Can change Partido	25	change_partido
99	Can delete Partido	25	delete_partido
100	Can view Partido	25	view_partido
101	Can add Grupo	26	add_grupo
102	Can change Grupo	26	change_grupo
103	Can delete Grupo	26	delete_grupo
104	Can view Grupo	26	view_grupo
105	Can add Abono	27	add_abono
106	Can change Abono	27	change_abono
107	Can delete Abono	27	delete_abono
108	Can view Abono	27	view_abono
109	Can add equipo	28	add_equipo
110	Can change equipo	28	change_equipo
111	Can delete equipo	28	delete_equipo
112	Can view equipo	28	view_equipo
113	Can add Categoria	29	add_categoria
114	Can change Categoria	29	change_categoria
115	Can delete Categoria	29	delete_categoria
116	Can view Categoria	29	view_categoria
117	Can add Disciplina	30	add_disciplina
118	Can change Disciplina	30	change_disciplina
119	Can delete Disciplina	30	delete_disciplina
120	Can view Disciplina	30	view_disciplina
121	Can add Subcdisciplina	31	add_subdisciplina
122	Can change Subcdisciplina	31	change_subdisciplina
123	Can delete Subcdisciplina	31	delete_subdisciplina
124	Can view Subcdisciplina	31	view_subdisciplina
125	Can add Rama	32	add_rama
126	Can change Rama	32	change_rama
127	Can delete Rama	32	delete_rama
128	Can view Rama	32	view_rama
129	Can add Escenario	33	add_escenario
130	Can change Escenario	33	change_escenario
131	Can delete Escenario	33	delete_escenario
132	Can view Escenario	33	view_escenario
133	Can add Evento	34	add_evento
134	Can change Evento	34	change_evento
135	Can delete Evento	34	delete_evento
136	Can view Evento	34	view_evento
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$150000$65uMMqEXaVRk$z4bxgowAXx04FI2t+uP1ruiZoBNiV3E5wIC7WK7K3Kg=	2021-08-12 10:36:30.71486-05	t	admin				t	t	2021-08-12 10:24:44.832175-05
3	pbkdf2_sha256$150000$l3sgfOxXcAxl$oi8nUx14M9W2/6IU3mNK8a46FkyfnjdnsLRpd0nA/oM=	2021-08-13 11:39:43.545921-05	f	andres.garcia@grupoda2.com	Andres Garcia		andres.garcia@grupoda2.com	f	t	2021-08-12 10:39:23.37347-05
6	pbkdf2_sha256$150000$NnBMZjn0BojM$/cJjYio1wYCLF3ILYgrikw6kxVDd/zCrP7wWnh8NOn4=	2021-08-13 11:52:51.768132-05	f	jeisson@grupoda2.com	Jeisson Rodriguez		jeisson@grupoda2.com	f	t	2021-08-12 10:50:15.626149-05
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: campeonato_abono; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.campeonato_abono (id, valor, fecha, archivo, equipo_id, evento_id) FROM stdin;
\.


--
-- Data for Name: campeonato_activarclasificados; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.campeonato_activarclasificados (id, fase, activar) FROM stdin;
\.


--
-- Data for Name: campeonato_equipo; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.campeonato_equipo (id, nombre, observaciones, abono_inscripcion, saldo_inscripcion, activo, aprobado, categoria_id, creado_por_id, departamento_id, evento_id, municipio_id) FROM stdin;
2	equipo1		0	1500000	t	\N	1	3	27	1	632
3	equipo2		0	1500000	t	\N	1	3	15	1	625
4	equipo3		0	1500000	t	\N	1	3	15	1	1033
5	equipo4		0	1500000	t	\N	1	3	29	1	546
1	equipo0		0	1500000	t	t	1	3	13	1	942
\.


--
-- Data for Name: campeonato_grupo; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.campeonato_grupo (id, nombre, numero_equipos, numero_clasificados, categoria_id, evento_id) FROM stdin;
\.


--
-- Data for Name: campeonato_grupo_equipos; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.campeonato_grupo_equipos (id, grupo_id, equipo_id) FROM stdin;
\.


--
-- Data for Name: campeonato_grupo_equipos_clasificados; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.campeonato_grupo_equipos_clasificados (id, grupo_id, equipo_id) FROM stdin;
\.


--
-- Data for Name: campeonato_jugadorespartido; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.campeonato_jugadorespartido (id, gol_partido, amarilla_partido, roja_partido, numero, local, evento_id, jugador_id) FROM stdin;
\.


--
-- Data for Name: campeonato_partido; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.campeonato_partido (id, numero_partido, fase, arbitro1, arbitro2, arbitro3, cronogramista, coordinador, fecha, soporte, estado, categoria_id, evento_id, ganador_id, grupo_id, local_id, visitante_id) FROM stdin;
\.


--
-- Data for Name: campeonato_partido_jugadores; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.campeonato_partido_jugadores (id, partido_id, jugadorespartido_id) FROM stdin;
\.


--
-- Data for Name: contactos_contacto; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_contacto (id, nombres, apellidos, imagen, documento, sexo, cargo, idioma, zona_horaria, sitioweb, activo, cliente, proveedor, empleado, ciudad, codigo_postal, direccion, telefono, email, es_compania, fecha_creacion, modificado_fecha, token_logueo, tipo_logueo, vencimiento_logueo, observaciones, es_jugador, talla, edad, creado_por_id, equipo_id, estado_id, grupo_id, modificado_por_id, pais_id, tipo_identificacion_id, usuario_id) FROM stdin;
1	Andres Garcia	\N		\N						t	f	f	f				3166128479	andres.garcia@grupoda2.com	f	2021-08-12 10:39:23.808933-05	\N			\N		f	\N	\N	\N	\N	\N	2	\N	\N	\N	3
2	Jeisson Rodriguez	\N		\N						t	f	f	f				3166962714	jeisson@grupoda2.com	f	2021-08-12 10:50:16.049043-05	\N			\N		f	\N	\N	\N	\N	\N	1	\N	\N	\N	6
3	jugador0	jugador0		11111111	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.642316-05	\N			\N		t	S	15	3	1	\N	\N	\N	\N	2	\N
4	jugador1	jugador1		11111112	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.666627-05	\N			\N		t	S	15	3	1	\N	\N	\N	\N	2	\N
5	jugador2	jugador2		11111113	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.700084-05	\N			\N		t	S	15	3	1	\N	\N	\N	\N	2	\N
6	jugador3	jugador3		11111114	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.706406-05	\N			\N		t	S	15	3	1	\N	\N	\N	\N	2	\N
7	tecnico3	tecnico3		11111115	Masculino					t	f	f	f				22222222	example@example.com	f	2021-08-12 14:20:27.714933-05	\N			\N		f	S	15	3	1	\N	\N	\N	\N	2	\N
8	tecnico3	tecnico3		11111116	Masculino					t	f	f	f				22222222	example@example.com	f	2021-08-12 14:20:27.723198-05	\N			\N		f	S	15	3	1	\N	\N	\N	\N	2	\N
9	jugador0	jugador0		11111117	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.739789-05	\N			\N		t	S	15	3	2	\N	\N	\N	\N	2	\N
10	jugador1	jugador1		11111118	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.748155-05	\N			\N		t	S	15	3	2	\N	\N	\N	\N	2	\N
11	jugador2	jugador2		11111119	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.75658-05	\N			\N		t	S	15	3	2	\N	\N	\N	\N	2	\N
12	jugador3	jugador3		11111120	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.764758-05	\N			\N		t	S	15	3	2	\N	\N	\N	\N	2	\N
13	tecnico3	tecnico3		11111121	Masculino					t	f	f	f				22222222	example@example.com	f	2021-08-12 14:20:27.773257-05	\N			\N		f	S	15	3	2	\N	\N	\N	\N	2	\N
14	tecnico3	tecnico3		11111122	Masculino					t	f	f	f				22222222	example@example.com	f	2021-08-12 14:20:27.781529-05	\N			\N		f	S	15	3	2	\N	\N	\N	\N	2	\N
15	jugador0	jugador0		11111123	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.798349-05	\N			\N		t	S	15	3	3	\N	\N	\N	\N	2	\N
16	jugador1	jugador1		11111124	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.806475-05	\N			\N		t	S	15	3	3	\N	\N	\N	\N	2	\N
17	jugador2	jugador2		11111125	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.814889-05	\N			\N		t	S	15	3	3	\N	\N	\N	\N	2	\N
18	jugador3	jugador3		11111126	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.823376-05	\N			\N		t	S	15	3	3	\N	\N	\N	\N	2	\N
19	tecnico3	tecnico3		11111127	Masculino					t	f	f	f				22222222	example@example.com	f	2021-08-12 14:20:27.831687-05	\N			\N		f	S	15	3	3	\N	\N	\N	\N	2	\N
20	tecnico3	tecnico3		11111128	Masculino					t	f	f	f				22222222	example@example.com	f	2021-08-12 14:20:27.839841-05	\N			\N		f	S	15	3	3	\N	\N	\N	\N	2	\N
21	jugador0	jugador0		11111129	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.856565-05	\N			\N		t	S	15	3	4	\N	\N	\N	\N	2	\N
22	jugador1	jugador1		11111130	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.865149-05	\N			\N		t	S	15	3	4	\N	\N	\N	\N	2	\N
23	jugador2	jugador2		11111131	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.873622-05	\N			\N		t	S	15	3	4	\N	\N	\N	\N	2	\N
24	jugador3	jugador3		11111132	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.881951-05	\N			\N		t	S	15	3	4	\N	\N	\N	\N	2	\N
25	tecnico3	tecnico3		11111133	Masculino					t	f	f	f				22222222	example@example.com	f	2021-08-12 14:20:27.890149-05	\N			\N		f	S	15	3	4	\N	\N	\N	\N	2	\N
26	tecnico3	tecnico3		11111134	Masculino					t	f	f	f				22222222	example@example.com	f	2021-08-12 14:20:27.898495-05	\N			\N		f	S	15	3	4	\N	\N	\N	\N	2	\N
27	jugador0	jugador0		11111135	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.91493-05	\N			\N		t	S	15	3	5	\N	\N	\N	\N	2	\N
28	jugador1	jugador1		11111136	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.923244-05	\N			\N		t	S	15	3	5	\N	\N	\N	\N	2	\N
29	jugador2	jugador2		11111137	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.931791-05	\N			\N		t	S	15	3	5	\N	\N	\N	\N	2	\N
30	jugador3	jugador3		11111138	Masculino					t	f	f	f				22222222		f	2021-08-12 14:20:27.940072-05	\N			\N		t	S	15	3	5	\N	\N	\N	\N	2	\N
31	tecnico3	tecnico3		11111139	Masculino					t	f	f	f				22222222	example@example.com	f	2021-08-12 14:20:27.948974-05	\N			\N		f	S	15	3	5	\N	\N	\N	\N	2	\N
32	tecnico3	tecnico3		11111140	Masculino					t	f	f	f				22222222	example@example.com	f	2021-08-12 14:20:27.956849-05	\N			\N		f	S	15	3	5	\N	\N	\N	\N	2	\N
\.


--
-- Data for Name: contactos_contacto_industria; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_contacto_industria (id, contacto_id, industria_id) FROM stdin;
\.


--
-- Data for Name: contactos_contactorelacion; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_contactorelacion (id, fecha_creacion, modificado_fecha, contacto_id, contacto_relacionado_id, creado_por_id, modificado_por_id, relacion_id) FROM stdin;
\.


--
-- Data for Name: contactos_departamento; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_departamento (id, nombre) FROM stdin;
1	Antioquia
2	Boyacá
3	Córdoba
4	Chocó
5	Nariño
6	Santander
7	Atlántico
8	Bolívar
9	Caldas
10	Caquetá
11	Cauca
12	Cesar
13	Cundinamarca
14	Huila
15	La Guajira
16	Magdalena
17	Meta
18	Quindío
19	Risaralda
20	Sucre
21	Tolima
22	Arauca
23	Casanare
24	Putumayo
25	Amazonas
26	Guainía
27	Vaupés
28	Vichada
29	Guaviare
30	Archipiélago de San Andrés, Providencia y Santa Catalina
31	Bogotá D.C.
32	Norte de Santander
33	Valle del Cauca
\.


--
-- Data for Name: contactos_industria; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_industria (id, nombre, activo, fecha_creacion, modificado_fecha, creado_por_id, modificado_por_id) FROM stdin;
\.


--
-- Data for Name: contactos_municipio; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_municipio (id, nombre, departamento_id) FROM stdin;
1	Medellín	1
2	Abejorral	1
3	Abriaquí	1
4	Alejandría	1
5	Amagá	1
6	Amalfi	1
7	Andes	1
8	Angelópolis	1
9	Angostura	1
10	Anorí	1
11	Tununguá	2
12	Anza	1
13	Apartadó	1
14	Arboletes	1
15	Argelia	1
16	Armenia	1
17	Barbosa	1
18	Bello	1
19	Betania	1
20	Betulia	1
21	Ciudad Bolívar	1
22	Briceño	1
23	Buriticá	1
24	Cáceres	1
25	Caicedo	1
26	Caldas	1
27	Campamento	1
28	Cañasgordas	1
29	Caracolí	1
30	Caramanta	1
31	Carepa	1
32	Motavita	2
33	Carolina	1
34	Caucasia	1
35	Chigorodó	1
36	Cisneros	1
37	Cocorná	1
38	Concepción	1
39	Concordia	1
40	Copacabana	1
41	Dabeiba	1
42	Don Matías	1
43	Ebéjico	1
44	El Bagre	1
45	Entrerrios	1
46	Envigado	1
47	Fredonia	1
48	San Bernardo del Viento	3
49	Giraldo	1
50	Girardota	1
51	Gómez Plata	1
52	Istmina	4
53	Guadalupe	1
54	Guarne	1
55	Guatapé	1
56	Heliconia	1
57	Hispania	1
58	Itagui	1
59	Ituango	1
60	Belmira	1
61	Jericó	1
62	La Ceja	1
63	La Estrella	1
64	La Pintada	1
65	La Unión	1
66	Liborina	1
67	Maceo	1
68	Marinilla	1
69	Montebello	1
70	Murindó	1
71	Mutatá	1
72	Nariño	1
73	Necoclí	1
74	Nechí	1
75	Olaya	1
76	Peñol	1
77	Peque	1
78	Pueblorrico	1
79	Puerto Berrío	1
80	Puerto Nare	1
81	Puerto Triunfo	1
82	Remedios	1
83	Retiro	1
84	Rionegro	1
85	Sabanalarga	1
86	Sabaneta	1
87	Salgar	1
88	Ciénega	2
89	Santacruz	5
90	San Francisco	1
91	San Jerónimo	1
92	Puerto Wilches	6
93	Puerto Parra	6
94	San Luis	1
95	San Pedro	1
96	San Rafael	1
97	San Roque	1
98	San Vicente	1
99	Santa Bárbara	1
100	Santo Domingo	1
101	El Santuario	1
102	Segovia	1
103	Sopetrán	1
104	Támesis	1
105	Tarazá	1
106	Tarso	1
107	Titiribí	1
108	Toledo	1
109	Turbo	1
110	Uramita	1
111	Urrao	1
112	Valdivia	1
113	Valparaíso	1
114	Vegachí	1
115	Venecia	1
116	Yalí	1
117	Yarumal	1
118	Yolombó	1
119	Yondó	1
120	Zaragoza	1
121	Barranquilla	7
122	Baranoa	7
123	Candelaria	7
124	Galapa	7
125	Luruaco	7
126	Malambo	7
127	Manatí	7
128	Piojó	7
129	Polonuevo	7
130	Sabanagrande	7
131	Sabanalarga	7
132	Santa Lucía	7
133	Santo Tomás	7
134	Soledad	7
135	Suan	7
136	Tubará	7
137	Usiacurí	7
138	Achí	8
139	Arenal	8
140	Arjona	8
141	Arroyohondo	8
142	Calamar	8
143	Cantagallo	8
144	Cicuco	8
145	Córdoba	8
146	Clemencia	8
147	El Guamo	8
148	Magangué	8
149	Mahates	8
150	Margarita	8
151	Montecristo	8
152	Mompós	8
153	Morales	8
154	Norosí	8
155	Pinillos	8
156	Regidor	8
157	Río Viejo	8
158	San Estanislao	8
159	San Fernando	8
160	San Juan Nepomuceno	8
161	Santa Catalina	8
162	Santa Rosa	8
163	Simití	8
164	Soplaviento	8
165	Talaigua Nuevo	8
166	Tiquisio	8
167	Turbaco	8
168	Turbaná	8
169	Villanueva	8
170	Tunja	2
171	Almeida	2
172	Aquitania	2
173	Arcabuco	2
174	Berbeo	2
175	Betéitiva	2
176	Boavita	2
177	Boyacá	2
178	Briceño	2
179	Buena Vista	2
180	Busbanzá	2
181	Caldas	2
182	Campohermoso	2
183	Cerinza	2
184	Chinavita	2
185	Chiquinquirá	2
186	Chiscas	2
187	Chita	2
188	Chitaraque	2
189	Chivatá	2
190	Cómbita	2
191	Coper	2
192	Corrales	2
193	Covarachía	2
194	Cubará	2
195	Cucaita	2
196	Cuítiva	2
197	Chíquiza	2
198	Chivor	2
199	Duitama	2
200	El Cocuy	2
201	El Espino	2
202	Firavitoba	2
203	Floresta	2
204	Gachantivá	2
205	Gameza	2
206	Garagoa	2
207	Guacamayas	2
208	Guateque	2
209	Guayatá	2
210	Güicán	2
211	Iza	2
212	Jenesano	2
213	Jericó	2
214	Labranzagrande	2
215	La Capilla	2
216	La Victoria	2
217	Macanal	2
218	Maripí	2
219	Miraflores	2
220	Mongua	2
221	Monguí	2
222	Moniquirá	2
223	Muzo	2
224	Nobsa	2
225	Nuevo Colón	2
226	Oicatá	2
227	Otanche	2
228	Pachavita	2
229	Páez	2
230	Paipa	2
231	Pajarito	2
232	Panqueba	2
233	Pauna	2
234	Paya	2
235	Pesca	2
236	Pisba	2
237	Puerto Boyacá	2
238	Quípama	2
239	Ramiriquí	2
240	Ráquira	2
241	Rondón	2
242	Saboyá	2
243	Sáchica	2
244	Samacá	2
245	San Eduardo	2
246	San Mateo	2
247	Santana	2
248	Santa María	2
249	Santa Sofía	2
250	Sativanorte	2
251	Sativasur	2
252	Siachoque	2
253	Soatá	2
254	Socotá	2
255	Socha	2
256	Sogamoso	2
257	Somondoco	2
258	Sora	2
259	Sotaquirá	2
260	Soracá	2
261	Susacón	2
262	Sutamarchán	2
263	Sutatenza	2
264	Tasco	2
265	Tenza	2
266	Tibaná	2
267	Tinjacá	2
268	Tipacoque	2
269	Toca	2
270	Tópaga	2
271	Tota	2
272	Turmequé	2
273	Tutazá	2
274	Umbita	2
275	Ventaquemada	2
276	Viracachá	2
277	Zetaquira	2
278	Manizales	9
279	Aguadas	9
280	Anserma	9
281	Aranzazu	9
282	Belalcázar	9
283	Chinchiná	9
284	Filadelfia	9
285	La Dorada	9
286	La Merced	9
287	Manzanares	9
288	Marmato	9
289	Marulanda	9
290	Neira	9
291	Norcasia	9
292	Pácora	9
293	Palestina	9
294	Pensilvania	9
295	Riosucio	9
296	Risaralda	9
297	Salamina	9
298	Samaná	9
299	San José	9
300	Supía	9
301	Victoria	9
302	Villamaría	9
303	Viterbo	9
304	Florencia	10
305	Albania	10
306	Curillo	10
307	El Doncello	10
308	El Paujil	10
309	Morelia	10
310	Puerto Rico	10
311	Solano	10
312	Solita	10
313	Valparaíso	10
314	Popayán	11
315	Almaguer	11
316	Argelia	11
317	Balboa	11
318	Bolívar	11
319	Buenos Aires	11
320	Cajibío	11
321	Caldono	11
322	Caloto	11
323	Corinto	11
324	El Tambo	11
325	Florencia	11
326	Guachené	11
327	Guapi	11
328	Inzá	11
329	Jambaló	11
330	La Sierra	11
331	La Vega	11
332	López	11
333	Mercaderes	11
334	Miranda	11
335	Morales	11
336	Padilla	11
337	Patía	11
338	Piamonte	11
339	Piendamó	11
340	Puerto Tejada	11
341	Puracé	11
342	Rosas	11
343	Santa Rosa	11
344	Silvia	11
345	Sotara	11
346	Suárez	11
347	Sucre	11
348	Timbío	11
349	Timbiquí	11
350	Toribio	11
351	Totoró	11
352	Villa Rica	11
353	Valledupar	12
354	Aguachica	12
355	Agustín Codazzi	12
356	Astrea	12
357	Becerril	12
358	Bosconia	12
359	Chimichagua	12
360	Chiriguaná	12
361	Curumaní	12
362	El Copey	12
363	El Paso	12
364	Gamarra	12
365	González	12
366	La Gloria	12
367	Manaure	12
368	Pailitas	12
369	Pelaya	12
370	Pueblo Bello	12
371	La Paz	12
372	San Alberto	12
373	San Diego	12
374	San Martín	12
375	Tamalameque	12
376	Montería	3
377	Ayapel	3
378	Buenavista	3
379	Canalete	3
380	Cereté	3
381	Chimá	3
382	Chinú	3
383	Cotorra	3
384	Lorica	3
385	Los Córdobas	3
386	Momil	3
387	Moñitos	3
388	Planeta Rica	3
389	Pueblo Nuevo	3
390	Puerto Escondido	3
391	Purísima	3
392	Sahagún	3
393	San Andrés Sotavento	3
394	San Antero	3
395	San Pelayo	3
396	Tierralta	3
397	Tuchín	3
398	Valencia	3
399	Anapoima	13
400	Arbeláez	13
401	Beltrán	13
402	Bituima	13
403	Bojacá	13
404	Cabrera	13
405	Cachipay	13
406	Cajicá	13
407	Caparrapí	13
408	Caqueza	13
409	Chaguaní	13
410	Chipaque	13
411	Choachí	13
412	Chocontá	13
413	Cogua	13
414	Cota	13
415	Cucunubá	13
416	El Colegio	13
417	El Rosal	13
418	Fomeque	13
419	Fosca	13
420	Funza	13
421	Fúquene	13
422	Gachala	13
423	Gachancipá	13
424	Gachetá	13
425	Girardot	13
426	Granada	13
427	Guachetá	13
428	Guaduas	13
429	Guasca	13
430	Guataquí	13
431	Guatavita	13
432	Guayabetal	13
433	Gutiérrez	13
434	Jerusalén	13
435	Junín	13
436	La Calera	13
437	La Mesa	13
438	La Palma	13
439	La Peña	13
440	La Vega	13
441	Lenguazaque	13
442	Macheta	13
443	Madrid	13
444	Manta	13
445	Medina	13
446	Mosquera	13
447	Nariño	13
448	Nemocón	13
449	Nilo	13
450	Nimaima	13
451	Nocaima	13
452	Venecia	13
453	Pacho	13
454	Paime	13
455	Pandi	13
456	Paratebueno	13
457	Pasca	13
458	Puerto Salgar	13
459	Pulí	13
460	Quebradanegra	13
461	Quetame	13
462	Quipile	13
463	Apulo	13
464	Ricaurte	13
465	San Bernardo	13
466	San Cayetano	13
467	San Francisco	13
468	Sesquilé	13
469	Sibaté	13
470	Silvania	13
471	Simijaca	13
472	Soacha	13
473	Subachoque	13
474	Suesca	13
475	Supatá	13
476	Susa	13
477	Sutatausa	13
478	Tabio	13
479	Tausa	13
480	Tena	13
481	Tenjo	13
482	Tibacuy	13
483	Tibirita	13
484	Tocaima	13
485	Tocancipá	13
486	Topaipí	13
487	Ubalá	13
488	Ubaque	13
489	Une	13
490	Útica	13
491	Vianí	13
492	Villagómez	13
493	Villapinzón	13
494	Villeta	13
495	Viotá	13
496	Zipacón	13
497	Quibdó	4
498	Acandí	4
499	Alto Baudo	4
500	Atrato	4
501	Bagadó	4
502	Bahía Solano	4
503	Bajo Baudó	4
504	Bojaya	4
505	Cértegui	4
506	Condoto	4
507	Juradó	4
508	Lloró	4
509	Medio Atrato	4
510	Medio Baudó	4
511	Medio San Juan	4
512	Nóvita	4
513	Nuquí	4
514	Río Iro	4
515	Río Quito	4
516	Riosucio	4
517	Sipí	4
518	Unguía	4
519	Neiva	14
520	Acevedo	14
521	Agrado	14
522	Aipe	14
523	Algeciras	14
524	Altamira	14
525	Baraya	14
526	Campoalegre	14
527	Colombia	14
528	Elías	14
529	Garzón	14
530	Gigante	14
531	Guadalupe	14
532	Hobo	14
533	Iquira	14
534	Isnos	14
535	La Argentina	14
536	La Plata	14
537	Nátaga	14
538	Oporapa	14
539	Paicol	14
540	Palermo	14
541	Palestina	14
542	Pital	14
543	Pitalito	14
544	Rivera	14
545	Saladoblanco	14
546	Santa María	14
547	Suaza	14
548	Tarqui	14
549	Tesalia	14
550	Tello	14
551	Teruel	14
552	Timaná	14
553	Villavieja	14
554	Yaguará	14
555	Riohacha	15
556	Albania	15
557	Barrancas	15
558	Dibula	15
559	Distracción	15
560	El Molino	15
561	Fonseca	15
562	Hatonuevo	15
563	Maicao	15
564	Manaure	15
565	Uribia	15
566	Urumita	15
567	Villanueva	15
568	Santa Marta	16
569	Algarrobo	16
570	Aracataca	16
571	Ariguaní	16
572	Cerro San Antonio	16
573	Chivolo	16
574	Concordia	16
575	El Banco	16
576	El Piñon	16
577	El Retén	16
578	Fundación	16
579	Guamal	16
580	Nueva Granada	16
581	Pedraza	16
582	Pivijay	16
583	Plato	16
584	Remolino	16
585	Salamina	16
586	San Zenón	16
587	Santa Ana	16
588	Sitionuevo	16
589	Tenerife	16
590	Zapayán	16
591	Zona Bananera	16
592	Villavicencio	17
593	Acacias	17
594	Cabuyaro	17
595	Cubarral	17
596	Cumaral	17
597	El Calvario	17
598	El Castillo	17
599	El Dorado	17
600	Granada	17
601	Guamal	17
602	Mapiripán	17
603	Mesetas	17
604	La Macarena	17
605	Uribe	17
606	Lejanías	17
607	Puerto Concordia	17
608	Puerto Gaitán	17
609	Puerto López	17
610	Puerto Lleras	17
611	Puerto Rico	17
612	Restrepo	17
613	San Juanito	17
614	San Martín	17
615	Vista Hermosa	17
616	Pasto	5
617	Albán	5
618	Aldana	5
619	Ancuyá	5
620	Barbacoas	5
621	Colón	5
622	Consaca	5
623	Contadero	5
624	Córdoba	5
625	Cuaspud	5
626	Cumbal	5
627	Cumbitara	5
628	El Charco	5
629	El Peñol	5
630	El Rosario	5
631	El Tambo	5
632	Funes	5
633	Guachucal	5
634	Guaitarilla	5
635	Gualmatán	5
636	Iles	5
637	Imués	5
638	Ipiales	5
639	La Cruz	5
640	La Florida	5
641	La Llanada	5
642	La Tola	5
643	La Unión	5
644	Leiva	5
645	Linares	5
646	Los Andes	5
647	Magüí	5
648	Mallama	5
649	Mosquera	5
650	Nariño	5
651	Olaya Herrera	5
652	Ospina	5
653	Francisco Pizarro	5
654	Policarpa	5
655	Potosí	5
656	Providencia	5
657	Puerres	5
658	Pupiales	5
659	Ricaurte	5
660	Roberto Payán	5
661	Samaniego	5
662	Sandoná	5
663	San Bernardo	5
664	San Lorenzo	5
665	San Pablo	5
666	Santa Bárbara	5
667	Sapuyes	5
668	Taminango	5
669	Tangua	5
670	Túquerres	5
671	Yacuanquer	5
672	Armenia	18
673	Buenavista	18
674	Circasia	18
675	Córdoba	18
676	Filandia	18
677	La Tebaida	18
678	Montenegro	18
679	Pijao	18
680	Quimbaya	18
681	Salento	18
682	Pereira	19
683	Apía	19
684	Balboa	19
685	Dosquebradas	19
686	Guática	19
687	La Celia	19
688	La Virginia	19
689	Marsella	19
690	Mistrató	19
691	Pueblo Rico	19
692	Quinchía	19
693	Santuario	19
694	Bucaramanga	6
695	Aguada	6
696	Albania	6
697	Aratoca	6
698	Barbosa	6
699	Barichara	6
700	Barrancabermeja	6
701	Betulia	6
702	Bolívar	6
703	Cabrera	6
704	California	6
705	Carcasí	6
706	Cepitá	6
707	Cerrito	6
708	Charalá	6
709	Charta	6
710	Chipatá	6
711	Cimitarra	6
712	Concepción	6
713	Confines	6
714	Contratación	6
715	Coromoro	6
716	Curití	6
717	El Guacamayo	6
718	El Playón	6
719	Encino	6
720	Enciso	6
721	Florián	6
722	Floridablanca	6
723	Galán	6
724	Gambita	6
725	Girón	6
726	Guaca	6
727	Guadalupe	6
728	Guapotá	6
729	Guavatá	6
730	Güepsa	6
731	Jesús María	6
732	Jordán	6
733	La Belleza	6
734	Landázuri	6
735	La Paz	6
736	Lebríja	6
737	Los Santos	6
738	Macaravita	6
739	Málaga	6
740	Matanza	6
741	Mogotes	6
742	Molagavita	6
743	Ocamonte	6
744	Oiba	6
745	Onzaga	6
746	Palmar	6
747	Páramo	6
748	Piedecuesta	6
749	Pinchote	6
750	Puente Nacional	6
751	Rionegro	6
752	San Andrés	6
753	San Gil	6
754	San Joaquín	6
755	San Miguel	6
756	Santa Bárbara	6
757	Simacota	6
758	Socorro	6
759	Suaita	6
760	Sucre	6
761	Suratá	6
762	Tona	6
763	Vélez	6
764	Vetas	6
765	Villanueva	6
766	Zapatoca	6
767	Sincelejo	20
768	Buenavista	20
769	Caimito	20
770	Coloso	20
771	Coveñas	20
772	Chalán	20
773	El Roble	20
774	Galeras	20
775	Guaranda	20
776	La Unión	20
777	Los Palmitos	20
778	Majagual	20
779	Morroa	20
780	Ovejas	20
781	Palmito	20
782	San Benito Abad	20
783	San Marcos	20
784	San Onofre	20
785	San Pedro	20
786	Sucre	20
787	Tolú Viejo	20
788	Alpujarra	21
789	Alvarado	21
790	Ambalema	21
791	Armero	21
792	Ataco	21
793	Cajamarca	21
794	Chaparral	21
795	Coello	21
796	Coyaima	21
797	Cunday	21
798	Dolores	21
799	Espinal	21
800	Falan	21
801	Flandes	21
802	Fresno	21
803	Guamo	21
804	Herveo	21
805	Honda	21
806	Icononzo	21
807	Mariquita	21
808	Melgar	21
809	Murillo	21
810	Natagaima	21
811	Ortega	21
812	Palocabildo	21
813	Piedras	21
814	Planadas	21
815	Prado	21
816	Purificación	21
817	Rio Blanco	21
818	Roncesvalles	21
819	Rovira	21
820	Saldaña	21
821	Santa Isabel	21
822	Venadillo	21
823	Villahermosa	21
824	Villarrica	21
825	Arauquita	22
826	Cravo Norte	22
827	Fortul	22
828	Puerto Rondón	22
829	Saravena	22
830	Tame	22
831	Arauca	22
832	Yopal	23
833	Aguazul	23
834	Chámeza	23
835	Hato Corozal	23
836	La Salina	23
837	Monterrey	23
838	Pore	23
839	Recetor	23
840	Sabanalarga	23
841	Sácama	23
842	Tauramena	23
843	Trinidad	23
844	Villanueva	23
845	Mocoa	24
846	Colón	24
847	Orito	24
848	Puerto Caicedo	24
849	Puerto Guzmán	24
850	Leguízamo	24
851	Sibundoy	24
852	San Francisco	24
853	San Miguel	24
854	Santiago	24
855	Leticia	25
856	El Encanto	25
857	La Chorrera	25
858	La Pedrera	25
859	La Victoria	25
860	Puerto Arica	25
861	Puerto Nariño	25
862	Puerto Santander	25
863	Tarapacá	25
864	Inírida	26
865	Barranco Minas	26
866	Mapiripana	26
867	San Felipe	26
868	Puerto Colombia	26
869	La Guadalupe	26
870	Cacahual	26
871	Pana Pana	26
872	Morichal	26
873	Mitú	27
874	Caruru	27
875	Pacoa	27
876	Taraira	27
877	Papunaua	27
878	Puerto Carreño	28
879	La Primavera	28
880	Santa Rosalía	28
881	Cumaribo	28
882	San José del Fragua	10
883	Barranca de Upía	17
884	Palmas del Socorro	6
885	San Juan de Río Seco	13
886	Juan de Acosta	7
887	Fuente de Oro	17
888	San Luis de Gaceno	23
889	El Litoral del San Juan	4
890	Villa de San Diego de Ubate	13
891	Barranco de Loba	8
892	Togüí	2
893	Santa Rosa del Sur	8
894	El Cantón del San Pablo	4
895	Villa de Leyva	2
896	San Sebastián de Buenavista	16
897	Paz de Río	2
898	Hatillo de Loba	8
899	Sabanas de San Angel	16
900	Calamar	29
901	Río de Oro	12
902	San Pedro de Uraba	1
903	San José del Guaviare	29
904	Santa Rosa de Viterbo	2
905	Santander de Quilichao	11
906	Miraflores	29
907	Santafé de Antioquia	1
908	San Carlos de Guaroa	17
909	Palmar de Varela	7
910	Santa Rosa de Osos	1
911	San Andrés de Cuerquía	1
912	Valle de San Juan	21
913	San Vicente de Chucurí	6
914	San José de Miranda	6
915	Providencia	30
916	Santa Rosa de Cabal	19
917	Guayabal de Siquima	13
918	Belén de Los Andaquies	10
919	Paz de Ariporo	23
920	Santa Helena del Opón	6
921	San Pablo de Borbur	2
922	La Jagua del Pilar	15
923	La Jagua de Ibirico	12
924	San Luis de Sincé	20
925	San Luis de Gaceno	2
926	El Carmen de Bolívar	8
927	El Carmen de Atrato	4
928	San Juan de Betulia	20
929	Pijiño del Carmen	16
930	Vigía del Fuerte	1
931	San Martín de Loba	8
932	Altos del Rosario	8
933	Carmen de Apicala	21
934	San Antonio del Tequendama	13
935	Sabana de Torres	6
936	El Retorno	29
937	San José de Uré	3
938	San Pedro de Cartago	5
939	Campo de La Cruz	7
940	San Juan de Arama	17
941	San José de La Montaña	1
942	Cartagena del Chairá	10
943	San José del Palmar	4
944	Agua de Dios	13
945	San Jacinto del Cauca	8
946	San Agustín	14
947	El Tablón de Gómez	5
948	San Andrés	30
949	San José de Pare	2
950	Valle de Guamez	24
951	San Pablo de Borbur	8
952	Santiago de Tolú	20
953	Bogotá D.C.	31
954	Carmen de Carupa	13
955	Ciénaga de Oro	3
956	San Juan de Urabá	1
957	San Juan del Cesar	15
958	El Carmen de Chucurí	6
959	El Carmen de Viboral	1
960	Belén de Umbría	19
961	Belén de Bajira	4
962	Valle de San José	6
963	San Luis	21
964	San Miguel de Sema	2
965	San Antonio	21
966	San Benito	6
967	Vergara	13
968	San Carlos	3
969	Puerto Alegría	25
970	Hato	6
971	San Jacinto	8
972	San Sebastián	11
973	San Carlos	1
974	Tuta	2
975	Silos	32
976	Cácota	32
977	El Dovio	33
978	Toledo	32
979	Roldanillo	33
980	Mutiscua	32
981	Argelia	33
982	El Zulia	32
983	Salazar	32
984	Sevilla	33
985	Zarzal	33
986	Cucutilla	32
987	El Cerrito	33
988	Cartago	33
989	Caicedonia	33
990	Puerto Santander	32
991	Gramalote	32
992	El Cairo	33
993	El Tarra	32
994	La Unión	33
995	Restrepo	33
996	Teorama	32
997	Dagua	33
998	Arboledas	32
999	Guacarí	33
1000	Lourdes	32
1001	Ansermanuevo	33
1002	Bochalema	32
1003	Bugalagrande	33
1004	Convención	32
1005	Hacarí	32
1006	La Victoria	33
1007	Herrán	32
1008	Ginebra	33
1009	Yumbo	33
1010	Obando	33
1011	Tibú	32
1012	San Cayetano	32
1013	San Calixto	32
1014	Bolívar	33
1015	La Playa	32
1016	Cali	33
1017	San Pedro	33
1018	Guadalajara de Buga	33
1019	Chinácota	32
1020	Ragonvalia	32
1021	La Esperanza	32
1022	Villa del Rosario	32
1023	Chitagá	32
1024	Calima	33
1025	Sardinata	32
1026	Andalucía	33
1027	Pradera	33
1028	Abrego	32
1029	Los Patios	32
1030	Ocaña	32
1031	Bucarasica	32
1032	Yotoco	33
1033	Palmira	33
1034	Riofrío	33
1035	Santiago	32
1036	Alcalá	33
1037	Versalles	33
1038	Labateca	32
1039	Cachirá	32
1040	Villa Caro	32
1041	Durania	32
1042	El Águila	33
1043	Toro	33
1044	Candelaria	33
1045	La Cumbre	33
1046	Ulloa	33
1047	Trujillo	33
1048	Vijes	33
1049	Chimá	6
1050	Sampués	20
1051	Nunchía	23
1052	Pamplona	32
1053	Albán	13
1054	Yavaraté	27
1055	Montelíbano	3
1056	Puerto Asís	24
1057	Corozal	20
1058	Buesaco	5
1059	Maní	23
1060	El Peñón	8
1061	Tuluá	33
1062	Casabianca	21
1063	Anolaima	13
1064	Chía	13
1065	San Andrés de Tumaco	5
1066	Milán	10
1067	Capitanejo	6
1068	Anzoátegui	21
1069	Florida	33
1070	Repelón	7
1071	Frontino	1
1072	El Peñón	13
1073	Pamplonita	32
1074	Miriti Paraná	25
1075	Támara	23
1076	Tibasosa	2
1077	Páez	11
1078	Ibagué	21
1079	Puerto Colombia	7
1080	Belén	5
1081	Sopó	13
1082	Carmen del Darien	4
1083	Gama	13
1084	Sasaima	13
1085	Chachagüí	5
1086	Cúcuta	32
1087	Cartagena	8
1088	Granada	1
1089	Santa Bárbara de Pinto	16
1090	María la Baja	8
1091	La Montañita	10
1092	San Vicente del Caguán	10
1093	El Peñón	6
1094	Jardín	1
1095	Jamundí	33
1096	Tadó	4
1097	Orocué	23
1098	Líbano	21
1099	Yacopí	13
1100	Calarcá	18
1101	Sonsón	1
1102	El Carmen	32
1103	Lérida	21
1104	La Apartada	3
1105	San Cristóbal	8
1106	Fusagasugá	13
1107	Zambrano	8
1108	La Uvita	2
1109	Zipaquirá	13
1110	Génova	18
1111	Suárez	21
1112	Castilla la Nueva	17
1113	Belén	2
1114	Unión Panamericana	4
1115	Pueblo Viejo	16
1116	Villagarzón	24
1117	Facatativá	13
1118	Puerto Libertador	3
1119	Marquetalia	9
1120	Arboleda	5
1121	Buenaventura	33
1122	Ciénaga	16
1123	Ponedera	7
\.


--
-- Data for Name: contactos_pais; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_pais (id, nombre, codigo, formato_direccion, codigo_telefono, fecha_creacion, modificado_fecha, creado_por_id, modificado_por_id) FROM stdin;
\.


--
-- Data for Name: contactos_paisestado; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_paisestado (id, nombre, codigo, fecha_creacion, modificado_fecha, creado_por_id, modificado_por_id, pais_id) FROM stdin;
\.


--
-- Data for Name: contactos_relacion; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_relacion (id, nombre, activo, fecha_creacion, modificado_fecha, creado_por_id, modificado_por_id) FROM stdin;
\.


--
-- Data for Name: contactos_tipoidentificacion; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_tipoidentificacion (id, nombre) FROM stdin;
0	Otro
1	Cedula Ciudadania
2	Cedula Extranjeria
3	NIT
4	Pasaporte
5	Registro Unico de Identificacion
6	Tarjeta de Identidad
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2021-08-12 10:41:39.949366-05	2	jeisson@grupoda2.com	3		4	1
2	2021-08-12 10:44:53.419736-05	4	jeisson@grupoda2.com	3		4	1
3	2021-08-12 10:46:07.058255-05	5	jeisson@grupoda2.com	3		4	1
4	2021-08-12 11:21:01.802204-05	1	UsuarioAdmin	2	[{"changed": {"fields": ["name"]}}]	2	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	group
3	auth	permission
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	admin2	userprofile
8	contactos	tipoidentificacion
9	contactos	pais
10	contactos	municipio
11	contactos	industria
12	contactos	contactorelacion
13	contactos	relacion
14	contactos	departamento
15	contactos	paisestado
16	contactos	contacto
17	tickets	userprofile
18	tickets	observacion
19	tickets	auditoriaticketestado
20	tickets	ticket
21	tickets	estadoticket
22	tickets	tipourgencia
23	campeonato	activarclasificados
24	campeonato	jugadorespartido
25	campeonato	partido
26	campeonato	grupo
27	campeonato	abono
28	campeonato	equipo
29	evento	categoria
30	evento	disciplina
31	evento	subdisciplina
32	evento	rama
33	evento	escenario
34	evento	evento
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2021-08-12 09:49:42.377687-05
2	auth	0001_initial	2021-08-12 09:49:42.669912-05
3	admin	0001_initial	2021-08-12 09:49:43.0537-05
4	admin	0002_logentry_remove_auto_add	2021-08-12 09:49:43.122548-05
5	admin	0003_logentry_add_action_flag_choices	2021-08-12 09:49:43.137901-05
6	contenttypes	0002_remove_content_type_name	2021-08-12 09:49:43.186398-05
7	auth	0002_alter_permission_name_max_length	2021-08-12 09:49:43.199622-05
8	auth	0003_alter_user_email_max_length	2021-08-12 09:49:43.221849-05
9	auth	0004_alter_user_username_opts	2021-08-12 09:49:43.237317-05
10	auth	0005_alter_user_last_login_null	2021-08-12 09:49:43.256506-05
11	auth	0006_require_contenttypes_0002	2021-08-12 09:49:43.260305-05
12	auth	0007_alter_validators_add_error_messages	2021-08-12 09:49:43.278224-05
13	auth	0008_alter_user_username_max_length	2021-08-12 09:49:43.318297-05
14	auth	0009_alter_user_last_name_max_length	2021-08-12 09:49:43.337556-05
15	auth	0010_alter_group_name_max_length	2021-08-12 09:49:43.354653-05
16	auth	0011_update_proxy_permissions	2021-08-12 09:49:43.381497-05
17	admin2	0001_initial	2021-08-12 09:49:43.443332-05
18	admin2	0002_auto_20210802_1446	2021-08-12 09:49:43.480455-05
19	admin2	0003_auto_20210802_1518	2021-08-12 09:49:43.54083-05
20	evento	0001_initial	2021-08-12 09:49:43.936063-05
21	evento	0002_remove_evento_categorias	2021-08-12 09:49:44.376335-05
22	evento	0003_evento_categorias	2021-08-12 09:49:44.428391-05
23	evento	0004_auto_20210803_1628	2021-08-12 09:49:44.542981-05
24	evento	0005_auto_20210803_1717	2021-08-12 09:49:44.618522-05
26	campeonato	0001_initial	2021-08-12 10:23:10.222073-05
27	contactos	0001_initial	2021-08-12 10:23:11.355043-05
28	campeonato	0002_auto_20210812_1023	2021-08-12 10:23:12.4225-05
29	sessions	0001_initial	2021-08-12 10:23:12.816351-05
30	tickets	0001_initial	2021-08-12 10:23:13.31742-05
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
j74x5pyovnkeb96dxb5uwdvmc06jq4l8	NmZjZmU2YzJiY2VhNGRkYmNlMmExYWJjYzlmYjdhY2IyMGNmZGIzNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2NDVjODA3MThhMDQwN2Q3YzUxNjA2NGY1YTM1OGM2YTk4NGE1ZjJkIn0=	2021-08-26 10:36:30.723702-05
lx2gwn9xm9bshcql8xfzh96mfg1z3xlp	MjAzMzBlNDQyZWFjMzI5YmUxYTIwMWYxODgyMzMwYzQ0ZDI0N2ZjMTp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MWM5NzQ1OWUxYTVjZDRhNWZiMTEyYjE3ZTU4YWNmNDAzZjMxNmJjIn0=	2021-08-27 11:39:43.554819-05
vz2ysyuzfah9hqq3ph6vob1kxmhdigmv	OTcwMjM2ZTJjYjA5ODdkMTE1OGZlNDQ0MDNlNmRiNGI3OTFiNDA1OTp7Il9hdXRoX3VzZXJfaWQiOiI2IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyNzNmMDczYzVlYjBmNGFlZDgxZTkxN2YwMWZmZGIwZjFlODJlYzEwIn0=	2021-08-27 11:52:51.779031-05
\.


--
-- Data for Name: evento_categoria; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.evento_categoria (id, nombre, genero, fecha_limite_inferior, fecha_limite_superior, fecha_maxima_inscripcion, disponible, evento_id) FROM stdin;
1	categoria0	Mixto	\N	\N	\N	t	1
2	categoria1	Mixto	\N	\N	\N	t	1
3	categoria2	Mixto	\N	\N	\N	t	1
\.


--
-- Data for Name: evento_disciplina; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.evento_disciplina (id, nombre, activo, observaciones, creado_por_id) FROM stdin;
1	Futbol	t		\N
2	Basketball	t		\N
3	Voleyball	t		\N
\.


--
-- Data for Name: evento_disciplina_subdisciplinas; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.evento_disciplina_subdisciplinas (id, disciplina_id, subdisciplina_id) FROM stdin;
1	1	1
2	1	2
3	1	3
4	1	4
5	2	5
6	2	6
7	3	7
\.


--
-- Data for Name: evento_escenario; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.evento_escenario (id, nombre, hora_inicio, hora_fin, dias_disponibilidad, disponible, observaciones, creado_por_id, evento_id) FROM stdin;
1	escenario0	\N	\N	0	t		6	1
2	escenario1	\N	\N	0	t		6	1
3	escenario2	\N	\N	0	t		6	1
\.


--
-- Data for Name: evento_evento; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.evento_evento (id, nombre, activo, finalizado, observaciones, creado_por_id, disciplina_id, subdisciplina_id) FROM stdin;
1	Evento de Prueba DA2Soft	t	f		6	1	1
\.


--
-- Data for Name: evento_rama; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.evento_rama (id, nombre, activo, observaciones, creado_por_id) FROM stdin;
\.


--
-- Data for Name: evento_subdisciplina; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.evento_subdisciplina (id, nombre, activo, observaciones, creado_por_id) FROM stdin;
1	Futbol_5	t		\N
2	Futbol_7	t		\N
3	Futbol_9	t		\N
4	Futbol_11	t		\N
5	Basketball_3x3	t		\N
6	Basketball_5x5	t		\N
7	Voleyball_6x6	t		\N
\.


--
-- Data for Name: tickets_auditoriaticketestado; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_auditoriaticketestado (id, observaciones, f_registro, estado_id, ticket_id, usuario_id, usuario_responsable_id) FROM stdin;
\.


--
-- Data for Name: tickets_estadoticket; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_estadoticket (id, nombre, color, descripcion) FROM stdin;
\.


--
-- Data for Name: tickets_observacion; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_observacion (id, registro, f_registro, ticket_id) FROM stdin;
\.


--
-- Data for Name: tickets_ticket; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_ticket (id, nombre, descripcion, notas, solucion, retroalimentacion, f_inicio, f_fin, f_registro, evaluacion_cliente, estado_id, responsable_id, solicitante_id, urgencia_id) FROM stdin;
\.


--
-- Data for Name: tickets_tipourgencia; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_tipourgencia (id, nombre, tiempo_solucion, descripcion) FROM stdin;
\.


--
-- Data for Name: tickets_userprofile; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_userprofile (user_id, telefono) FROM stdin;
\.


--
-- Name: admin2_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.admin2_userprofile_id_seq', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 3, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 136, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 6, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: campeonato_abono_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.campeonato_abono_id_seq', 1, false);


--
-- Name: campeonato_activarclasificados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.campeonato_activarclasificados_id_seq', 1, false);


--
-- Name: campeonato_equipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.campeonato_equipo_id_seq', 5, true);


--
-- Name: campeonato_grupo_equipos_clasificados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.campeonato_grupo_equipos_clasificados_id_seq', 1, false);


--
-- Name: campeonato_grupo_equipos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.campeonato_grupo_equipos_id_seq', 1, false);


--
-- Name: campeonato_grupo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.campeonato_grupo_id_seq', 1, false);


--
-- Name: campeonato_jugadorespartido_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.campeonato_jugadorespartido_id_seq', 1, false);


--
-- Name: campeonato_partido_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.campeonato_partido_id_seq', 1, false);


--
-- Name: campeonato_partido_jugadores_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.campeonato_partido_jugadores_id_seq', 1, false);


--
-- Name: contactos_contacto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_contacto_id_seq', 32, true);


--
-- Name: contactos_contacto_industria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_contacto_industria_id_seq', 1, false);


--
-- Name: contactos_contactorelacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_contactorelacion_id_seq', 1, false);


--
-- Name: contactos_departamento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_departamento_id_seq', 33, true);


--
-- Name: contactos_industria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_industria_id_seq', 1, false);


--
-- Name: contactos_municipio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_municipio_id_seq', 1123, true);


--
-- Name: contactos_pais_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_pais_id_seq', 1, false);


--
-- Name: contactos_paisestado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_paisestado_id_seq', 1, false);


--
-- Name: contactos_relacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_relacion_id_seq', 1, false);


--
-- Name: contactos_tipoidentificacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_tipoidentificacion_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 4, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 34, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 30, true);


--
-- Name: evento_categoria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.evento_categoria_id_seq', 3, true);


--
-- Name: evento_disciplina_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.evento_disciplina_id_seq', 3, true);


--
-- Name: evento_disciplina_subdisciplinas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.evento_disciplina_subdisciplinas_id_seq', 7, true);


--
-- Name: evento_escenario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.evento_escenario_id_seq', 3, true);


--
-- Name: evento_evento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.evento_evento_id_seq', 1, true);


--
-- Name: evento_rama_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.evento_rama_id_seq', 1, false);


--
-- Name: evento_subdisciplina_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.evento_subdisciplina_id_seq', 7, true);


--
-- Name: tickets_auditoriaticketestado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.tickets_auditoriaticketestado_id_seq', 1, false);


--
-- Name: tickets_observacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.tickets_observacion_id_seq', 1, false);


--
-- Name: tickets_ticket_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.tickets_ticket_id_seq', 1, false);


--
-- Name: admin2_userprofile admin2_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.admin2_userprofile
    ADD CONSTRAINT admin2_userprofile_pkey PRIMARY KEY (id);


--
-- Name: admin2_userprofile admin2_userprofile_usuario_id_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.admin2_userprofile
    ADD CONSTRAINT admin2_userprofile_usuario_id_key UNIQUE (usuario_id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: campeonato_abono campeonato_abono_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_abono
    ADD CONSTRAINT campeonato_abono_pkey PRIMARY KEY (id);


--
-- Name: campeonato_activarclasificados campeonato_activarclasificados_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_activarclasificados
    ADD CONSTRAINT campeonato_activarclasificados_pkey PRIMARY KEY (id);


--
-- Name: campeonato_equipo campeonato_equipo_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_equipo
    ADD CONSTRAINT campeonato_equipo_pkey PRIMARY KEY (id);


--
-- Name: campeonato_grupo_equipos_clasificados campeonato_grupo_equipos_clasificados_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo_equipos_clasificados
    ADD CONSTRAINT campeonato_grupo_equipos_clasificados_pkey PRIMARY KEY (id);


--
-- Name: campeonato_grupo_equipos_clasificados campeonato_grupo_equipos_grupo_id_equipo_id_50f88708_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo_equipos_clasificados
    ADD CONSTRAINT campeonato_grupo_equipos_grupo_id_equipo_id_50f88708_uniq UNIQUE (grupo_id, equipo_id);


--
-- Name: campeonato_grupo_equipos campeonato_grupo_equipos_grupo_id_equipo_id_ce472cce_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo_equipos
    ADD CONSTRAINT campeonato_grupo_equipos_grupo_id_equipo_id_ce472cce_uniq UNIQUE (grupo_id, equipo_id);


--
-- Name: campeonato_grupo_equipos campeonato_grupo_equipos_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo_equipos
    ADD CONSTRAINT campeonato_grupo_equipos_pkey PRIMARY KEY (id);


--
-- Name: campeonato_grupo campeonato_grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo
    ADD CONSTRAINT campeonato_grupo_pkey PRIMARY KEY (id);


--
-- Name: campeonato_jugadorespartido campeonato_jugadorespartido_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_jugadorespartido
    ADD CONSTRAINT campeonato_jugadorespartido_pkey PRIMARY KEY (id);


--
-- Name: campeonato_partido_jugadores campeonato_partido_jugad_partido_id_jugadorespart_0b68dfdf_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido_jugadores
    ADD CONSTRAINT campeonato_partido_jugad_partido_id_jugadorespart_0b68dfdf_uniq UNIQUE (partido_id, jugadorespartido_id);


--
-- Name: campeonato_partido_jugadores campeonato_partido_jugadores_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido_jugadores
    ADD CONSTRAINT campeonato_partido_jugadores_pkey PRIMARY KEY (id);


--
-- Name: campeonato_partido campeonato_partido_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido
    ADD CONSTRAINT campeonato_partido_pkey PRIMARY KEY (id);


--
-- Name: contactos_contacto_industria contactos_contacto_indus_contacto_id_industria_id_3ab0c8dc_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto_industria
    ADD CONSTRAINT contactos_contacto_indus_contacto_id_industria_id_3ab0c8dc_uniq UNIQUE (contacto_id, industria_id);


--
-- Name: contactos_contacto_industria contactos_contacto_industria_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto_industria
    ADD CONSTRAINT contactos_contacto_industria_pkey PRIMARY KEY (id);


--
-- Name: contactos_contacto contactos_contacto_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_pkey PRIMARY KEY (id);


--
-- Name: contactos_contacto contactos_contacto_usuario_id_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_usuario_id_key UNIQUE (usuario_id);


--
-- Name: contactos_contactorelacion contactos_contactorelacion_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactorelacion_pkey PRIMARY KEY (id);


--
-- Name: contactos_departamento contactos_departamento_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_departamento
    ADD CONSTRAINT contactos_departamento_pkey PRIMARY KEY (id);


--
-- Name: contactos_industria contactos_industria_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_industria
    ADD CONSTRAINT contactos_industria_pkey PRIMARY KEY (id);


--
-- Name: contactos_municipio contactos_municipio_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_municipio
    ADD CONSTRAINT contactos_municipio_pkey PRIMARY KEY (id);


--
-- Name: contactos_pais contactos_pais_codigo_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais
    ADD CONSTRAINT contactos_pais_codigo_key UNIQUE (codigo);


--
-- Name: contactos_pais contactos_pais_nombre_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais
    ADD CONSTRAINT contactos_pais_nombre_key UNIQUE (nombre);


--
-- Name: contactos_pais contactos_pais_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais
    ADD CONSTRAINT contactos_pais_pkey PRIMARY KEY (id);


--
-- Name: contactos_paisestado contactos_paisestado_pais_id_codigo_3e8d9101_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado
    ADD CONSTRAINT contactos_paisestado_pais_id_codigo_3e8d9101_uniq UNIQUE (pais_id, codigo);


--
-- Name: contactos_paisestado contactos_paisestado_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado
    ADD CONSTRAINT contactos_paisestado_pkey PRIMARY KEY (id);


--
-- Name: contactos_relacion contactos_relacion_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_relacion
    ADD CONSTRAINT contactos_relacion_pkey PRIMARY KEY (id);


--
-- Name: contactos_tipoidentificacion contactos_tipoidentificacion_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_tipoidentificacion
    ADD CONSTRAINT contactos_tipoidentificacion_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: evento_categoria evento_categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_categoria
    ADD CONSTRAINT evento_categoria_pkey PRIMARY KEY (id);


--
-- Name: evento_disciplina evento_disciplina_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_disciplina
    ADD CONSTRAINT evento_disciplina_pkey PRIMARY KEY (id);


--
-- Name: evento_disciplina_subdisciplinas evento_disciplina_subdis_disciplina_id_subdiscipl_c5ea8516_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_disciplina_subdisciplinas
    ADD CONSTRAINT evento_disciplina_subdis_disciplina_id_subdiscipl_c5ea8516_uniq UNIQUE (disciplina_id, subdisciplina_id);


--
-- Name: evento_disciplina_subdisciplinas evento_disciplina_subdisciplinas_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_disciplina_subdisciplinas
    ADD CONSTRAINT evento_disciplina_subdisciplinas_pkey PRIMARY KEY (id);


--
-- Name: evento_escenario evento_escenario_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_escenario
    ADD CONSTRAINT evento_escenario_pkey PRIMARY KEY (id);


--
-- Name: evento_evento evento_evento_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_evento
    ADD CONSTRAINT evento_evento_pkey PRIMARY KEY (id);


--
-- Name: evento_rama evento_rama_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_rama
    ADD CONSTRAINT evento_rama_pkey PRIMARY KEY (id);


--
-- Name: evento_subdisciplina evento_subdisciplina_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_subdisciplina
    ADD CONSTRAINT evento_subdisciplina_pkey PRIMARY KEY (id);


--
-- Name: tickets_auditoriaticketestado tickets_auditoriaticketestado_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado
    ADD CONSTRAINT tickets_auditoriaticketestado_pkey PRIMARY KEY (id);


--
-- Name: tickets_estadoticket tickets_estadoticket_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_estadoticket
    ADD CONSTRAINT tickets_estadoticket_pkey PRIMARY KEY (id);


--
-- Name: tickets_observacion tickets_observacion_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_observacion
    ADD CONSTRAINT tickets_observacion_pkey PRIMARY KEY (id);


--
-- Name: tickets_ticket tickets_ticket_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket
    ADD CONSTRAINT tickets_ticket_pkey PRIMARY KEY (id);


--
-- Name: tickets_tipourgencia tickets_tipourgencia_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_tipourgencia
    ADD CONSTRAINT tickets_tipourgencia_pkey PRIMARY KEY (id);


--
-- Name: tickets_userprofile tickets_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_userprofile
    ADD CONSTRAINT tickets_userprofile_pkey PRIMARY KEY (user_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: campeonato_abono_equipo_id_3d541ddf; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_abono_equipo_id_3d541ddf ON public.campeonato_abono USING btree (equipo_id);


--
-- Name: campeonato_abono_evento_id_fbc7b13f; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_abono_evento_id_fbc7b13f ON public.campeonato_abono USING btree (evento_id);


--
-- Name: campeonato_equipo_categoria_id_56de5214; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_equipo_categoria_id_56de5214 ON public.campeonato_equipo USING btree (categoria_id);


--
-- Name: campeonato_equipo_creado_por_id_090084b1; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_equipo_creado_por_id_090084b1 ON public.campeonato_equipo USING btree (creado_por_id);


--
-- Name: campeonato_equipo_departamento_id_a19e183a; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_equipo_departamento_id_a19e183a ON public.campeonato_equipo USING btree (departamento_id);


--
-- Name: campeonato_equipo_evento_id_62eb3066; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_equipo_evento_id_62eb3066 ON public.campeonato_equipo USING btree (evento_id);


--
-- Name: campeonato_equipo_municipio_id_42cdbee1; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_equipo_municipio_id_42cdbee1 ON public.campeonato_equipo USING btree (municipio_id);


--
-- Name: campeonato_grupo_categoria_id_6528544c; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_grupo_categoria_id_6528544c ON public.campeonato_grupo USING btree (categoria_id);


--
-- Name: campeonato_grupo_equipos_clasificados_equipo_id_d1f8ef08; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_grupo_equipos_clasificados_equipo_id_d1f8ef08 ON public.campeonato_grupo_equipos_clasificados USING btree (equipo_id);


--
-- Name: campeonato_grupo_equipos_clasificados_grupo_id_e7e32de9; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_grupo_equipos_clasificados_grupo_id_e7e32de9 ON public.campeonato_grupo_equipos_clasificados USING btree (grupo_id);


--
-- Name: campeonato_grupo_equipos_equipo_id_a312ede0; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_grupo_equipos_equipo_id_a312ede0 ON public.campeonato_grupo_equipos USING btree (equipo_id);


--
-- Name: campeonato_grupo_equipos_grupo_id_865b4be8; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_grupo_equipos_grupo_id_865b4be8 ON public.campeonato_grupo_equipos USING btree (grupo_id);


--
-- Name: campeonato_grupo_evento_id_9965646f; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_grupo_evento_id_9965646f ON public.campeonato_grupo USING btree (evento_id);


--
-- Name: campeonato_jugadorespartido_evento_id_cbe5122e; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_jugadorespartido_evento_id_cbe5122e ON public.campeonato_jugadorespartido USING btree (evento_id);


--
-- Name: campeonato_jugadorespartido_jugador_id_5437b31a; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_jugadorespartido_jugador_id_5437b31a ON public.campeonato_jugadorespartido USING btree (jugador_id);


--
-- Name: campeonato_partido_categoria_id_ce249ab9; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_partido_categoria_id_ce249ab9 ON public.campeonato_partido USING btree (categoria_id);


--
-- Name: campeonato_partido_evento_id_cef937e5; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_partido_evento_id_cef937e5 ON public.campeonato_partido USING btree (evento_id);


--
-- Name: campeonato_partido_ganador_id_1826eb81; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_partido_ganador_id_1826eb81 ON public.campeonato_partido USING btree (ganador_id);


--
-- Name: campeonato_partido_grupo_id_a1e54728; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_partido_grupo_id_a1e54728 ON public.campeonato_partido USING btree (grupo_id);


--
-- Name: campeonato_partido_jugadores_jugadorespartido_id_9cc79825; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_partido_jugadores_jugadorespartido_id_9cc79825 ON public.campeonato_partido_jugadores USING btree (jugadorespartido_id);


--
-- Name: campeonato_partido_jugadores_partido_id_170226c6; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_partido_jugadores_partido_id_170226c6 ON public.campeonato_partido_jugadores USING btree (partido_id);


--
-- Name: campeonato_partido_local_id_c001a3c3; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_partido_local_id_c001a3c3 ON public.campeonato_partido USING btree (local_id);


--
-- Name: campeonato_partido_visitante_id_349ccacd; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX campeonato_partido_visitante_id_349ccacd ON public.campeonato_partido USING btree (visitante_id);


--
-- Name: contactos_contacto_creado_por_id_21b74f22; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_creado_por_id_21b74f22 ON public.contactos_contacto USING btree (creado_por_id);


--
-- Name: contactos_contacto_equipo_id_d14f9901; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_equipo_id_d14f9901 ON public.contactos_contacto USING btree (equipo_id);


--
-- Name: contactos_contacto_estado_id_bfa0a26f; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_estado_id_bfa0a26f ON public.contactos_contacto USING btree (estado_id);


--
-- Name: contactos_contacto_grupo_id_dc60af8d; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_grupo_id_dc60af8d ON public.contactos_contacto USING btree (grupo_id);


--
-- Name: contactos_contacto_industria_contacto_id_91b84802; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_industria_contacto_id_91b84802 ON public.contactos_contacto_industria USING btree (contacto_id);


--
-- Name: contactos_contacto_industria_industria_id_60f8c341; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_industria_industria_id_60f8c341 ON public.contactos_contacto_industria USING btree (industria_id);


--
-- Name: contactos_contacto_modificado_por_id_a362c749; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_modificado_por_id_a362c749 ON public.contactos_contacto USING btree (modificado_por_id);


--
-- Name: contactos_contacto_pais_id_5d8ab74e; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_pais_id_5d8ab74e ON public.contactos_contacto USING btree (pais_id);


--
-- Name: contactos_contacto_tipo_identificacion_id_c657fc86; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_tipo_identificacion_id_c657fc86 ON public.contactos_contacto USING btree (tipo_identificacion_id);


--
-- Name: contactos_contactorelacion_contacto_id_1e98bf9b; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contactorelacion_contacto_id_1e98bf9b ON public.contactos_contactorelacion USING btree (contacto_id);


--
-- Name: contactos_contactorelacion_contacto_relacionado_id_7f14d590; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contactorelacion_contacto_relacionado_id_7f14d590 ON public.contactos_contactorelacion USING btree (contacto_relacionado_id);


--
-- Name: contactos_contactorelacion_creado_por_id_8f9b80d8; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contactorelacion_creado_por_id_8f9b80d8 ON public.contactos_contactorelacion USING btree (creado_por_id);


--
-- Name: contactos_contactorelacion_modificado_por_id_8075cd68; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contactorelacion_modificado_por_id_8075cd68 ON public.contactos_contactorelacion USING btree (modificado_por_id);


--
-- Name: contactos_contactorelacion_relacion_id_480832f8; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contactorelacion_relacion_id_480832f8 ON public.contactos_contactorelacion USING btree (relacion_id);


--
-- Name: contactos_industria_creado_por_id_66da5cb1; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_industria_creado_por_id_66da5cb1 ON public.contactos_industria USING btree (creado_por_id);


--
-- Name: contactos_industria_modificado_por_id_d143c13e; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_industria_modificado_por_id_d143c13e ON public.contactos_industria USING btree (modificado_por_id);


--
-- Name: contactos_municipio_departamento_id_d40485c5; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_municipio_departamento_id_d40485c5 ON public.contactos_municipio USING btree (departamento_id);


--
-- Name: contactos_pais_codigo_3ad22b70_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_pais_codigo_3ad22b70_like ON public.contactos_pais USING btree (codigo varchar_pattern_ops);


--
-- Name: contactos_pais_creado_por_id_ceac3c22; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_pais_creado_por_id_ceac3c22 ON public.contactos_pais USING btree (creado_por_id);


--
-- Name: contactos_pais_modificado_por_id_d3b6c179; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_pais_modificado_por_id_d3b6c179 ON public.contactos_pais USING btree (modificado_por_id);


--
-- Name: contactos_pais_nombre_ebce9cee_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_pais_nombre_ebce9cee_like ON public.contactos_pais USING btree (nombre varchar_pattern_ops);


--
-- Name: contactos_paisestado_creado_por_id_168088dd; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_paisestado_creado_por_id_168088dd ON public.contactos_paisestado USING btree (creado_por_id);


--
-- Name: contactos_paisestado_modificado_por_id_f0c0bfa1; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_paisestado_modificado_por_id_f0c0bfa1 ON public.contactos_paisestado USING btree (modificado_por_id);


--
-- Name: contactos_paisestado_pais_id_910f30ba; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_paisestado_pais_id_910f30ba ON public.contactos_paisestado USING btree (pais_id);


--
-- Name: contactos_relacion_creado_por_id_311687a5; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_relacion_creado_por_id_311687a5 ON public.contactos_relacion USING btree (creado_por_id);


--
-- Name: contactos_relacion_modificado_por_id_094367ca; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_relacion_modificado_por_id_094367ca ON public.contactos_relacion USING btree (modificado_por_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: evento_categoria_evento_id_92abf84f; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX evento_categoria_evento_id_92abf84f ON public.evento_categoria USING btree (evento_id);


--
-- Name: evento_disciplina_creado_por_id_356563d4; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX evento_disciplina_creado_por_id_356563d4 ON public.evento_disciplina USING btree (creado_por_id);


--
-- Name: evento_disciplina_subdisciplinas_disciplina_id_b2b6add4; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX evento_disciplina_subdisciplinas_disciplina_id_b2b6add4 ON public.evento_disciplina_subdisciplinas USING btree (disciplina_id);


--
-- Name: evento_disciplina_subdisciplinas_subdisciplina_id_5b406b8f; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX evento_disciplina_subdisciplinas_subdisciplina_id_5b406b8f ON public.evento_disciplina_subdisciplinas USING btree (subdisciplina_id);


--
-- Name: evento_escenario_creado_por_id_5bf0faa4; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX evento_escenario_creado_por_id_5bf0faa4 ON public.evento_escenario USING btree (creado_por_id);


--
-- Name: evento_escenario_evento_id_b50b0533; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX evento_escenario_evento_id_b50b0533 ON public.evento_escenario USING btree (evento_id);


--
-- Name: evento_evento_creado_por_id_1585026a; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX evento_evento_creado_por_id_1585026a ON public.evento_evento USING btree (creado_por_id);


--
-- Name: evento_evento_disciplina_id_80e92d31; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX evento_evento_disciplina_id_80e92d31 ON public.evento_evento USING btree (disciplina_id);


--
-- Name: evento_evento_subdisciplina_id_6679e423; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX evento_evento_subdisciplina_id_6679e423 ON public.evento_evento USING btree (subdisciplina_id);


--
-- Name: evento_rama_creado_por_id_7a528549; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX evento_rama_creado_por_id_7a528549 ON public.evento_rama USING btree (creado_por_id);


--
-- Name: evento_subdisciplina_creado_por_id_d7868e41; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX evento_subdisciplina_creado_por_id_d7868e41 ON public.evento_subdisciplina USING btree (creado_por_id);


--
-- Name: tickets_auditoriaticketestado_estado_id_21f33d24; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_auditoriaticketestado_estado_id_21f33d24 ON public.tickets_auditoriaticketestado USING btree (estado_id);


--
-- Name: tickets_auditoriaticketestado_estado_id_21f33d24_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_auditoriaticketestado_estado_id_21f33d24_like ON public.tickets_auditoriaticketestado USING btree (estado_id varchar_pattern_ops);


--
-- Name: tickets_auditoriaticketestado_ticket_id_eae8c426; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_auditoriaticketestado_ticket_id_eae8c426 ON public.tickets_auditoriaticketestado USING btree (ticket_id);


--
-- Name: tickets_auditoriaticketestado_usuario_id_ee0dec32; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_auditoriaticketestado_usuario_id_ee0dec32 ON public.tickets_auditoriaticketestado USING btree (usuario_id);


--
-- Name: tickets_auditoriaticketestado_usuario_responsable_id_401ab195; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_auditoriaticketestado_usuario_responsable_id_401ab195 ON public.tickets_auditoriaticketestado USING btree (usuario_responsable_id);


--
-- Name: tickets_estadoticket_id_5f7b5f8d_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_estadoticket_id_5f7b5f8d_like ON public.tickets_estadoticket USING btree (id varchar_pattern_ops);


--
-- Name: tickets_observacion_ticket_id_8a25dc87; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_observacion_ticket_id_8a25dc87 ON public.tickets_observacion USING btree (ticket_id);


--
-- Name: tickets_ticket_estado_id_de9f88a5; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_estado_id_de9f88a5 ON public.tickets_ticket USING btree (estado_id);


--
-- Name: tickets_ticket_estado_id_de9f88a5_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_estado_id_de9f88a5_like ON public.tickets_ticket USING btree (estado_id varchar_pattern_ops);


--
-- Name: tickets_ticket_responsable_id_2e8d6597; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_responsable_id_2e8d6597 ON public.tickets_ticket USING btree (responsable_id);


--
-- Name: tickets_ticket_solicitante_id_f155a8aa; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_solicitante_id_f155a8aa ON public.tickets_ticket USING btree (solicitante_id);


--
-- Name: tickets_ticket_urgencia_id_857ef8d2; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_urgencia_id_857ef8d2 ON public.tickets_ticket USING btree (urgencia_id);


--
-- Name: tickets_ticket_urgencia_id_857ef8d2_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_urgencia_id_857ef8d2_like ON public.tickets_ticket USING btree (urgencia_id varchar_pattern_ops);


--
-- Name: tickets_tipourgencia_id_ac202dac_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_tipourgencia_id_ac202dac_like ON public.tickets_tipourgencia USING btree (id varchar_pattern_ops);


--
-- Name: admin2_userprofile admin2_userprofile_usuario_id_edf8a88d_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.admin2_userprofile
    ADD CONSTRAINT admin2_userprofile_usuario_id_edf8a88d_fk_auth_user_id FOREIGN KEY (usuario_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_abono campeonato_abono_equipo_id_3d541ddf_fk_campeonato_equipo_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_abono
    ADD CONSTRAINT campeonato_abono_equipo_id_3d541ddf_fk_campeonato_equipo_id FOREIGN KEY (equipo_id) REFERENCES public.campeonato_equipo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_abono campeonato_abono_evento_id_fbc7b13f_fk_evento_evento_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_abono
    ADD CONSTRAINT campeonato_abono_evento_id_fbc7b13f_fk_evento_evento_id FOREIGN KEY (evento_id) REFERENCES public.evento_evento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_equipo campeonato_equipo_categoria_id_56de5214_fk_evento_categoria_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_equipo
    ADD CONSTRAINT campeonato_equipo_categoria_id_56de5214_fk_evento_categoria_id FOREIGN KEY (categoria_id) REFERENCES public.evento_categoria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_equipo campeonato_equipo_creado_por_id_090084b1_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_equipo
    ADD CONSTRAINT campeonato_equipo_creado_por_id_090084b1_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_equipo campeonato_equipo_departamento_id_a19e183a_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_equipo
    ADD CONSTRAINT campeonato_equipo_departamento_id_a19e183a_fk_contactos FOREIGN KEY (departamento_id) REFERENCES public.contactos_departamento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_equipo campeonato_equipo_evento_id_62eb3066_fk_evento_evento_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_equipo
    ADD CONSTRAINT campeonato_equipo_evento_id_62eb3066_fk_evento_evento_id FOREIGN KEY (evento_id) REFERENCES public.evento_evento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_equipo campeonato_equipo_municipio_id_42cdbee1_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_equipo
    ADD CONSTRAINT campeonato_equipo_municipio_id_42cdbee1_fk_contactos FOREIGN KEY (municipio_id) REFERENCES public.contactos_municipio(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_grupo campeonato_grupo_categoria_id_6528544c_fk_evento_categoria_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo
    ADD CONSTRAINT campeonato_grupo_categoria_id_6528544c_fk_evento_categoria_id FOREIGN KEY (categoria_id) REFERENCES public.evento_categoria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_grupo_equipos campeonato_grupo_equ_equipo_id_a312ede0_fk_campeonat; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo_equipos
    ADD CONSTRAINT campeonato_grupo_equ_equipo_id_a312ede0_fk_campeonat FOREIGN KEY (equipo_id) REFERENCES public.campeonato_equipo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_grupo_equipos_clasificados campeonato_grupo_equ_equipo_id_d1f8ef08_fk_campeonat; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo_equipos_clasificados
    ADD CONSTRAINT campeonato_grupo_equ_equipo_id_d1f8ef08_fk_campeonat FOREIGN KEY (equipo_id) REFERENCES public.campeonato_equipo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_grupo_equipos campeonato_grupo_equ_grupo_id_865b4be8_fk_campeonat; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo_equipos
    ADD CONSTRAINT campeonato_grupo_equ_grupo_id_865b4be8_fk_campeonat FOREIGN KEY (grupo_id) REFERENCES public.campeonato_grupo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_grupo_equipos_clasificados campeonato_grupo_equ_grupo_id_e7e32de9_fk_campeonat; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo_equipos_clasificados
    ADD CONSTRAINT campeonato_grupo_equ_grupo_id_e7e32de9_fk_campeonat FOREIGN KEY (grupo_id) REFERENCES public.campeonato_grupo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_grupo campeonato_grupo_evento_id_9965646f_fk_evento_evento_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_grupo
    ADD CONSTRAINT campeonato_grupo_evento_id_9965646f_fk_evento_evento_id FOREIGN KEY (evento_id) REFERENCES public.evento_evento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_jugadorespartido campeonato_jugadores_evento_id_cbe5122e_fk_evento_ev; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_jugadorespartido
    ADD CONSTRAINT campeonato_jugadores_evento_id_cbe5122e_fk_evento_ev FOREIGN KEY (evento_id) REFERENCES public.evento_evento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_jugadorespartido campeonato_jugadores_jugador_id_5437b31a_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_jugadorespartido
    ADD CONSTRAINT campeonato_jugadores_jugador_id_5437b31a_fk_contactos FOREIGN KEY (jugador_id) REFERENCES public.contactos_contacto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_partido campeonato_partido_categoria_id_ce249ab9_fk_evento_categoria_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido
    ADD CONSTRAINT campeonato_partido_categoria_id_ce249ab9_fk_evento_categoria_id FOREIGN KEY (categoria_id) REFERENCES public.evento_categoria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_partido campeonato_partido_evento_id_cef937e5_fk_evento_evento_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido
    ADD CONSTRAINT campeonato_partido_evento_id_cef937e5_fk_evento_evento_id FOREIGN KEY (evento_id) REFERENCES public.evento_evento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_partido campeonato_partido_ganador_id_1826eb81_fk_campeonato_equipo_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido
    ADD CONSTRAINT campeonato_partido_ganador_id_1826eb81_fk_campeonato_equipo_id FOREIGN KEY (ganador_id) REFERENCES public.campeonato_equipo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_partido campeonato_partido_grupo_id_a1e54728_fk_campeonato_grupo_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido
    ADD CONSTRAINT campeonato_partido_grupo_id_a1e54728_fk_campeonato_grupo_id FOREIGN KEY (grupo_id) REFERENCES public.campeonato_grupo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_partido_jugadores campeonato_partido_j_jugadorespartido_id_9cc79825_fk_campeonat; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido_jugadores
    ADD CONSTRAINT campeonato_partido_j_jugadorespartido_id_9cc79825_fk_campeonat FOREIGN KEY (jugadorespartido_id) REFERENCES public.campeonato_jugadorespartido(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_partido_jugadores campeonato_partido_j_partido_id_170226c6_fk_campeonat; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido_jugadores
    ADD CONSTRAINT campeonato_partido_j_partido_id_170226c6_fk_campeonat FOREIGN KEY (partido_id) REFERENCES public.campeonato_partido(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_partido campeonato_partido_local_id_c001a3c3_fk_campeonato_equipo_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido
    ADD CONSTRAINT campeonato_partido_local_id_c001a3c3_fk_campeonato_equipo_id FOREIGN KEY (local_id) REFERENCES public.campeonato_equipo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: campeonato_partido campeonato_partido_visitante_id_349ccacd_fk_campeonat; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.campeonato_partido
    ADD CONSTRAINT campeonato_partido_visitante_id_349ccacd_fk_campeonat FOREIGN KEY (visitante_id) REFERENCES public.campeonato_equipo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_creado_por_id_21b74f22_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_creado_por_id_21b74f22_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_equipo_id_d14f9901_fk_campeonato_equipo_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_equipo_id_d14f9901_fk_campeonato_equipo_id FOREIGN KEY (equipo_id) REFERENCES public.campeonato_equipo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_estado_id_bfa0a26f_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_estado_id_bfa0a26f_fk_contactos FOREIGN KEY (estado_id) REFERENCES public.contactos_paisestado(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_grupo_id_dc60af8d_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_grupo_id_dc60af8d_fk_auth_group_id FOREIGN KEY (grupo_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto_industria contactos_contacto_i_contacto_id_91b84802_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto_industria
    ADD CONSTRAINT contactos_contacto_i_contacto_id_91b84802_fk_contactos FOREIGN KEY (contacto_id) REFERENCES public.contactos_contacto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto_industria contactos_contacto_i_industria_id_60f8c341_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto_industria
    ADD CONSTRAINT contactos_contacto_i_industria_id_60f8c341_fk_contactos FOREIGN KEY (industria_id) REFERENCES public.contactos_industria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_modificado_por_id_a362c749_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_modificado_por_id_a362c749_fk_auth_user_id FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_pais_id_5d8ab74e_fk_contactos_pais_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_pais_id_5d8ab74e_fk_contactos_pais_id FOREIGN KEY (pais_id) REFERENCES public.contactos_pais(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_tipo_identificacion__c657fc86_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_tipo_identificacion__c657fc86_fk_contactos FOREIGN KEY (tipo_identificacion_id) REFERENCES public.contactos_tipoidentificacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_usuario_id_b821c02b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_usuario_id_b821c02b_fk_auth_user_id FOREIGN KEY (usuario_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contactorelacion contactos_contactore_contacto_id_1e98bf9b_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactore_contacto_id_1e98bf9b_fk_contactos FOREIGN KEY (contacto_id) REFERENCES public.contactos_contacto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contactorelacion contactos_contactore_contacto_relacionado_7f14d590_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactore_contacto_relacionado_7f14d590_fk_contactos FOREIGN KEY (contacto_relacionado_id) REFERENCES public.contactos_contacto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contactorelacion contactos_contactore_creado_por_id_8f9b80d8_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactore_creado_por_id_8f9b80d8_fk_auth_user FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contactorelacion contactos_contactore_modificado_por_id_8075cd68_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactore_modificado_por_id_8075cd68_fk_auth_user FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contactorelacion contactos_contactore_relacion_id_480832f8_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactore_relacion_id_480832f8_fk_contactos FOREIGN KEY (relacion_id) REFERENCES public.contactos_relacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_industria contactos_industria_creado_por_id_66da5cb1_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_industria
    ADD CONSTRAINT contactos_industria_creado_por_id_66da5cb1_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_industria contactos_industria_modificado_por_id_d143c13e_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_industria
    ADD CONSTRAINT contactos_industria_modificado_por_id_d143c13e_fk_auth_user_id FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_municipio contactos_municipio_departamento_id_d40485c5_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_municipio
    ADD CONSTRAINT contactos_municipio_departamento_id_d40485c5_fk_contactos FOREIGN KEY (departamento_id) REFERENCES public.contactos_departamento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_pais contactos_pais_creado_por_id_ceac3c22_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais
    ADD CONSTRAINT contactos_pais_creado_por_id_ceac3c22_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_pais contactos_pais_modificado_por_id_d3b6c179_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais
    ADD CONSTRAINT contactos_pais_modificado_por_id_d3b6c179_fk_auth_user_id FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_paisestado contactos_paisestado_creado_por_id_168088dd_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado
    ADD CONSTRAINT contactos_paisestado_creado_por_id_168088dd_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_paisestado contactos_paisestado_modificado_por_id_f0c0bfa1_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado
    ADD CONSTRAINT contactos_paisestado_modificado_por_id_f0c0bfa1_fk_auth_user_id FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_paisestado contactos_paisestado_pais_id_910f30ba_fk_contactos_pais_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado
    ADD CONSTRAINT contactos_paisestado_pais_id_910f30ba_fk_contactos_pais_id FOREIGN KEY (pais_id) REFERENCES public.contactos_pais(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_relacion contactos_relacion_creado_por_id_311687a5_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_relacion
    ADD CONSTRAINT contactos_relacion_creado_por_id_311687a5_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_relacion contactos_relacion_modificado_por_id_094367ca_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_relacion
    ADD CONSTRAINT contactos_relacion_modificado_por_id_094367ca_fk_auth_user_id FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_categoria evento_categoria_evento_id_92abf84f_fk_evento_evento_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_categoria
    ADD CONSTRAINT evento_categoria_evento_id_92abf84f_fk_evento_evento_id FOREIGN KEY (evento_id) REFERENCES public.evento_evento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_disciplina evento_disciplina_creado_por_id_356563d4_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_disciplina
    ADD CONSTRAINT evento_disciplina_creado_por_id_356563d4_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_disciplina_subdisciplinas evento_disciplina_su_disciplina_id_b2b6add4_fk_evento_di; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_disciplina_subdisciplinas
    ADD CONSTRAINT evento_disciplina_su_disciplina_id_b2b6add4_fk_evento_di FOREIGN KEY (disciplina_id) REFERENCES public.evento_disciplina(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_disciplina_subdisciplinas evento_disciplina_su_subdisciplina_id_5b406b8f_fk_evento_su; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_disciplina_subdisciplinas
    ADD CONSTRAINT evento_disciplina_su_subdisciplina_id_5b406b8f_fk_evento_su FOREIGN KEY (subdisciplina_id) REFERENCES public.evento_subdisciplina(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_escenario evento_escenario_creado_por_id_5bf0faa4_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_escenario
    ADD CONSTRAINT evento_escenario_creado_por_id_5bf0faa4_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_escenario evento_escenario_evento_id_b50b0533_fk_evento_evento_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_escenario
    ADD CONSTRAINT evento_escenario_evento_id_b50b0533_fk_evento_evento_id FOREIGN KEY (evento_id) REFERENCES public.evento_evento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_evento evento_evento_creado_por_id_1585026a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_evento
    ADD CONSTRAINT evento_evento_creado_por_id_1585026a_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_evento evento_evento_disciplina_id_80e92d31_fk_evento_disciplina_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_evento
    ADD CONSTRAINT evento_evento_disciplina_id_80e92d31_fk_evento_disciplina_id FOREIGN KEY (disciplina_id) REFERENCES public.evento_disciplina(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_evento evento_evento_subdisciplina_id_6679e423_fk_evento_su; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_evento
    ADD CONSTRAINT evento_evento_subdisciplina_id_6679e423_fk_evento_su FOREIGN KEY (subdisciplina_id) REFERENCES public.evento_subdisciplina(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_rama evento_rama_creado_por_id_7a528549_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_rama
    ADD CONSTRAINT evento_rama_creado_por_id_7a528549_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_subdisciplina evento_subdisciplina_creado_por_id_d7868e41_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.evento_subdisciplina
    ADD CONSTRAINT evento_subdisciplina_creado_por_id_d7868e41_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_auditoriaticketestado tickets_auditoriatic_estado_id_21f33d24_fk_tickets_e; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado
    ADD CONSTRAINT tickets_auditoriatic_estado_id_21f33d24_fk_tickets_e FOREIGN KEY (estado_id) REFERENCES public.tickets_estadoticket(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_auditoriaticketestado tickets_auditoriatic_ticket_id_eae8c426_fk_tickets_t; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado
    ADD CONSTRAINT tickets_auditoriatic_ticket_id_eae8c426_fk_tickets_t FOREIGN KEY (ticket_id) REFERENCES public.tickets_ticket(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_auditoriaticketestado tickets_auditoriatic_usuario_id_ee0dec32_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado
    ADD CONSTRAINT tickets_auditoriatic_usuario_id_ee0dec32_fk_auth_user FOREIGN KEY (usuario_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_auditoriaticketestado tickets_auditoriatic_usuario_responsable__401ab195_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado
    ADD CONSTRAINT tickets_auditoriatic_usuario_responsable__401ab195_fk_auth_user FOREIGN KEY (usuario_responsable_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_observacion tickets_observacion_ticket_id_8a25dc87_fk_tickets_ticket_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_observacion
    ADD CONSTRAINT tickets_observacion_ticket_id_8a25dc87_fk_tickets_ticket_id FOREIGN KEY (ticket_id) REFERENCES public.tickets_ticket(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_ticket tickets_ticket_estado_id_de9f88a5_fk_tickets_estadoticket_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket
    ADD CONSTRAINT tickets_ticket_estado_id_de9f88a5_fk_tickets_estadoticket_id FOREIGN KEY (estado_id) REFERENCES public.tickets_estadoticket(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_ticket tickets_ticket_responsable_id_2e8d6597_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket
    ADD CONSTRAINT tickets_ticket_responsable_id_2e8d6597_fk_auth_user_id FOREIGN KEY (responsable_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_ticket tickets_ticket_solicitante_id_f155a8aa_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket
    ADD CONSTRAINT tickets_ticket_solicitante_id_f155a8aa_fk_auth_user_id FOREIGN KEY (solicitante_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_ticket tickets_ticket_urgencia_id_857ef8d2_fk_tickets_tipourgencia_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket
    ADD CONSTRAINT tickets_ticket_urgencia_id_857ef8d2_fk_tickets_tipourgencia_id FOREIGN KEY (urgencia_id) REFERENCES public.tickets_tipourgencia(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_userprofile tickets_userprofile_user_id_75909dd7_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_userprofile
    ADD CONSTRAINT tickets_userprofile_user_id_75909dd7_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

