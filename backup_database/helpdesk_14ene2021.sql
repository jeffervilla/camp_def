--
-- PostgreSQL database dump
--

-- Dumped from database version 10.15 (Ubuntu 10.15-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.15 (Ubuntu 10.15-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin2_userprofile; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.admin2_userprofile (
    id integer NOT NULL,
    phone character varying(31) NOT NULL,
    usuario_id integer NOT NULL
);


ALTER TABLE public.admin2_userprofile OWNER TO grupoda2;

--
-- Name: admin2_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.admin2_userprofile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin2_userprofile_id_seq OWNER TO grupoda2;

--
-- Name: admin2_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.admin2_userprofile_id_seq OWNED BY public.admin2_userprofile.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO grupoda2;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO grupoda2;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO grupoda2;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO grupoda2;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO grupoda2;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO grupoda2;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO grupoda2;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO grupoda2;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO grupoda2;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO grupoda2;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO grupoda2;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO grupoda2;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: contactos_contacto; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_contacto (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    imagen character varying(100),
    documento character varying(512),
    sexo character varying(16) NOT NULL,
    cargo character varying(254),
    idioma character varying(512),
    zona_horaria character varying(512),
    sitioweb character varying(512),
    direccion character varying(512),
    activo boolean NOT NULL,
    cliente boolean NOT NULL,
    proveedor boolean NOT NULL,
    empleado boolean NOT NULL,
    codigo_postal character varying(512),
    ciudad character varying(512),
    email character varying(512),
    telefono character varying(512),
    es_compania boolean NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    token_logueo character varying(512),
    tipo_logueo character varying(512),
    vencimiento_logueo timestamp with time zone,
    observaciones text,
    creado_por_id integer,
    estado_id integer,
    modificado_por_id integer,
    pais_id integer,
    usuario_id integer
);


ALTER TABLE public.contactos_contacto OWNER TO grupoda2;

--
-- Name: contactos_contacto_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_contacto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_contacto_id_seq OWNER TO grupoda2;

--
-- Name: contactos_contacto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_contacto_id_seq OWNED BY public.contactos_contacto.id;


--
-- Name: contactos_contacto_industria; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_contacto_industria (
    id integer NOT NULL,
    contacto_id integer NOT NULL,
    industria_id integer NOT NULL
);


ALTER TABLE public.contactos_contacto_industria OWNER TO grupoda2;

--
-- Name: contactos_contacto_industria_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_contacto_industria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_contacto_industria_id_seq OWNER TO grupoda2;

--
-- Name: contactos_contacto_industria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_contacto_industria_id_seq OWNED BY public.contactos_contacto_industria.id;


--
-- Name: contactos_contactorelacion; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_contactorelacion (
    id integer NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    contacto_id integer NOT NULL,
    contacto_relacionado_id integer NOT NULL,
    creado_por_id integer,
    modificado_por_id integer,
    relacion_id integer NOT NULL
);


ALTER TABLE public.contactos_contactorelacion OWNER TO grupoda2;

--
-- Name: contactos_contactorelacion_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_contactorelacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_contactorelacion_id_seq OWNER TO grupoda2;

--
-- Name: contactos_contactorelacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_contactorelacion_id_seq OWNED BY public.contactos_contactorelacion.id;


--
-- Name: contactos_industria; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_industria (
    id integer NOT NULL,
    nombre character varying(512),
    activo boolean NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    creado_por_id integer,
    modificado_por_id integer
);


ALTER TABLE public.contactos_industria OWNER TO grupoda2;

--
-- Name: contactos_industria_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_industria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_industria_id_seq OWNER TO grupoda2;

--
-- Name: contactos_industria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_industria_id_seq OWNED BY public.contactos_industria.id;


--
-- Name: contactos_pais; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_pais (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    codigo character varying(2),
    formato_direccion text,
    codigo_telefono integer,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    creado_por_id integer,
    modificado_por_id integer
);


ALTER TABLE public.contactos_pais OWNER TO grupoda2;

--
-- Name: contactos_pais_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_pais_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_pais_id_seq OWNER TO grupoda2;

--
-- Name: contactos_pais_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_pais_id_seq OWNED BY public.contactos_pais.id;


--
-- Name: contactos_paisestado; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_paisestado (
    id integer NOT NULL,
    nombre character varying(512) NOT NULL,
    codigo character varying(128) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    creado_por_id integer,
    modificado_por_id integer,
    pais_id integer NOT NULL
);


ALTER TABLE public.contactos_paisestado OWNER TO grupoda2;

--
-- Name: contactos_paisestado_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_paisestado_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_paisestado_id_seq OWNER TO grupoda2;

--
-- Name: contactos_paisestado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_paisestado_id_seq OWNED BY public.contactos_paisestado.id;


--
-- Name: contactos_relacion; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.contactos_relacion (
    id integer NOT NULL,
    nombre character varying(512),
    activo boolean NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    modificado_fecha timestamp with time zone,
    creado_por_id integer,
    modificado_por_id integer
);


ALTER TABLE public.contactos_relacion OWNER TO grupoda2;

--
-- Name: contactos_relacion_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.contactos_relacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactos_relacion_id_seq OWNER TO grupoda2;

--
-- Name: contactos_relacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.contactos_relacion_id_seq OWNED BY public.contactos_relacion.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO grupoda2;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO grupoda2;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO grupoda2;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO grupoda2;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO grupoda2;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO grupoda2;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO grupoda2;

--
-- Name: tickets_auditoriaticketestado; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_auditoriaticketestado (
    id integer NOT NULL,
    observaciones character varying(1024) NOT NULL,
    f_registro timestamp with time zone NOT NULL,
    estado_id character varying(16) NOT NULL,
    ticket_id integer NOT NULL,
    usuario_id integer,
    usuario_responsable_id integer
);


ALTER TABLE public.tickets_auditoriaticketestado OWNER TO grupoda2;

--
-- Name: tickets_auditoriaticketestado_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.tickets_auditoriaticketestado_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tickets_auditoriaticketestado_id_seq OWNER TO grupoda2;

--
-- Name: tickets_auditoriaticketestado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.tickets_auditoriaticketestado_id_seq OWNED BY public.tickets_auditoriaticketestado.id;


--
-- Name: tickets_estadoticket; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_estadoticket (
    id character varying(16) NOT NULL,
    nombre character varying(32) NOT NULL,
    color character varying(32) NOT NULL,
    descripcion character varying(256) NOT NULL
);


ALTER TABLE public.tickets_estadoticket OWNER TO grupoda2;

--
-- Name: tickets_observacion; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_observacion (
    id integer NOT NULL,
    registro character varying(1024) NOT NULL,
    f_registro timestamp with time zone NOT NULL,
    ticket_id integer NOT NULL
);


ALTER TABLE public.tickets_observacion OWNER TO grupoda2;

--
-- Name: tickets_observacion_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.tickets_observacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tickets_observacion_id_seq OWNER TO grupoda2;

--
-- Name: tickets_observacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.tickets_observacion_id_seq OWNED BY public.tickets_observacion.id;


--
-- Name: tickets_ticket; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_ticket (
    id integer NOT NULL,
    nombre character varying(64) NOT NULL,
    descripcion text NOT NULL,
    notas text NOT NULL,
    solucion text NOT NULL,
    retroalimentacion text NOT NULL,
    f_inicio timestamp with time zone,
    f_fin timestamp with time zone,
    f_registro timestamp with time zone NOT NULL,
    estado_id character varying(16),
    responsable_id integer,
    solicitante_id integer,
    urgencia_id character varying(16) NOT NULL,
    evaluacion_cliente bigint
);


ALTER TABLE public.tickets_ticket OWNER TO grupoda2;

--
-- Name: tickets_ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: grupoda2
--

CREATE SEQUENCE public.tickets_ticket_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tickets_ticket_id_seq OWNER TO grupoda2;

--
-- Name: tickets_ticket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grupoda2
--

ALTER SEQUENCE public.tickets_ticket_id_seq OWNED BY public.tickets_ticket.id;


--
-- Name: tickets_tipourgencia; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_tipourgencia (
    id character varying(16) NOT NULL,
    nombre character varying(32) NOT NULL,
    tiempo_solucion integer NOT NULL,
    descripcion character varying(256) NOT NULL
);


ALTER TABLE public.tickets_tipourgencia OWNER TO grupoda2;

--
-- Name: tickets_userprofile; Type: TABLE; Schema: public; Owner: grupoda2
--

CREATE TABLE public.tickets_userprofile (
    user_id integer NOT NULL,
    telefono character varying(64) NOT NULL
);


ALTER TABLE public.tickets_userprofile OWNER TO grupoda2;

--
-- Name: admin2_userprofile id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.admin2_userprofile ALTER COLUMN id SET DEFAULT nextval('public.admin2_userprofile_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: contactos_contacto id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto ALTER COLUMN id SET DEFAULT nextval('public.contactos_contacto_id_seq'::regclass);


--
-- Name: contactos_contacto_industria id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto_industria ALTER COLUMN id SET DEFAULT nextval('public.contactos_contacto_industria_id_seq'::regclass);


--
-- Name: contactos_contactorelacion id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion ALTER COLUMN id SET DEFAULT nextval('public.contactos_contactorelacion_id_seq'::regclass);


--
-- Name: contactos_industria id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_industria ALTER COLUMN id SET DEFAULT nextval('public.contactos_industria_id_seq'::regclass);


--
-- Name: contactos_pais id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais ALTER COLUMN id SET DEFAULT nextval('public.contactos_pais_id_seq'::regclass);


--
-- Name: contactos_paisestado id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado ALTER COLUMN id SET DEFAULT nextval('public.contactos_paisestado_id_seq'::regclass);


--
-- Name: contactos_relacion id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_relacion ALTER COLUMN id SET DEFAULT nextval('public.contactos_relacion_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: tickets_auditoriaticketestado id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado ALTER COLUMN id SET DEFAULT nextval('public.tickets_auditoriaticketestado_id_seq'::regclass);


--
-- Name: tickets_observacion id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_observacion ALTER COLUMN id SET DEFAULT nextval('public.tickets_observacion_id_seq'::regclass);


--
-- Name: tickets_ticket id; Type: DEFAULT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket ALTER COLUMN id SET DEFAULT nextval('public.tickets_ticket_id_seq'::regclass);


--
-- Data for Name: admin2_userprofile; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.admin2_userprofile (id, phone, usuario_id) FROM stdin;
1		5
2		6
3		7
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_group (id, name) FROM stdin;
1	Administradores
3	Tecnicos
4	Usuarios
2	Supervisor
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	2	66
2	2	67
3	3	65
4	3	68
5	4	65
6	4	66
8	2	68
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add user-profile	7	add_userprofile
26	Can change user-profile	7	change_userprofile
27	Can delete user-profile	7	delete_userprofile
28	Can view user-profile	7	view_userprofile
29	Can add Contacto	8	add_contacto
30	Can change Contacto	8	change_contacto
31	Can delete Contacto	8	delete_contacto
32	Can view Contacto	8	view_contacto
33	Can add Pais	9	add_pais
34	Can change Pais	9	change_pais
35	Can delete Pais	9	delete_pais
36	Can view Pais	9	view_pais
37	Can add Relacion	10	add_relacion
38	Can change Relacion	10	change_relacion
39	Can delete Relacion	10	delete_relacion
40	Can view Relacion	10	view_relacion
41	Can add Estado	11	add_paisestado
42	Can change Estado	11	change_paisestado
43	Can delete Estado	11	delete_paisestado
44	Can view Estado	11	view_paisestado
45	Can add Industria	12	add_industria
46	Can change Industria	12	change_industria
47	Can delete Industria	12	delete_industria
48	Can view Industria	12	view_industria
49	Can add relacion-contacto	13	add_contactorelacion
50	Can change relacion-contacto	13	change_contactorelacion
51	Can delete relacion-contacto	13	delete_contactorelacion
52	Can view relacion-contacto	13	view_contactorelacion
53	Can add estado - caso	14	add_estadoticket
54	Can change estado - caso	14	change_estadoticket
55	Can delete estado - caso	14	delete_estadoticket
56	Can view estado - caso	14	view_estadoticket
57	Can add tipo - urgencia	15	add_tipourgencia
58	Can change tipo - urgencia	15	change_tipourgencia
59	Can delete tipo - urgencia	15	delete_tipourgencia
60	Can view tipo - urgencia	15	view_tipourgencia
61	Can add user profile	16	add_userprofile
62	Can change user profile	16	change_userprofile
63	Can delete user profile	16	delete_userprofile
64	Can view user profile	16	view_userprofile
65	Can add Ticket	17	add_ticket
66	Can change Ticket	17	change_ticket
67	Can delete Ticket	17	delete_ticket
68	Can view Ticket	17	view_ticket
69	Can add ObservacionTicket	18	add_observacion
70	Can change ObservacionTicket	18	change_observacion
71	Can delete ObservacionTicket	18	delete_observacion
72	Can view ObservacionTicket	18	view_observacion
73	Can add AuditoriaTicketEstado	19	add_auditoriaticketestado
74	Can change AuditoriaTicketEstado	19	change_auditoriaticketestado
75	Can delete AuditoriaTicketEstado	19	delete_auditoriaticketestado
76	Can view AuditoriaTicketEstado	19	view_auditoriaticketestado
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
5	pbkdf2_sha256$150000$RZ1rceY8JzMR$iXV+V1QvbnpIy3dXrcVZ6ZI90lhviZYfmZNZGM4M5oI=	\N	f	maito.diaz	Maito Felino	Garcia Diaz		f	t	2020-04-15 16:53:30.525488-05
2	pbkdf2_sha256$150000$8btxE5AdAWuU$cSwXUyek/f9/u9uBrh3e+4OokLPOki7W3ZEnPlXGUE4=	2020-04-15 17:16:30.186972-05	f	usuario	Javier	amaya		f	t	2020-02-10 11:26:49-05
3	pbkdf2_sha256$150000$yZrmn8XaS6oq$uz+5uzQIamsDyxgnWIf38DiIrter169/tTrL1oW9XZw=	2021-01-14 14:57:40.317944-05	f	tecnico	William	Moreno		f	t	2020-02-10 11:27:43-05
1	pbkdf2_sha256$150000$6ay1ZRZYxpNp$twYVYGzsFcvZ2xmNsDgekj8KHahMxx6b2nSj1KigkiI=	2021-01-14 16:40:15.971369-05	t	administrador				t	t	2020-02-10 11:21:56-05
6	pbkdf2_sha256$150000$WeSnyBGFP9N7$EvjrcLucqXtY17qfsi+tgQSpJAtZX3ZF9rsMak83Nro=	2021-01-14 16:41:59.369884-05	f	emanuel	Emanuel	Montealegre		f	t	2021-01-14 16:41:20.89756-05
7	pbkdf2_sha256$150000$q0ZR9TKiZTix$eI+YAoHKpS2LuC+587jmR09G6qkA0YCXjn3KF0Zuh5k=	2021-01-14 16:46:47.299808-05	f	ardubert	Ardubert S.	Alvarez		f	t	2021-01-14 16:45:26.094597-05
4	pbkdf2_sha256$150000$u5CXzyWX6zfA$LKM8CoMx+oGyz0y/sFlidH1sUxDJRvUDyQ1FUgv18KM=	2021-01-14 16:49:37.837828-05	f	supervisor	Maritza Yicel	Diaz Pulido		f	t	2020-02-10 11:28:22-05
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
1	2	4
2	3	3
3	4	2
4	5	3
5	1	1
6	6	4
7	7	3
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: contactos_contacto; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_contacto (id, nombre, imagen, documento, sexo, cargo, idioma, zona_horaria, sitioweb, direccion, activo, cliente, proveedor, empleado, codigo_postal, ciudad, email, telefono, es_compania, fecha_creacion, modificado_fecha, token_logueo, tipo_logueo, vencimiento_logueo, observaciones, creado_por_id, estado_id, modificado_por_id, pais_id, usuario_id) FROM stdin;
\.


--
-- Data for Name: contactos_contacto_industria; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_contacto_industria (id, contacto_id, industria_id) FROM stdin;
\.


--
-- Data for Name: contactos_contactorelacion; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_contactorelacion (id, fecha_creacion, modificado_fecha, contacto_id, contacto_relacionado_id, creado_por_id, modificado_por_id, relacion_id) FROM stdin;
\.


--
-- Data for Name: contactos_industria; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_industria (id, nombre, activo, fecha_creacion, modificado_fecha, creado_por_id, modificado_por_id) FROM stdin;
\.


--
-- Data for Name: contactos_pais; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_pais (id, nombre, codigo, formato_direccion, codigo_telefono, fecha_creacion, modificado_fecha, creado_por_id, modificado_por_id) FROM stdin;
\.


--
-- Data for Name: contactos_paisestado; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_paisestado (id, nombre, codigo, fecha_creacion, modificado_fecha, creado_por_id, modificado_por_id, pais_id) FROM stdin;
\.


--
-- Data for Name: contactos_relacion; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.contactos_relacion (id, nombre, activo, fecha_creacion, modificado_fecha, creado_por_id, modificado_por_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2020-02-10 11:26:49.973667-05	2	usuario	1	[{"added": {}}]	4	1
2	2020-02-10 11:27:21.823726-05	2	usuario	2	[{"changed": {"fields": ["first_name", "last_name", "groups"]}}]	4	1
3	2020-02-10 11:27:43.781046-05	3	tecnico	1	[{"added": {}}]	4	1
4	2020-02-10 11:28:04.772302-05	3	tecnico	2	[{"changed": {"fields": ["first_name", "last_name", "groups"]}}]	4	1
5	2020-02-10 11:28:22.964589-05	4	supervisor	1	[{"added": {}}]	4	1
6	2020-02-10 11:28:31.581118-05	4	supervisor	2	[{"changed": {"fields": ["groups"]}}]	4	1
7	2020-02-11 08:36:55.239466-05	4	Usuarios	2	[{"changed": {"fields": ["permissions"]}}]	3	1
8	2020-02-11 08:37:14.138155-05	4	Usuarios	2	[{"changed": {"fields": ["permissions"]}}]	3	1
9	2020-02-11 08:37:26.544448-05	2	Supervisor	2	[{"changed": {"fields": ["permissions"]}}]	3	1
10	2020-04-15 17:52:49.484177-05	1	administrador	2	[{"changed": {"fields": ["groups"]}}]	4	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	admin2	userprofile
8	contactos	contacto
9	contactos	pais
10	contactos	relacion
11	contactos	paisestado
12	contactos	industria
13	contactos	contactorelacion
14	tickets	estadoticket
15	tickets	tipourgencia
16	tickets	userprofile
17	tickets	ticket
18	tickets	observacion
19	tickets	auditoriaticketestado
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2020-02-10 11:21:22.362079-05
2	auth	0001_initial	2020-02-10 11:21:22.681987-05
3	admin	0001_initial	2020-02-10 11:21:23.19891-05
4	admin	0002_logentry_remove_auto_add	2020-02-10 11:21:23.30627-05
5	admin	0003_logentry_add_action_flag_choices	2020-02-10 11:21:23.332282-05
6	admin2	0001_initial	2020-02-10 11:21:23.434753-05
7	admin2	0002_auto_20200119_1243	2020-02-10 11:21:23.512553-05
8	admin2	0003_auto_20200130_1154	2020-02-10 11:21:23.56798-05
9	contenttypes	0002_remove_content_type_name	2020-02-10 11:21:23.605413-05
10	auth	0002_alter_permission_name_max_length	2020-02-10 11:21:23.617397-05
11	auth	0003_alter_user_email_max_length	2020-02-10 11:21:23.644654-05
12	auth	0004_alter_user_username_opts	2020-02-10 11:21:23.666609-05
13	auth	0005_alter_user_last_login_null	2020-02-10 11:21:23.695124-05
14	auth	0006_require_contenttypes_0002	2020-02-10 11:21:23.704552-05
15	auth	0007_alter_validators_add_error_messages	2020-02-10 11:21:23.727817-05
16	auth	0008_alter_user_username_max_length	2020-02-10 11:21:23.785763-05
17	auth	0009_alter_user_last_name_max_length	2020-02-10 11:21:23.82535-05
18	auth	0010_alter_group_name_max_length	2020-02-10 11:21:23.849328-05
19	auth	0011_update_proxy_permissions	2020-02-10 11:21:23.87738-05
20	contactos	0001_initial	2020-02-10 11:21:24.578135-05
21	contactos	0002_remove_contacto_direccion2	2020-02-10 11:21:25.537251-05
22	contactos	0003_auto_20200129_1108	2020-02-10 11:21:25.58184-05
23	contactos	0004_auto_20200129_1110	2020-02-10 11:21:25.700098-05
24	contactos	0005_auto_20200131_1327	2020-02-10 11:21:25.871772-05
25	contactos	0006_auto_20200131_1642	2020-02-10 11:21:25.90771-05
26	contactos	0007_auto_20200131_1730	2020-02-10 11:21:26.082778-05
27	contactos	0008_auto_20200201_1114	2020-02-10 11:21:26.179829-05
28	contactos	0009_auto_20200201_1325	2020-02-10 11:21:26.354273-05
29	sessions	0001_initial	2020-02-10 11:21:26.432771-05
30	tickets	0001_initial	2020-02-10 11:21:26.929408-05
31	tickets	0002_observacioncliente	2020-02-10 11:21:27.519943-05
32	tickets	0003_delete_observacioncliente	2020-02-10 11:21:27.579361-05
33	tickets	0004_ticket_evaluacion_cliente	2020-02-10 11:21:27.615165-05
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
at4igywvzvz9r59tmin4yqurh03jr4hd	Yjk2NWU3OWZiNGMzMDcyNzA3ZTYyNTc1OGUwZDg2OGUwOWI0MWU5Yzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJjOTZjNzdmMDFiNjcyYWJmNTBkZWI5MTc0NGFlYmE0Y2ZjZjczN2ZkIn0=	2020-02-25 08:22:51.946215-05
4vnl2r5dqod53af9owxhdrulm39j5e7r	YzkyZTExMmMwZGU0MjhjYWI0OGIyNDY2MGUxYjNhZWUzMzE3NWVkMjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NWQ3NjJlMDUzYTM1NGExMDIzMTZlZmE1NzBmYTdiOGMwM2VkNjQ0In0=	2020-02-25 08:26:47.328553-05
ya3c5a52buv6oe2rfkzb5r72tykb286h	N2Y1NDBkM2RjMTRjNzU0Y2JjMzE0NjBjZjgzYzY1YTc2ZjJjNTU1NTp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzE2NGY2YWExZDQyZjI4ZDI3ODU1MWVjZTU0NTgzMmFhZTE4NTAyIn0=	2020-02-25 08:27:42.924136-05
jfr8brnv6v8828fvvyr0ouufxag1yv2q	ZjU4NzIzZDUwNjNmODAzODU5NDBlNWJmZGQ3YzIxYzNkMTQxMjNmMjp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlNjMyZTFmMWZjZmRlNmRlZWRmMzJkMmY4NTYwYTEwM2U1ZWEwYmE3In0=	2020-02-25 08:28:25.443883-05
0u178xfp1scneo4etko24llr22gqvi6s	N2Y1NDBkM2RjMTRjNzU0Y2JjMzE0NjBjZjgzYzY1YTc2ZjJjNTU1NTp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzE2NGY2YWExZDQyZjI4ZDI3ODU1MWVjZTU0NTgzMmFhZTE4NTAyIn0=	2020-03-18 17:07:29.384588-05
oywpu91j20a61iurfpa24ee168lg6yq5	YzkyZTExMmMwZGU0MjhjYWI0OGIyNDY2MGUxYjNhZWUzMzE3NWVkMjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NWQ3NjJlMDUzYTM1NGExMDIzMTZlZmE1NzBmYTdiOGMwM2VkNjQ0In0=	2020-04-08 22:13:12.394495-05
ehgpbi0al55vj53t0agmme6tcvbvl0g6	Yjk2NWU3OWZiNGMzMDcyNzA3ZTYyNTc1OGUwZDg2OGUwOWI0MWU5Yzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJjOTZjNzdmMDFiNjcyYWJmNTBkZWI5MTc0NGFlYmE0Y2ZjZjczN2ZkIn0=	2020-04-29 13:12:47.558986-05
p5apxlojq1w61gzjkidmu50dv3wwttta	YzkyZTExMmMwZGU0MjhjYWI0OGIyNDY2MGUxYjNhZWUzMzE3NWVkMjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NWQ3NjJlMDUzYTM1NGExMDIzMTZlZmE1NzBmYTdiOGMwM2VkNjQ0In0=	2020-04-29 17:16:30.200915-05
0pa9j3gd9geqursx9o05368gg7jk02fh	N2Y1NDBkM2RjMTRjNzU0Y2JjMzE0NjBjZjgzYzY1YTc2ZjJjNTU1NTp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzE2NGY2YWExZDQyZjI4ZDI3ODU1MWVjZTU0NTgzMmFhZTE4NTAyIn0=	2020-04-29 17:18:31.784472-05
mq5pob8p8tksyt8ldt2jlgvn9zol02hu	ZjU4NzIzZDUwNjNmODAzODU5NDBlNWJmZGQ3YzIxYzNkMTQxMjNmMjp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlNjMyZTFmMWZjZmRlNmRlZWRmMzJkMmY4NTYwYTEwM2U1ZWEwYmE3In0=	2020-04-29 17:48:27.893729-05
6oxxtv644b96keyg6syrfmfply9pvx26	ZjU4NzIzZDUwNjNmODAzODU5NDBlNWJmZGQ3YzIxYzNkMTQxMjNmMjp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlNjMyZTFmMWZjZmRlNmRlZWRmMzJkMmY4NTYwYTEwM2U1ZWEwYmE3In0=	2020-04-29 19:47:07.860117-05
45f6xx730q5dofs79emdpmdjrrmxnjo6	ZjU4NzIzZDUwNjNmODAzODU5NDBlNWJmZGQ3YzIxYzNkMTQxMjNmMjp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlNjMyZTFmMWZjZmRlNmRlZWRmMzJkMmY4NTYwYTEwM2U1ZWEwYmE3In0=	2021-01-28 14:57:40.327328-05
ztv4qx5rjzmi9ewqv6c1gf9h506w7jna	N2Y1NDBkM2RjMTRjNzU0Y2JjMzE0NjBjZjgzYzY1YTc2ZjJjNTU1NTp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzE2NGY2YWExZDQyZjI4ZDI3ODU1MWVjZTU0NTgzMmFhZTE4NTAyIn0=	2021-01-28 15:02:21.815315-05
xw4a2wgdizmy3sfl2wm6e5w6vl8wi8e8	ZDcwZDI4ODg0YzkzY2E4NmZiYWI1MWFjNTdiYzA1ZjM2NTkxZmMyNDp7Il9hdXRoX3VzZXJfaWQiOiI2IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3ZTZhZTQwNTAwYjlkODZhNjdjMGFlMzRkYjVlODYyYzk5MTllMTE3In0=	2021-01-28 16:41:59.379133-05
12fb5mey1cwlspnjmr324semikly85np	YmU3YWZiYzg0MjFlZDAyNzlhZDBkNmRiMGFlOTU0Y2Y5MGIxNjJhZjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1NDc0ZjM4N2M5NWM3NDk4MmJlNjU3ODA0Y2EwMmZhNjlhZjhiOWQ4In0=	2021-01-28 16:46:47.309864-05
j22pfgnv84kllsl5brbdgaoytbcttz1x	N2Y1NDBkM2RjMTRjNzU0Y2JjMzE0NjBjZjgzYzY1YTc2ZjJjNTU1NTp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzE2NGY2YWExZDQyZjI4ZDI3ODU1MWVjZTU0NTgzMmFhZTE4NTAyIn0=	2021-01-28 16:49:37.847733-05
\.


--
-- Data for Name: tickets_auditoriaticketestado; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_auditoriaticketestado (id, observaciones, f_registro, estado_id, ticket_id, usuario_id, usuario_responsable_id) FROM stdin;
1	Primer ticket para analizar	2020-02-11 08:27:17.005828-05	10	1	2	\N
2	apoyo con soporte redes	2020-02-11 08:34:41.071984-05	10	2	3	\N
3	se le asigno a ud el caso	2020-02-11 08:37:49.104358-05	20	2	4	3
4	Aceptado	2020-02-11 08:38:21.302893-05	30	2	3	3
5	lleve la herramienta	2020-02-11 08:38:37.894418-05	20	1	4	3
6	En ejecución	2020-02-11 08:38:44.226831-05	40	2	3	3
7	se hizo el cambio	2020-02-11 08:46:34.604183-05	80	2	3	3
8	se le asigno a ud el caso	2020-02-11 08:47:17.155685-05	20	2	4	3
9	Aceptado	2020-02-11 08:48:03.769534-05	30	2	3	3
10	En ejecución	2020-02-11 08:48:07.768828-05	40	2	3	3
11	se daño todo	2020-02-11 08:48:18.470575-05	80	2	3	3
12	se le asigno a ud el caso	2020-02-11 08:48:52.789007-05	20	2	4	3
13	Aceptado	2020-02-11 08:49:09.197733-05	30	2	3	3
14	En ejecución	2020-02-11 08:49:12.042265-05	40	2	3	3
15	paila no sirve	2020-02-11 08:49:21.951295-05	80	2	3	3
16	se le asigno a ud el caso	2020-02-11 08:50:10.601156-05	20	2	4	3
17	ñlḱ	2020-03-25 22:10:32.669919-05	10	3	1	4
18	lkjlk	2020-03-25 22:13:25.259632-05	10	4	2	\N
19	Se daño mi tv	2020-04-15 17:17:14.757262-05	10	5	2	\N
20	kjhk	2020-04-15 17:44:44.880045-05	20	3	4	3
21		2020-04-15 17:45:06.300392-05	20	5	4	5
22	Aceptado	2020-04-15 17:49:06.021244-05	30	1	3	3
23	En ejecución	2020-04-15 17:49:18.061486-05	40	1	3	3
24	se hizo el cambio del daño	2020-04-15 17:49:42.074134-05	50	1	3	3
25	no naada	2020-04-15 17:50:28.801541-05	70	1	2	3
26	urgente apoyo a cambio	2020-04-15 19:51:16.633109-05	10	6	3	\N
27	se daño el pc rojo	2021-01-14 15:00:49.696133-05	10	7	3	\N
28	xxxxx notas xxxx	2021-01-14 15:02:40.818122-05	20	7	4	3
29	Aceptado	2021-01-14 15:02:57.456297-05	30	7	3	3
30	Aceptado	2021-01-14 15:03:03.533088-05	30	2	3	3
31	En ejecución	2021-01-14 15:03:08.494407-05	40	2	3	3
32	se arreglo	2021-01-14 15:04:36.441714-05	50	2	3	3
33	nota para el tecnico	2021-01-14 15:22:17.072993-05	20	6	4	3
34	Aceptado	2021-01-14 15:24:15.746118-05	30	6	3	3
35	En ejecución	2021-01-14 15:24:26.88635-05	40	7	3	3
36	se hizo cambio de direcciones ip	2021-01-14 15:25:08.310115-05	50	7	3	3
37	no tenemos conectividad	2021-01-14 15:30:15.654628-05	10	8	3	\N
38	por favor apoyar urgentemenete	2021-01-14 15:31:07.069272-05	20	8	4	5
39	En ejecución	2021-01-14 15:34:00.5993-05	40	6	3	3
40	Aceptado	2021-01-14 15:34:23.890837-05	30	8	3	5
41	En ejecución	2021-01-14 15:34:28.019701-05	40	8	3	5
42	no hay repuestos puntuales	2021-01-14 15:34:53.605147-05	80	8	3	5
43	Necesito subir a windows 10 e instalar adobe XD	2021-01-14 16:43:07.488228-05	10	9	6	\N
44	vaya a ver que le paso	2021-01-14 16:51:26.101461-05	20	4	4	7
45	ayudele por favoir lleve para formatearbbbhbkjhkjhjkhjkhjkhjkhjkhjkh	2021-01-14 16:53:00.291527-05	20	9	4	7
46	Aceptado	2021-01-14 16:53:31.077296-05	30	4	7	7
47	Aceptado	2021-01-14 16:53:33.896796-05	30	9	7	7
48	En ejecución	2021-01-14 16:55:23.237932-05	40	9	7	7
49	se ejecuto la instalacion y se hizo un manntto preventivo	2021-01-14 16:58:10.603209-05	50	9	7	7
50	No dejo instalado el Adobe XD	2021-01-14 16:59:29.108569-05	80	9	6	7
51	vaya cumpla con su trabvajo flojo no hizo completo falto adobe	2021-01-14 17:01:49.171355-05	20	9	4	7
52	Aceptado	2021-01-14 17:01:59.353113-05	30	9	7	7
53	En ejecución	2021-01-14 17:02:06.11494-05	40	9	7	7
54	ya quedo instalado	2021-01-14 17:03:00.956539-05	50	9	7	7
55	muy regular el servicio	2021-01-14 17:03:38.176448-05	70	9	6	7
\.


--
-- Data for Name: tickets_estadoticket; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_estadoticket (id, nombre, color, descripcion) FROM stdin;
10	POR_ASIGNAR	#65c7ff	
20	ASIGNADO	#984dff	
30	ACEPTADO	#41e5c0	
40	EN_EJECUCION	#ab8903	
50	EN_EVALUACION	#29B765	
60	REASIGNADO	#D67520	
70	FINALIZADO	#3498DB	
80	NO_CONCLUIDO	#E74C3C	
\.


--
-- Data for Name: tickets_observacion; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_observacion (id, registro, f_registro, ticket_id) FROM stdin;
1	aun no han hecho nada	2020-02-11 08:39:31.2834-05	1
2	aun no han pasado a arreglarlo	2020-04-15 17:46:28.472234-05	5
3	no se ha tenido repuesta	2021-01-14 15:31:27.078568-05	8
4	no me han atendido	2021-01-14 15:31:45.755701-05	8
5	no me han atendido aun	2021-01-14 16:52:21.772603-05	9
6	drgtertert	2021-01-14 17:04:23.994625-05	9
\.


--
-- Data for Name: tickets_ticket; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_ticket (id, nombre, descripcion, notas, solucion, retroalimentacion, f_inicio, f_fin, f_registro, estado_id, responsable_id, solicitante_id, urgencia_id, evaluacion_cliente) FROM stdin;
3	lñk	ñlḱ	kjhk			\N	\N	2020-03-25 22:10:32.436954-05	20	3	1	10	\N
5	ticket 15 ABR 5:17	Se daño mi tv				\N	\N	2020-04-15 17:17:14.635995-05	20	5	2	20	\N
1	Nuevo ticket	Primer ticket para analizar	lleve la herramienta	se hizo el cambio del daño	no naada	2020-04-15 17:49:18.039095-05	2020-04-15 17:50:28.772675-05	2020-02-11 08:27:16.852301-05	70	3	2	20	\N
2	ticket generado por el tecnico	apoyo con soporte redes	se le asigno a ud el caso	se arreglo		2021-01-14 15:03:08.444135-05	\N	2020-02-11 08:34:41.054752-05	50	3	3	30	\N
7	ticket jueves	se daño el pc rojo	xxxxx notas xxxx	se hizo cambio de direcciones ip		2021-01-14 15:24:26.849767-05	\N	2021-01-14 15:00:49.617452-05	50	3	3	20	\N
6	Nuevo caso del tecnico	urgente apoyo a cambio	nota para el tecnico			2021-01-14 15:34:00.571735-05	\N	2020-04-15 19:51:16.608555-05	40	3	3	10	\N
8	el rack esta en mal estado	no tenemos conectividad	por favor apoyar urgentemenete	no hay repuestos puntuales		2021-01-14 15:34:27.987491-05	\N	2021-01-14 15:30:15.624805-05	80	5	3	10	\N
4	lklj	lkjlk	vaya a ver que le paso			\N	\N	2020-03-25 22:13:25.207157-05	30	7	2	30	\N
9	Actualicacion Portatil Lenovo	Necesito subir a windows 10 e instalar adobe XD	vaya cumpla con su trabvajo flojo no hizo completo falto adobe	ya quedo instalado	muy regular el servicio	2021-01-14 17:02:06.072548-05	2021-01-14 17:03:38.147973-05	2021-01-14 16:43:07.451811-05	70	7	6	10	\N
\.


--
-- Data for Name: tickets_tipourgencia; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_tipourgencia (id, nombre, tiempo_solucion, descripcion) FROM stdin;
10	ALTA	1	
20	MEDIA	1	
30	BAJA	1	
\.


--
-- Data for Name: tickets_userprofile; Type: TABLE DATA; Schema: public; Owner: grupoda2
--

COPY public.tickets_userprofile (user_id, telefono) FROM stdin;
\.


--
-- Name: admin2_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.admin2_userprofile_id_seq', 3, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 8, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 8, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 76, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 7, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 7, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: contactos_contacto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_contacto_id_seq', 1, false);


--
-- Name: contactos_contacto_industria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_contacto_industria_id_seq', 1, false);


--
-- Name: contactos_contactorelacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_contactorelacion_id_seq', 1, false);


--
-- Name: contactos_industria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_industria_id_seq', 1, false);


--
-- Name: contactos_pais_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_pais_id_seq', 1, false);


--
-- Name: contactos_paisestado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_paisestado_id_seq', 1, false);


--
-- Name: contactos_relacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.contactos_relacion_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 10, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 19, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 33, true);


--
-- Name: tickets_auditoriaticketestado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.tickets_auditoriaticketestado_id_seq', 55, true);


--
-- Name: tickets_observacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.tickets_observacion_id_seq', 6, true);


--
-- Name: tickets_ticket_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grupoda2
--

SELECT pg_catalog.setval('public.tickets_ticket_id_seq', 9, true);


--
-- Name: admin2_userprofile admin2_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.admin2_userprofile
    ADD CONSTRAINT admin2_userprofile_pkey PRIMARY KEY (id);


--
-- Name: admin2_userprofile admin2_userprofile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.admin2_userprofile
    ADD CONSTRAINT admin2_userprofile_user_id_key UNIQUE (usuario_id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: contactos_contacto contactos_contacto_documento_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_documento_key UNIQUE (documento);


--
-- Name: contactos_contacto_industria contactos_contacto_indus_contacto_id_industria_id_3ab0c8dc_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto_industria
    ADD CONSTRAINT contactos_contacto_indus_contacto_id_industria_id_3ab0c8dc_uniq UNIQUE (contacto_id, industria_id);


--
-- Name: contactos_contacto_industria contactos_contacto_industria_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto_industria
    ADD CONSTRAINT contactos_contacto_industria_pkey PRIMARY KEY (id);


--
-- Name: contactos_contacto contactos_contacto_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_pkey PRIMARY KEY (id);


--
-- Name: contactos_contacto contactos_contacto_usuario_id_b821c02b_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_usuario_id_b821c02b_uniq UNIQUE (usuario_id);


--
-- Name: contactos_contactorelacion contactos_contactorelacion_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactorelacion_pkey PRIMARY KEY (id);


--
-- Name: contactos_industria contactos_industria_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_industria
    ADD CONSTRAINT contactos_industria_pkey PRIMARY KEY (id);


--
-- Name: contactos_pais contactos_pais_codigo_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais
    ADD CONSTRAINT contactos_pais_codigo_key UNIQUE (codigo);


--
-- Name: contactos_pais contactos_pais_nombre_key; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais
    ADD CONSTRAINT contactos_pais_nombre_key UNIQUE (nombre);


--
-- Name: contactos_pais contactos_pais_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais
    ADD CONSTRAINT contactos_pais_pkey PRIMARY KEY (id);


--
-- Name: contactos_paisestado contactos_paisestado_pais_id_codigo_3e8d9101_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado
    ADD CONSTRAINT contactos_paisestado_pais_id_codigo_3e8d9101_uniq UNIQUE (pais_id, codigo);


--
-- Name: contactos_paisestado contactos_paisestado_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado
    ADD CONSTRAINT contactos_paisestado_pkey PRIMARY KEY (id);


--
-- Name: contactos_relacion contactos_relacion_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_relacion
    ADD CONSTRAINT contactos_relacion_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: tickets_auditoriaticketestado tickets_auditoriaticketestado_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado
    ADD CONSTRAINT tickets_auditoriaticketestado_pkey PRIMARY KEY (id);


--
-- Name: tickets_estadoticket tickets_estadoticket_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_estadoticket
    ADD CONSTRAINT tickets_estadoticket_pkey PRIMARY KEY (id);


--
-- Name: tickets_observacion tickets_observacion_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_observacion
    ADD CONSTRAINT tickets_observacion_pkey PRIMARY KEY (id);


--
-- Name: tickets_ticket tickets_ticket_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket
    ADD CONSTRAINT tickets_ticket_pkey PRIMARY KEY (id);


--
-- Name: tickets_tipourgencia tickets_tipourgencia_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_tipourgencia
    ADD CONSTRAINT tickets_tipourgencia_pkey PRIMARY KEY (id);


--
-- Name: tickets_userprofile tickets_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_userprofile
    ADD CONSTRAINT tickets_userprofile_pkey PRIMARY KEY (user_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: contactos_contacto_creado_por_id_21b74f22; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_creado_por_id_21b74f22 ON public.contactos_contacto USING btree (creado_por_id);


--
-- Name: contactos_contacto_documento_58e35795_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_documento_58e35795_like ON public.contactos_contacto USING btree (documento varchar_pattern_ops);


--
-- Name: contactos_contacto_estado_id_bfa0a26f; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_estado_id_bfa0a26f ON public.contactos_contacto USING btree (estado_id);


--
-- Name: contactos_contacto_industria_contacto_id_91b84802; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_industria_contacto_id_91b84802 ON public.contactos_contacto_industria USING btree (contacto_id);


--
-- Name: contactos_contacto_industria_industria_id_60f8c341; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_industria_industria_id_60f8c341 ON public.contactos_contacto_industria USING btree (industria_id);


--
-- Name: contactos_contacto_modificado_por_id_a362c749; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_modificado_por_id_a362c749 ON public.contactos_contacto USING btree (modificado_por_id);


--
-- Name: contactos_contacto_pais_id_5d8ab74e; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contacto_pais_id_5d8ab74e ON public.contactos_contacto USING btree (pais_id);


--
-- Name: contactos_contactorelacion_contacto_id_1e98bf9b; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contactorelacion_contacto_id_1e98bf9b ON public.contactos_contactorelacion USING btree (contacto_id);


--
-- Name: contactos_contactorelacion_contacto_relacionado_id_7f14d590; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contactorelacion_contacto_relacionado_id_7f14d590 ON public.contactos_contactorelacion USING btree (contacto_relacionado_id);


--
-- Name: contactos_contactorelacion_creado_por_id_8f9b80d8; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contactorelacion_creado_por_id_8f9b80d8 ON public.contactos_contactorelacion USING btree (creado_por_id);


--
-- Name: contactos_contactorelacion_modificado_por_id_8075cd68; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contactorelacion_modificado_por_id_8075cd68 ON public.contactos_contactorelacion USING btree (modificado_por_id);


--
-- Name: contactos_contactorelacion_relacion_id_480832f8; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_contactorelacion_relacion_id_480832f8 ON public.contactos_contactorelacion USING btree (relacion_id);


--
-- Name: contactos_industria_creado_por_id_66da5cb1; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_industria_creado_por_id_66da5cb1 ON public.contactos_industria USING btree (creado_por_id);


--
-- Name: contactos_industria_modificado_por_id_d143c13e; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_industria_modificado_por_id_d143c13e ON public.contactos_industria USING btree (modificado_por_id);


--
-- Name: contactos_pais_codigo_3ad22b70_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_pais_codigo_3ad22b70_like ON public.contactos_pais USING btree (codigo varchar_pattern_ops);


--
-- Name: contactos_pais_creado_por_id_ceac3c22; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_pais_creado_por_id_ceac3c22 ON public.contactos_pais USING btree (creado_por_id);


--
-- Name: contactos_pais_modificado_por_id_d3b6c179; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_pais_modificado_por_id_d3b6c179 ON public.contactos_pais USING btree (modificado_por_id);


--
-- Name: contactos_pais_nombre_ebce9cee_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_pais_nombre_ebce9cee_like ON public.contactos_pais USING btree (nombre varchar_pattern_ops);


--
-- Name: contactos_paisestado_creado_por_id_168088dd; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_paisestado_creado_por_id_168088dd ON public.contactos_paisestado USING btree (creado_por_id);


--
-- Name: contactos_paisestado_modificado_por_id_f0c0bfa1; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_paisestado_modificado_por_id_f0c0bfa1 ON public.contactos_paisestado USING btree (modificado_por_id);


--
-- Name: contactos_paisestado_pais_id_910f30ba; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_paisestado_pais_id_910f30ba ON public.contactos_paisestado USING btree (pais_id);


--
-- Name: contactos_relacion_creado_por_id_311687a5; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_relacion_creado_por_id_311687a5 ON public.contactos_relacion USING btree (creado_por_id);


--
-- Name: contactos_relacion_modificado_por_id_094367ca; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX contactos_relacion_modificado_por_id_094367ca ON public.contactos_relacion USING btree (modificado_por_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: tickets_auditoriaticketestado_estado_id_21f33d24; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_auditoriaticketestado_estado_id_21f33d24 ON public.tickets_auditoriaticketestado USING btree (estado_id);


--
-- Name: tickets_auditoriaticketestado_estado_id_21f33d24_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_auditoriaticketestado_estado_id_21f33d24_like ON public.tickets_auditoriaticketestado USING btree (estado_id varchar_pattern_ops);


--
-- Name: tickets_auditoriaticketestado_ticket_id_eae8c426; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_auditoriaticketestado_ticket_id_eae8c426 ON public.tickets_auditoriaticketestado USING btree (ticket_id);


--
-- Name: tickets_auditoriaticketestado_usuario_id_ee0dec32; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_auditoriaticketestado_usuario_id_ee0dec32 ON public.tickets_auditoriaticketestado USING btree (usuario_id);


--
-- Name: tickets_auditoriaticketestado_usuario_responsable_id_401ab195; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_auditoriaticketestado_usuario_responsable_id_401ab195 ON public.tickets_auditoriaticketestado USING btree (usuario_responsable_id);


--
-- Name: tickets_estadoticket_id_5f7b5f8d_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_estadoticket_id_5f7b5f8d_like ON public.tickets_estadoticket USING btree (id varchar_pattern_ops);


--
-- Name: tickets_observacion_ticket_id_8a25dc87; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_observacion_ticket_id_8a25dc87 ON public.tickets_observacion USING btree (ticket_id);


--
-- Name: tickets_ticket_estado_id_de9f88a5; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_estado_id_de9f88a5 ON public.tickets_ticket USING btree (estado_id);


--
-- Name: tickets_ticket_estado_id_de9f88a5_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_estado_id_de9f88a5_like ON public.tickets_ticket USING btree (estado_id varchar_pattern_ops);


--
-- Name: tickets_ticket_responsable_id_2e8d6597; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_responsable_id_2e8d6597 ON public.tickets_ticket USING btree (responsable_id);


--
-- Name: tickets_ticket_solicitante_id_f155a8aa; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_solicitante_id_f155a8aa ON public.tickets_ticket USING btree (solicitante_id);


--
-- Name: tickets_ticket_urgencia_id_857ef8d2; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_urgencia_id_857ef8d2 ON public.tickets_ticket USING btree (urgencia_id);


--
-- Name: tickets_ticket_urgencia_id_857ef8d2_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_ticket_urgencia_id_857ef8d2_like ON public.tickets_ticket USING btree (urgencia_id varchar_pattern_ops);


--
-- Name: tickets_tipourgencia_id_ac202dac_like; Type: INDEX; Schema: public; Owner: grupoda2
--

CREATE INDEX tickets_tipourgencia_id_ac202dac_like ON public.tickets_tipourgencia USING btree (id varchar_pattern_ops);


--
-- Name: admin2_userprofile admin2_userprofile_usuario_id_edf8a88d_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.admin2_userprofile
    ADD CONSTRAINT admin2_userprofile_usuario_id_edf8a88d_fk_auth_user_id FOREIGN KEY (usuario_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_creado_por_id_21b74f22_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_creado_por_id_21b74f22_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_estado_id_bfa0a26f_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_estado_id_bfa0a26f_fk_contactos FOREIGN KEY (estado_id) REFERENCES public.contactos_paisestado(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto_industria contactos_contacto_i_contacto_id_91b84802_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto_industria
    ADD CONSTRAINT contactos_contacto_i_contacto_id_91b84802_fk_contactos FOREIGN KEY (contacto_id) REFERENCES public.contactos_contacto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto_industria contactos_contacto_i_industria_id_60f8c341_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto_industria
    ADD CONSTRAINT contactos_contacto_i_industria_id_60f8c341_fk_contactos FOREIGN KEY (industria_id) REFERENCES public.contactos_industria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_modificado_por_id_a362c749_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_modificado_por_id_a362c749_fk_auth_user_id FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_pais_id_5d8ab74e_fk_contactos_pais_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_pais_id_5d8ab74e_fk_contactos_pais_id FOREIGN KEY (pais_id) REFERENCES public.contactos_pais(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contacto contactos_contacto_usuario_id_b821c02b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contacto
    ADD CONSTRAINT contactos_contacto_usuario_id_b821c02b_fk_auth_user_id FOREIGN KEY (usuario_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contactorelacion contactos_contactore_contacto_id_1e98bf9b_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactore_contacto_id_1e98bf9b_fk_contactos FOREIGN KEY (contacto_id) REFERENCES public.contactos_contacto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contactorelacion contactos_contactore_contacto_relacionado_7f14d590_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactore_contacto_relacionado_7f14d590_fk_contactos FOREIGN KEY (contacto_relacionado_id) REFERENCES public.contactos_contacto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contactorelacion contactos_contactore_creado_por_id_8f9b80d8_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactore_creado_por_id_8f9b80d8_fk_auth_user FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contactorelacion contactos_contactore_modificado_por_id_8075cd68_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactore_modificado_por_id_8075cd68_fk_auth_user FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_contactorelacion contactos_contactore_relacion_id_480832f8_fk_contactos; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_contactorelacion
    ADD CONSTRAINT contactos_contactore_relacion_id_480832f8_fk_contactos FOREIGN KEY (relacion_id) REFERENCES public.contactos_relacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_industria contactos_industria_creado_por_id_66da5cb1_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_industria
    ADD CONSTRAINT contactos_industria_creado_por_id_66da5cb1_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_industria contactos_industria_modificado_por_id_d143c13e_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_industria
    ADD CONSTRAINT contactos_industria_modificado_por_id_d143c13e_fk_auth_user_id FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_pais contactos_pais_creado_por_id_ceac3c22_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais
    ADD CONSTRAINT contactos_pais_creado_por_id_ceac3c22_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_pais contactos_pais_modificado_por_id_d3b6c179_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_pais
    ADD CONSTRAINT contactos_pais_modificado_por_id_d3b6c179_fk_auth_user_id FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_paisestado contactos_paisestado_creado_por_id_168088dd_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado
    ADD CONSTRAINT contactos_paisestado_creado_por_id_168088dd_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_paisestado contactos_paisestado_modificado_por_id_f0c0bfa1_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado
    ADD CONSTRAINT contactos_paisestado_modificado_por_id_f0c0bfa1_fk_auth_user_id FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_paisestado contactos_paisestado_pais_id_910f30ba_fk_contactos_pais_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_paisestado
    ADD CONSTRAINT contactos_paisestado_pais_id_910f30ba_fk_contactos_pais_id FOREIGN KEY (pais_id) REFERENCES public.contactos_pais(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_relacion contactos_relacion_creado_por_id_311687a5_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_relacion
    ADD CONSTRAINT contactos_relacion_creado_por_id_311687a5_fk_auth_user_id FOREIGN KEY (creado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactos_relacion contactos_relacion_modificado_por_id_094367ca_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.contactos_relacion
    ADD CONSTRAINT contactos_relacion_modificado_por_id_094367ca_fk_auth_user_id FOREIGN KEY (modificado_por_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_auditoriaticketestado tickets_auditoriatic_estado_id_21f33d24_fk_tickets_e; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado
    ADD CONSTRAINT tickets_auditoriatic_estado_id_21f33d24_fk_tickets_e FOREIGN KEY (estado_id) REFERENCES public.tickets_estadoticket(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_auditoriaticketestado tickets_auditoriatic_ticket_id_eae8c426_fk_tickets_t; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado
    ADD CONSTRAINT tickets_auditoriatic_ticket_id_eae8c426_fk_tickets_t FOREIGN KEY (ticket_id) REFERENCES public.tickets_ticket(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_auditoriaticketestado tickets_auditoriatic_usuario_id_ee0dec32_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado
    ADD CONSTRAINT tickets_auditoriatic_usuario_id_ee0dec32_fk_auth_user FOREIGN KEY (usuario_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_auditoriaticketestado tickets_auditoriatic_usuario_responsable__401ab195_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_auditoriaticketestado
    ADD CONSTRAINT tickets_auditoriatic_usuario_responsable__401ab195_fk_auth_user FOREIGN KEY (usuario_responsable_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_observacion tickets_observacion_ticket_id_8a25dc87_fk_tickets_ticket_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_observacion
    ADD CONSTRAINT tickets_observacion_ticket_id_8a25dc87_fk_tickets_ticket_id FOREIGN KEY (ticket_id) REFERENCES public.tickets_ticket(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_ticket tickets_ticket_estado_id_de9f88a5_fk_tickets_estadoticket_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket
    ADD CONSTRAINT tickets_ticket_estado_id_de9f88a5_fk_tickets_estadoticket_id FOREIGN KEY (estado_id) REFERENCES public.tickets_estadoticket(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_ticket tickets_ticket_responsable_id_2e8d6597_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket
    ADD CONSTRAINT tickets_ticket_responsable_id_2e8d6597_fk_auth_user_id FOREIGN KEY (responsable_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_ticket tickets_ticket_solicitante_id_f155a8aa_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket
    ADD CONSTRAINT tickets_ticket_solicitante_id_f155a8aa_fk_auth_user_id FOREIGN KEY (solicitante_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_ticket tickets_ticket_urgencia_id_857ef8d2_fk_tickets_tipourgencia_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_ticket
    ADD CONSTRAINT tickets_ticket_urgencia_id_857ef8d2_fk_tickets_tipourgencia_id FOREIGN KEY (urgencia_id) REFERENCES public.tickets_tipourgencia(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tickets_userprofile tickets_userprofile_user_id_75909dd7_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: grupoda2
--

ALTER TABLE ONLY public.tickets_userprofile
    ADD CONSTRAINT tickets_userprofile_user_id_75909dd7_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

